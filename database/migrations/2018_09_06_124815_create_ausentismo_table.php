<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAusentismoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ausentismo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cliente');
            $table->integer('empresa');
            $table->integer('mes_carga');
            $table->integer('id_empleado');
            $table->date('fecha_inicio');
            $table->date('fecha_fin');
            $table->integer('causal');
            $table->integer('dias');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ausentismo', function (Blueprint $table) {
            Schema::dropIfExists('ausentismo');
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRegCargas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reg_carga', function (Blueprint $table) {
            $table->increments('id');
            $table->string('empresa');
            $table->string('usuario');
            $table->string('mes');
            $table->string('anio');
            $table->string('tipo_carga');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reg_carga', function (Blueprint $table) {
            Schema::dropIfExists('reg_carga');
        });
    }
}

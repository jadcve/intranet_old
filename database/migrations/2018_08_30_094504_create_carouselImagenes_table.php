<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarouselImagenesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imagenes_carousel', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('carousel');
            $table->string('imagen')->unique();            
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('imagenes_carousel', function (Blueprint $table) {
            Schema::dropIfExists('imagenes_carousel');
        });
    }
}

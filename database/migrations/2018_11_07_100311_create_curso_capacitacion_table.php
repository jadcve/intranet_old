<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCursoCapacitacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('curso_capacitacion', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('empresa');
            $table->string('curso');
            $table->string('nombre');
            $table->string('categoria');
            $table->string('modalidad');
            $table->string('monto');
            $table->date('fecha_inicio');
            $table->date('fecha_fin');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('curso_capacitacion', function (Blueprint $table) {
            Schema::dropIfExists('capacitacion');
        });
    }
}
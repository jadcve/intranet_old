<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBenckmarkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('benchmark', function (Blueprint $table) {
            $table->increments('id');
            $table->string('benchmark');
            $table->string('descripcion');
            $table->string('document');
            $table->integer('empresa');
            $table->integer('usuario');
            $table->integer('grupo');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('benchmark', function (Blueprint $table) {
            Schema::dropIfExists('benchmark');
        });
    }
}

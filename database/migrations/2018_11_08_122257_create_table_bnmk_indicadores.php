<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBnmkIndicadores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bnmk_indicadores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('empresa');
            $table->string('usuario');
            $table->string('historial_carga');
            $table->string('modalidad');
            $table->string('plan');
            $table->string('es_admin');
            $table->string('url_bi');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bnmk_indicadores', function (Blueprint $table) {
            Schema::dropIfExists('bnmk_indicadores');
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTrackin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trackin', function (Blueprint $table) {
            $table->increments('id');
            $table->string('empresa');
            $table->string('usuario');
            $table->string('historial_carga');
            $table->string('modalidad');
            $table->string('plan');
            $table->string('es_admin');
            $table->string('url_bi');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trackin', function (Blueprint $table) {
            Schema::dropIfExists('trackin');
        });
    }
}
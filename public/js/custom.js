$(document).ready(function()
{


});

$('.insertar_input_mayor').change(function() {

	check = this;
	
    $id_check = $(check).attr("id");
	$activo = $(check).attr("activo");

	if ($activo == "no") 
	{
		
		$(check).parent().parent().append("<input type='text' autocomplete='off' class='form-control' style='width:90% !important;' placeholder='Ingrese numero' id='"+$id_check+"_valor_maximo' name='"+$id_check+"_valor_maximo'> </input>");
		$activo = $(check).attr("activo","si");
	}else{
		
		$("#"+$id_check+"_valor_maximo").remove();
		$activo = $(check).attr("activo","no");
	}
});

$('.insertar_input_menor').change(function() {

	check = this;

	$id_check = $(check).attr("id");
	$activo = $(check).attr("activo");

	if ($activo == "no") 
	{
		$(check).parent().parent().append("<input type='text' autocomplete='off' class='form-control' style='width:90% !important;' placeholder='Ingrese numero' id='"+$id_check+"_valor_minimo' name='"+$id_check+"_valor_minimo'> </input>");
		$activo = $(check).attr("activo","si");
	}else{
		
		$("#"+$id_check+"_valor_minimo").remove();
		$activo = $(check).attr("activo","no");
	}
});

$('.insertar_input_fecha_mayor').change(function() {

	check = this;

	$id_check = $(check).attr("id");
	$activo = $(check).attr("activo");

	if ($activo == "no") 
	{
		$(check).parent().parent().append("<input type='date' autocomplete='off' class='form-control' style='width:90% !important;' id='"+$id_check+"_valor_fecha_maximo' name='"+$id_check+"_valor_fecha_maximo'> </input>");
		$activo = $(check).attr("activo","si");
	}else{
		
		$("#"+$id_check+"_valor_fecha_maximo").remove();
		$activo = $(check).attr("activo","no");
	}

});

$('.insertar_input_fecha_menor').change(function() {

	check = this;

	$id_check = $(check).attr("id");
	$activo = $(check).attr("activo");

	if ($activo == "no") 
	{
		$(check).parent().parent().append("<input type='date' autocomplete='off' class='form-control' style='width:90% !important;' id='"+$id_check+"_valor_fecha_minimo' name='"+$id_check+"_valor_fecha_minimo'> </input>");
		$activo = $(check).attr("activo","si");
	}else{
		
		$("#"+$id_check+"_valor_fecha_minimo").remove();
		$activo = $(check).attr("activo","no");
	}
});

$('.insertar_input_valor_valido').change(function() {

	check = this;
	$id_check = $(check).attr("id");
	$activo = $(check).attr("activo");

	if ($activo == "no") 
	{
		$(check).parent().parent().append("<input type='text' autocomplete='off' class='form-control' style='width:90% !important;' id='"+$id_check+"_valores_validos' name='"+$id_check+"_valores_validos'> </input>");
		$activo = $(check).attr("activo","si");
	}else{
		
		$("#"+$id_check+"_valores_validos").remove();
		$activo = $(check).attr("activo","no");
	}
});

$('.insertar_select_padre_hijo').change(function() {

	check = this;

	$id_check = $(check).attr("id");
	$activo = $(check).attr("activo");

	if ($activo == "no") 
	{
		$optionArray = {'id_empleado' : 'ID Empleado',
 						'nombre' : 'Nombre',
						'email' :'Email',
						'genero' : 'Genero',
						'fecha_nacimiento' : 'Fecha de Nacimiento',
						'fecha_ingreso' :'Fecha de ingreso',
						'antiguedad_n' : 'Antiguedad',
						'edad_n' : 'Edad',
						'edad_categorizada' : 'Edad Categorizada',
						'generacion' : 'Generación',
						'antiguedad_cat' : 'Antiguedad Categorizada',
						'antig_menor_anio' : 'Antiguedad menor a 1 año',
						'tipo_de_cargo' : 'Tipo de Cargo',
						'cargo' : 'Cargo',
						'cargo2' : 'Cargo 2',
						'nivel_educacional' : 'Nivel Educacional',
						'region' : 'Región',
						'ciudad' : 'Ciudad',
						'es_jefe' : 'Es Jefe',
						'id_jefe' : 'ID Jefe',
						'cat_jefe' : 'Cat Jefe',
						'n1' : 'N1',
						'n2' : 'N2',
						'n3' : 'N3',
						'n4' : 'N4',
						'n5' : 'N5',
						'segmento_adicional_1' : 'Segmento add 1',
						'segmento_adicional_2' : 'Segmento add 2',
						'segmento_adicional_3' : 'Segmento add 3',
						'ausentismo_anio': 'Ausentismo Año',
						'desempenio' : 'Desempeño',
						'cumplimiento_metas' : 'Cumplimiento de metas',
						'horas_extras_anio' : 'Horas extras',
						'horas_capa	citacion' : 'Horas Capacitación',
						'tipo_contrato' : 'Tipo Contrato',
						'tipo_remuneracion' : 'Tipo remuneración',
            			'sindicalizado' : 'Sindicalizado',
						'var_hr1' : 'Campo adicional 1',
						'var_hr2' : 'Campo adicional 2',
						'var_hr3' : 'Campo adicional 3',
						'var_hr4' : 'Campo adicional 4',
						'var_hr5' : 'Campo adicional 5',
						'var_hr6' : 'Campo adicional 6',
						'var_hr7' : 'Campo adicional 7',
						'var_hr8' : 'Campo adicional 8',
						'var_hr9' : 'Campo adicional 9',
						'var_hr10': 'Campo adicional 10'
					};

		
		var appendSelect = $(check).parent().parent().append("<div id='"+$id_check+"_padre_hijo'><center> Casilla Hijo </center><select id='"+$id_check+"_select' class='form-control' required name='"+$id_check+"_padre_hijo'><option selected='selected' value=''>Selecciona</option></select></div>");
		$activo = $(check).attr("activo","si");

		$.each($optionArray, function( index, value ) {

          var appendOption = ("<option value='"+index+"'>"+value+"</option>");

          $(appendOption).appendTo("#"+$id_check+"_select");

         });

	}else{
		
		$("#"+$id_check+"_padre_hijo").remove();
		$activo = $(check).attr("activo","no");
	}
});

/*
function insertar_input_mayor(check)
{
	$id_check = $(check).attr("id");
	$activo = $(check).attr("activo");

	if ($activo == "no") 
	{
		$(check).parent().append("<input type='text' autocomplete='off' class='form-control' style='width:90% !important;' placeholder='Ingrese numero' id='"+$id_check+"_valor_maximo' name='"+$id_check+"_valor_maximo'> </input>");
		$activo = $(check).attr("activo","si");
	}else{
		
		$("#"+$id_check+"_valor_maximo").remove();
		$activo = $(check).attr("activo","no");
	}
	
}


function insertar_input_menor(check)
{
	$id_check = $(check).attr("id");
	$activo = $(check).attr("activo");

	if ($activo == "no") 
	{
		$(check).parent().append("<input type='text' autocomplete='off' class='form-control' style='width:90% !important;' placeholder='Ingrese numero' id='"+$id_check+"_valor_minimo' name='"+$id_check+"_valor_minimo'> </input>");
		$activo = $(check).attr("activo","si");
	}else{
		
		$("#"+$id_check+"_valor_minimo").remove();
		$activo = $(check).attr("activo","no");
	}
	
}

function insertar_input_fecha_mayor(check)
{
	$id_check = $(check).attr("id");
	$activo = $(check).attr("activo");

	if ($activo == "no") 
	{
		$(check).parent().append("<input type='date' autocomplete='off' class='form-control' style='width:90% !important;' id='"+$id_check+"_valor_fecha_maximo' name='"+$id_check+"_valor_fecha_maximo'> </input>");
		$activo = $(check).attr("activo","si");
	}else{
		
		$("#"+$id_check+"_valor_fecha_maximo").remove();
		$activo = $(check).attr("activo","no");
	}

}

function insertar_input_fecha_menor(check)
{
	$id_check = $(check).attr("id");
	$activo = $(check).attr("activo");

	if ($activo == "no") 
	{
		$(check).parent().append("<input type='date' autocomplete='off' class='form-control' style='width:90% !important;' id='"+$id_check+"_valor_fecha_minimo' name='"+$id_check+"_valor_fecha_minimo'> </input>");
		$activo = $(check).attr("activo","si");
	}else{
		
		$("#"+$id_check+"_valor_fecha_minimo").remove();
		$activo = $(check).attr("activo","no");
	}

}

function insertar_input_valor_valido(check)
{
	$id_check = $(check).attr("id");
	$activo = $(check).attr("activo");

	if ($activo == "no") 
	{
		$(check).parent().append("<input type='text' autocomplete='off' class='form-control' style='width:90% !important;' id='"+$id_check+"_valores_validos' name='"+$id_check+"_valores_validos'> </input>");
		$activo = $(check).attr("activo","si");
	}else{
		
		$("#"+$id_check+"_valores_validos").remove();
		$activo = $(check).attr("activo","no");
	}

}
*/
function insertar_select_padre_hijo(check)
{
	$id_check = $(check).attr("id");
	$activo = $(check).attr("activo");

	if ($activo == "no") 
	{
		$optionArray = {'id_empleado' : 'ID Empleado',
 						'nombre' : 'Nombre',
						'email' :'Email',
						'genero' : 'Genero',
						'fecha_nacimiento' : 'Fecha de Nacimiento',
						'fecha_ingreso' :'Fecha de ingreso',
						'antiguedad_n' : 'Antiguedad',
						'edad_n' : 'Edad',
						'edad_categorizada' : 'Edad Categorizada',
						'generacion' : 'Generación',
						'antiguedad_categorizada' : 'Antiguedad Categorizada',
						'antiguedad_menor_a_1_anio' : 'Antiguedad menor a 1 año',
						'tipo_de_cargo' : 'Tipo de Cargo',
						'cargo' : 'Cargo',
						'cargo_2' : 'Cargo 2',
						'nivel_educacional' : 'Nivel Educacional',
						'region' : 'Región',
						'ciudad' : 'Ciudad',
						'es_jefe' : 'Es Jefe',
						'id_jefe' : 'ID Jefe',
						'cat_jefe' : 'Cat Jefe',
						'n1' : 'N1',
						'n2' : 'N2',
						'n3' : 'N3',
						'n4' : 'N4',
						'n5' : 'N5',
						'segmento_adicional_1' : 'Segmento add 1',
						'segmento_adicional_2' : 'Segmento add 2',
						'segmento_adicional_3' : 'Segmento add 3',
						'desempenio' : 'Desempeño',
						'cumplimiento_metas' : 'Cumplimiento de metas',
						'horas_extras_anio' : 'Horas extras',
						'horas_capacitacion' : 'Horas Capacitación',
						'tipo_contrato' : 'Tipo Contrato',
						'tipo_remuneracion' : 'Tipo remuneración'
					};

		
		var appendSelect = $(check).parent().append("<div id='"+$id_check+"_padre_hijo'><center> Casilla Hijo </center><select id='"+$id_check+"_select' class='form-control' required name='"+$id_check+"_padre_hijo'><option selected='selected' value=''>Selecciona</option></select></div>");
		$activo = $(check).attr("activo","si");

		$.each($optionArray, function( index, value ) {

          var appendOption = ("<option value='"+index+"'>"+value+"</option>");

          $(appendOption).appendTo("#"+$id_check+"_select");

         });

	}else{
		
		$("#"+$id_check+"_padre_hijo").remove();
		$activo = $(check).attr("activo","no");
	}
}

function actualizar_cliente()
{
	var empresa_seleccionada = $('#empresa option:selected').val();

	$("#cliente").empty();
	$("#estudios").empty();

	var clienteOptionDefault = ("<option selected='selected' disabled value=''> Selecciona </option>");
    $(clienteOptionDefault).appendTo("#cliente");

    var estudiosOptionDefault = ("<option selected='selected' disabled value=''> Selecciona </option>");
    $(estudiosOptionDefault).appendTo("#estudios");

  	$.ajax({
      url:BaseURL+'/getClientes',
      data:{empresa : empresa_seleccionada},
      method:'post',
      dataType: 'json',
      headers: {
                'x-csrf-token': $('meta[name="csrf-token"]').attr('content'),
              },
      success:function(response)
      {

      	$.each(response, function(index, value ){

      		var appendOption = ("<option value='"+index+"'>"+value+"</option>");
          	$(appendOption).appendTo("#cliente");

      	});
        
      }
    });
}

function actualizar_estudio()
{
	var cliente_seleccionada = $('#cliente option:selected').val();

	$("#estudios").empty();

	var estudiosOptionDefault = ("<option selected='selected' disabled value=''> Selecciona </option>");
    $(estudiosOptionDefault).appendTo("#estudios");

  	$.ajax({
      url:BaseURL+'/getEstudios',
      data:{cliente : cliente_seleccionada},
      method:'post',
      dataType: 'json',
      headers: {
                'x-csrf-token': $('meta[name="csrf-token"]').attr('content'),
              },
      success:function(response)
      {

      	$.each(response, function(index, value ){

      		var appendOption = ("<option value='"+index+"'>"+value+"</option>");
          	$(appendOption).appendTo("#estudios");

      	});
        
      }
    });
}


	
	
	
	

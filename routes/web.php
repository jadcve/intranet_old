<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'frontController@index');
Route::get('logout', 'Auth\LoginController@logout');
Route::resource('log', 'LogController');

Route::get('createTableValidacionEstudios', function () {
    return view('createTableValidacionEstudios');
});


	Route::group(['middleware' => 'auth'], function(){
		Route::get('admin', function(){ return view('admin'); });
		Route::resource('usuarios', 'usuarioController');
		Route::resource('clientes', 'clienteController');
		Route::get('/trackins/cliente/{id}', 'clienteController@getcliente');
		Route::resource('paises', 'paisController');
		Route::resource('subrubros', 'subrubroController');
		Route::resource('departamentos', 'departamentoController');
		Route::resource('cargos', 'cargoController');
		Route::resource('rubros', 'rubroController');
		Route::resource('empresas', 'empresaController');
		Route::resource('estudios', 'estudioController');
		Route::resource('encuestas', 'encuestaController');
		Route::resource('bitacoras', 'bitacoraController');
		Route::resource('benchmarks', 'benchmarkController');
		Route::resource('mails', 'mailController');
		Route::resource('promotions', 'promotionController');
		Route::resource('carousels', 'carouselController');
		Route::resource('uploadimagenes', 'uploadimagenesController');
		Route::get('cargarimg/{id}', 'uploadimagenesController@create');
		Route::resource('trackins', 'trackinController');
		Route::resource('reg_cargas', 'reg_cargasController');
		Route::resource('estamentos', 'estamentoController');
		Route::resource('areas', 'areaController');
		Route::resource('tipo_areas', 'tipo_areaController');
		Route::resource('tipo_causales', 'tipo_causalController');
		Route::resource('causales', 'causalController');
		Route::resource('bench_indicadores', 'b_indicadoresController');
        Route::get('/bench_indicadores/cliente/{id}', 'clienteController@getcliente');
        Route::get('usuarios/excel', 'usuarioController@excel');
        Route::get('/estudios/cliente/{id}', 'clienteController@getcliente');
        Route::resource('/estudio_validate','estudio_validacionController');
        Route::get('estudio_validate/{id}', 'clienteController@getcliente');
        Route::get('estudio_validate/delete/{id}','estudio_validacionController@destroy');
        Route::post('getClientes','clienteController@actualizarClientes');
        Route::post('getEstudios','estudioController@actualizarEstudios');


	});


<?php

namespace kpi;

use Illuminate\Database\Eloquent\Model;

class validacion_menor_mayor extends Model
{
    protected $connection = 'mysql';
	
	protected $table = 'validacion_menor_mayor';

	protected $fillable = ['numero', 'campo_a_validar', 'id_tabla', 'tipo_validacion'];

	protected $primaryKey = 'id';
	
}

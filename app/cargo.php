<?php

namespace kpi;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class cargo extends Model
{
	use SoftDeletes;

    protected $fillable = [
        'cargo', 'status'
    ];
}

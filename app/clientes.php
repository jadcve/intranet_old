<?php

namespace kpi;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;

class clientes extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    

    protected $table = 'clientes';

    protected $fillable = [
        'nombre', 'apellido', 'empresa', 'cargo', 'telefono', 'path', 'email', 'password', 'status'];

    public function setPathAttribute($path){
        if(!empty($path)){
        $this->attributes['path'] = Carbon::now()->second.$path->GetClientOriginalName();
        $name = Carbon::now()->second.$path->GetClientOriginalName();
        \Storage::disk('fotos')->put($name, \File::get($path));
        \Storage::disk('s3')->put($name, \File::get($path));
        }

    }

    public static function ClientesEmpre(){
        return DB::table('clientes')
        ->join('empresas','empresas.id','=','clientes.empresa')
        ->join('cargos','cargos.id','=','clientes.cargo')
        ->select('clientes.*','empresas.empresa','cargos.cargo')
        ->get();
    }
   
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($valor){
        if(!empty($valor)){
            $this->attributes['password'] = \Hash::make($valor);
        }
    }

    public static function clientesParaSelect($empresa_id){
    return DB::table('clientes as c')
    ->select(DB::raw("c.id, CONCAT( c.nombre,' ' , c.apellido )  as nombreCompleto") )
    ->where('c.empresa','=', $empresa_id)
    ->pluck('nombreCompleto','c.id');
    }


}
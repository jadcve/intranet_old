<?php

namespace kpi;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;

class encuesta extends Model
{
    protected $table = 'encuestas';

    protected $fillable = [
        'encuesta', 'descripcion', 'document', 'empresa', 'usuario', 'grupo', 'status', 'created_at'
    ];


    public static function Encuestas(){
        return DB::table('encuestas')
        ->join('clientes','clientes.id','=','encuestas.usuario')
        ->select('encuestas.*','clientes.nombre', 'clientes.apellido')
        ->get();
    }
}
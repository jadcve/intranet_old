<?php

namespace kpi;

use Illuminate\Database\Eloquent\Model;

class tipo_area extends Model
{
	protected $table = 'tipo_area';

    protected $fillable = [
        'tipo', 'status'
    ];
}

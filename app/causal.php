<?php

namespace kpi;

use Illuminate\Database\Eloquent\Model;
use DB;

class causal extends Model
{
    protected $table = 'causales';

    protected $fillable = [
        'tipo_causal', 'causal', 'status',
    ];


    public static function causales(){
        return DB::table('causales')
        ->join('tipo_causal','tipo_causal.id','=','causales.tipo_causal')
        ->select('causales.*','tipo_causal.tipo')
        ->get();
    }
}

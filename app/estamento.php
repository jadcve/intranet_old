<?php

namespace kpi;

use Illuminate\Database\Eloquent\Model;

class estamento extends Model
{

	protected $table = 'estamentos';

    protected $fillable = [
        'estamento', 'status'
    ];
}

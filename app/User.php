<?php

namespace kpi;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';

    protected $fillable = [
        'nombre', 'apellido', 'telefono', 'path', 'email', 'password', 'rol', 'password_new', 'status'];

    public function setPathAttribute($path){
        if(!empty($path)){
        $this->attributes['path'] = Carbon::now()->second.$path->GetClientOriginalName();
        $name = Carbon::now()->second.$path->GetClientOriginalName();
        \Storage::disk('fotos')->put($name, \File::get($path));
       // \Storage::disk('s3')->put($name, \File::get($path));
        }
    }


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($valor){
        if(!empty($valor)){
            $this->attributes['password'] = \Hash::make($valor);
        }
    }
}

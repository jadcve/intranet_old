<?php

namespace kpi;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use DB;

class subrubro extends Model
{
    use SoftDeletes;

    protected $table = 'subrubros';

    protected $fillable = [
        'rubro', 'subrubro', 'status'
    ];

    public static function Subrubros(){
        return DB::table('subrubros')
        ->join('rubros','rubros.id','=','subrubros.rubro')
        ->select('subrubros.*','rubros.rubro')
        ->get();
    }    
}

<?php

namespace kpi;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;

class estudio extends Model
{
    protected $table = 'estudios';

    protected $fillable = [
        'estudio', 'descripcion', 'envio_mail', 'tipo', 'path', 'url', 'usuario', 'carga_datos','status', 'created_at'
    ];

    public function setPathAttribute($path){
        if(!empty($path)){
        $this->attributes['path'] = Carbon::now()->second.$path->GetClientOriginalName();
        $name = Carbon::now()->second.$path->GetClientOriginalName();
      //  \Storage::disk('archivos')->put($name, \File::get($path));
        \Storage::disk('s3')->put($name,  \File::get($path));
        }
    }

    public static function Estudios(){
        return DB::table('estudios')
        ->join('clientes','clientes.id','=','estudios.usuario')
        ->select('estudios.*','clientes.nombre', 'clientes.apellido')
        ->get();
    }

    public static function clientes_estudio(){
        return DB::table('estudios')
            ->join('empresas','empresas.id','=','estudios.empresa')
            ->join('clientes', 'clientes.id', '=', 'estudios.usuario')
            ->select('estudios.*','empresas.empresa', 'clientes.nombre', 'clientes.apellido')
            ->get();
    }

    public static function estudiosParaSelect($cliente_id)
    {
        $estudios = DB::table('estudios as e')
        ->where('e.usuario', $cliente_id)
        ->pluck('estudio', 'id');

        return $estudios;
    }

}
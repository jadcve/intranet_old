<?php

namespace kpi;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;

class carousel extends Model
{
    protected $table = 'carousels';

    protected $fillable = [
        'titulo', 'descripcion', 'status'
    ];
}

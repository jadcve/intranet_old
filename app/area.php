<?php

namespace kpi;

use Illuminate\Database\Eloquent\Model;
use DB;

class area extends Model
{
    protected $table = 'area';

    protected $fillable = [
        'tipo_area', 'area', 'status',
    ];


    public static function areas(){
        return DB::table('area')
        ->join('tipo_area','tipo_area.id','=','area.tipo_area')
        ->select('area.*','tipo_area.tipo')
        ->get();
    }
}

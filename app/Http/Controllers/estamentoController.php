<?php

namespace kpi\Http\Controllers;

use kpi\Http\Requests;
use kpi\Http\Requests\EstamentoCreateRequest;
use kpi\Http\Requests\EstamentoUpdateRequest;
use kpi\Http\Controllers\Controller;
use Session;
use Redirect;
use kpi\estamento;
use Illuminate\Http\Request;

class estamentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estamentos = estamento::paginate(10);
        return view('estamentos.index', compact('estamentos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EstamentoCreateRequest $request)
    {
        estamento::create([
            'estamento' => $request['estamento'],
            'status' => $request['status'],            
        ]);
        
        Session::flash('message', 'Estamento Creado Correctamente');
        return Redirect::to('estamentos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $estamento = estamento::find($id);
        return view('estamentos.edit',['estamento'=>$estamento]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EstamentoUpdateRequest $request, $id)
    {
        $estamento = estamento::find($id)->update($request->all());
        Session::flash('message', 'Estamento Editado Correctamente');
        return Redirect::to('estamentos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $estamento = estamento::find($id);
        $estamento->delete();

        Session::flash('message', 'Estamento Eliminado Correctamente');
        return Redirect::to('estamentos');
    }
}

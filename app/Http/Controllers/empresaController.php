<?php

namespace kpi\Http\Controllers;

use kpi\Http\Requests;
use kpi\Http\Requests\EmpresaCreateRequest;
use kpi\Http\Requests\EmpresaUpdateRequest;
use kpi\Http\Controllers\Controller;
use Session;
use Redirect;
use kpi\empresa;
use kpi\rubro;
use kpi\subrubro;
use kpi\pais;
use Illuminate\Http\Request;

class empresaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $empresas = empresa::empresas();
        return view('empresas.index', compact('empresas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rubros = rubro::pluck('rubro','id');
        $paises = pais::pluck('pais','id');
        $subrubros = subrubro::pluck('subrubro','id');

        return view('empresas.crear')->with(compact('rubros','paises','subrubros'));
       
        //$rubros = Rubro::lists('rubro', 'rubro')->all(); 
        //return view('empresas.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmpresaCreateRequest $request)
    {
        empresa::create([
            'empresa' => $request['empresa'],
            'rubro' => $request['rubro'],
            'subrubro' => $request['subrubro'],
            'telefono' => $request['telefono'],
            'origen' => $request['origen'],
            'pais' => $request['pais'],
            'web' => $request['web'],
            'path' => $request['path'],
            'status' => $request['status'],
        ]);
        
        Session::flash('message', 'Empresa Creada Correctamente');
        return Redirect::to('empresas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rubros = rubro::pluck('rubro','id');
        $paises = pais::pluck('pais','id');
        $subrubros = subrubro::pluck('subrubro','id');
        $empresa = empresa::find($id);
        return view('empresas.edit',['empresa'=>$empresa])->with(compact('rubros','paises','subrubros'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, EmpresaUpdateRequest $request)
    {
        $empresa = empresa::find($id)->update($request->all());
        Session::flash('message', 'Empresa Editada Correctamente');
        return Redirect::to('empresas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $empresa = empresa::find($id);
        $empresa->delete();

        Session::flash('message', 'Empresa Eliminada Correctamente');
        return Redirect::to('empresas');
    }
}

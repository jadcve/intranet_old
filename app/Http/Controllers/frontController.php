<?php

namespace kpi\Http\Controllers;

use kpi\Http\Requests;
use kpi\Http\Controllers\Controller;

use Illuminate\Http\Request;


class frontController extends Controller
{
	public function __construct(){
		$this->middleware('auth',['only' => 'admin']);
	}

    public Function index()
    {
    	return view('index');
    }
}


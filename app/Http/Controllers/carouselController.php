<?php

namespace kpi\Http\Controllers;

use kpi\Http\Requests;
use kpi\Http\Requests\CarouselCreateRequest;
use kpi\Http\Requests\CarouselUpdateRequest;
use kpi\Http\Controllers\Controller;
use Redirect;
use Session;
use kpi\imagenes_carousel;
use kpi\carousel;
use kpi\promotion;
use Illuminate\Http\Request;
use Mail;
use Auth;
use DB;

class carouselController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $carousels = carousel::all();
        return view('carousels.index', compact('carousels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('carousels.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CarouselCreateRequest $request)
    {
        carousel::create($request->all());
        Session::flash('message', 'Carrusel Creado Correctamente, favor subir imagenes');
        return Redirect::to('carousels');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $carousel = carousel::find($id);
        return view('carousels.edit',['carousel'=>$carousel]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CarouselUpdateRequest $request, $id)
    {
        $carousel = carousel::find($id)->update($request->all());
        Session::flash('message', 'Carrusel Editado Correctamente');
        return Redirect::to('carousels');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        echo $promotions = DB::table('promotions')
        ->join('carousels','carousels.titulo','=','promotions.carousel')
        ->select('promotions.id')
        ->where('carousels.id','=',$id)
        ->delete();

        $carousel = carousel::find($id);
        $carousel->delete();   

        Session::flash('message', 'Carrusel Eliminado Correctamente');
        return Redirect::to('carousels');        
    }

}

<?php

namespace kpi\Http\Controllers;

use kpi\Http\Requests;
use kpi\Http\Requests\AreaCreateRequest;
use kpi\Http\Requests\AreaUpdateRequest;
use kpi\Http\Controllers\Controller;
use Session;
use Redirect;
use kpi\area;
use kpi\tipo_area;
use Illuminate\Http\Request;

class areaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $areas = area::areas();
        $tipo_areas = tipo_area::pluck('tipo','id');
        return view('areas.index', compact('areas','tipo_areas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AreaCreateRequest $request)
    {
        area::create([
            'tipo_area' => $request['tipo_area'],
            'area' => $request['area'],
            'status' => $request['status'],            
        ]);
        
        Session::flash('message', 'Area Creada Correctamente');
        return Redirect::to('areas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $area = area::find($id);
        $tipo_areas = tipo_area::pluck('tipo','id');
        return view('areas.edit',['area'=>$area])->with(compact('tipo_areas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AreaUpdateRequest $request, $id)
    {
        $area = area::find($id)->update($request->all());
        Session::flash('message', 'Area Editada Correctamente');
        return Redirect::to('areas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $area = area::find($id);
        $area->delete();

        Session::flash('message', 'Area Eliminada Correctamente');
        return Redirect::to('areas');
    }
}

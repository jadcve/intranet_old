<?php

namespace kpi\Http\Controllers;

use kpi\Http\Requests;
use kpi\Http\Requests\BenchmarkCreateRequest;
use kpi\Http\Requests\BenchmarkUpdateRequest;
use kpi\Http\Controllers\Controller;
use Session;
use Redirect;
use kpi\benchmark;
use kpi\empresa;
use kpi\clientes;
use Illuminate\Http\Request;
use DB;
use Mail;

class benchmarkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientes = clientes::select(DB::raw("CONCAT(nombre,' ',apellido) AS nombre"),'id')->where('status', 1)->pluck('nombre', 'id');
        $benchmarks = benchmark::benchmarks();

        return view('benchmarks.index')->with(compact('benchmarks','clientes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clientes = clientes::select(DB::raw("CONCAT(nombre,' ',apellido) AS nombre"),'id')->where('status', 1)->pluck('nombre', 'id');
        return view('benchmarks.crear')->with(compact('clientes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BenchmarkCreateRequest $request)
     { 
        $usuarios = $_POST["usuario"];
        $enviar_mail = $request->enviar_mail;
        
        if(!empty($request->path)){
            $file = $request->file('path');
            $nombre_archivo = $file->getClientOriginalName();
            \Storage::disk('archivos')->put($nombre_archivo,  \File::get($file));
        }else{
            $nombre_archivo = NULL;
        }

        if ($enviar_mail == 'SI')
        {
            foreach ($usuarios as $usuario)
            {

                $query = db::insert("INSERT INTO benchmarks (benchmark, descripcion, envio_mail, tipo, path, url, status, created_at, usuario) VALUES ('$request->benchmark','$request->descripcion','$request->enviar_mail','$request->tipo','$nombre_archivo','$request->url','$request->status',NOW(),'{$usuario}')");

                $id_cliente = $usuario;
                $results = DB::select("SELECT email FROM clientes WHERE id = '$id_cliente'");
                $results_mail = $results[0];
                $mail_cliente=$results_mail->email; 

                Mail::send('emails.nuevoBenchmark',$request->all(), function($msj) use ($mail_cliente){
                $msj->subject('Nuevo benchmark disponible - KPI Estudios');
                $msj->to($mail_cliente);
                });                    
            }
            Session::flash('message', 'Benchmark Creado y Correos enviados Correctamente');
            return Redirect::to('benchmarks');
        }else{


                $query = db::insert("INSERT INTO benchmarks (benchmark, descripcion, envio_mail, tipo, path, url, status, created_at, usuario) VALUES ('$request->benchmark','$request->descripcion','NO','$request->tipo','$nombre_archivo','$request->url','$request->status',NOW(),'{$usuarios}')");

            Session::flash('message', 'Benchmark Creado Correctamente');
            return Redirect::to('benchmarks');  
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $benchmark = benchmark::find($id);
        $clientes = clientes::select(DB::raw("CONCAT(nombre,' ',apellido) AS nombre"),'id')->where('status', 1)->pluck('nombre', 'id');
        return view('benchmarks.edit',['benchmark'=>$benchmark])->with(compact('clientes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, BenchmarkUpdateRequest $request)
    {
        $benchmark = benchmark::find($id)->update($request->all());
        Session::flash('message', 'Benchmark Editado Correctamente');
        return Redirect::to('benchmarks');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    $benchmark = benchmark::find($id);
    $benchmark->delete();

    Session::flash('message', 'Benchmark Eliminado Correctamente');
    return Redirect::to('benchmarks');
    }
}

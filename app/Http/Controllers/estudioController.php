<?php

namespace kpi\Http\Controllers;

use kpi\Http\Requests;
use kpi\Http\Requests\EstudioCreateRequest;
use kpi\Http\Requests\EstudioUpdateRequest;
use kpi\Http\Controllers\Controller;
use Session;
use Redirect;
use kpi\estudio;
use kpi\empresa;
use kpi\clientes;
use Illuminate\Http\Request;
use DB;
USE Mail;

class estudioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $clientes = clientes::select(DB::raw("CONCAT(nombre,' ',apellido) AS nombre"),'id')->where('status', 1)->pluck('nombre', 'id');

        $estudios = estudio::Estudios();

        return view('estudios.index')->with(compact('estudios','clientes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $clientes = clientes::select(DB::raw("CONCAT(nombre,' ',apellido) AS nombre"),'id')->where('status', 1)->pluck('nombre', 'id');
        $empresas = empresa::select()->pluck('empresa','id') ;
        return view('estudios.crear')->with(compact('clientes','empresas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EstudioCreateRequest $request)
    {


        $usuarios = $_POST["id_cliente"];
        $enviar_mail = $request->enviar_mail;
        $validar = $request->validar;
        
        if(!empty($request->path)){
            $file = $request->file('path');
            $nombre_archivo = $file->getClientOriginalName();
            \Storage::disk('s3')->put($nombre_archivo,  \File::get($file));

        }else{
            $nombre_archivo = NULL;
        }

        if ($enviar_mail == 'SI')
        {

            $query = db::insert("INSERT INTO estudios (estudio, descripcion, envio_mail, tipo, path, url, status, created_at, usuario) VALUES ('$request->estudio','$request->descripcion','$request->enviar_mail','$request->tipo','$nombre_archivo','$request->url','$request->status',NOW(),'{$usuario}')");

            $id_cliente = $usuario;
            $results = DB::select("SELECT email FROM clientes WHERE id = '$id_cliente'");
            $results_mail = $results[0];
            $mail_cliente=$results_mail->email;

            Mail::send('emails.nuevoEstudio',$request->all(), function($msj) use ($mail_cliente){
                $msj->subject('Nuevo estudio disponible - KPI Estudios');
                $msj->to($mail_cliente);
                });

            Session::flash('message', 'Estudio Creado y Correos enviados Correctamente');
            return Redirect::to('estudios');
        }else{

            $query = db::insert("INSERT INTO estudios (estudio, empresa,descripcion, envio_mail, tipo, path, url, status, carga_datos, created_at, usuario) VALUES ('$request->estudio','$request->empresa','$request->descripcion','NO','$request->tipo','$nombre_archivo','$request->url','$request->status','$request->carga_datos',NOW(),'{$usuarios}')");

            if ($validar == 'SI') // Si se selecciona la opción valdiar
            {
                dd(1990);
            }
            else{   //hasta aquí
                Session::flash('message', 'Estudio Creado Correctamente');
                return Redirect::to('estudios');
            }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $estudio = estudio::find($id);
        //dd($estudio);
        $clientes = clientes::select(DB::raw("CONCAT(nombre,' ',apellido) AS nombre"),'id')->where('status', 1)->pluck('nombre', 'id');
        $empresa = empresa::select('empresa','id')->pluck('empresa','id') ;
        return view('estudios.edit',['estudio'=>$estudio])->with(compact('clientes','empresa'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EstudioUpdateRequest $request, $id)
    {
        $estudio = estudio::find($id)->update($request->all());
        return Redirect::to('estudios');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    $estudio = estudio::find($id);
    $estudio->delete();

    Session::flash('message', 'Estudio Eliminado Correctamente');
    return Redirect::to('estudios');
    }

    public function actualizarEstudios(Request $request)
    {
        $cliente_id = $request->input('cliente');
        $estudios = estudio::estudiosParaSelect($cliente_id);

        return $estudios;
    }
}
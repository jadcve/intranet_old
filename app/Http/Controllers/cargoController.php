<?php

namespace kpi\Http\Controllers;

use kpi\Http\Requests;
use kpi\Http\Requests\CargoCreateRequest;
use kpi\Http\Requests\CargoUpdateRequest;
use kpi\Http\Controllers\Controller;
use Session;
use Redirect;
use kpi\cargo;
use Illuminate\Http\Request;

class cargoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cargos = cargo::all();
        return view('cargos.index', compact('cargos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CargoCreateRequest $request)
    {
        cargo::create([
            'cargo' => $request['cargo'],
            'status' => $request['status'],            
        ]);
        
        Session::flash('message', 'Cargo Creado Correctamente');
        return Redirect::to('/cargos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cargo = cargo::find($id);
        return view('cargos.edit',['cargo'=>$cargo]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, CargoUpdateRequest $request)
    {
        $cargo = cargo::find($id)->update($request->all());
        Session::flash('message', 'Cargo Editado Correctamente');
        return Redirect::to('/cargos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cargo = cargo::find($id);
        $cargo->delete();

        Session::flash('message', 'Cargo Eliminado Correctamente');
        return Redirect::to('cargos');
    }
}

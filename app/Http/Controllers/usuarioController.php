<?php

namespace kpi\Http\Controllers;

use kpi\Http\Requests;
use kpi\Http\Requests\UserCreateRequest;
use kpi\Http\Requests\UserUpdateRequest;
use kpi\Http\Controllers\Controller;
use Maatwebsite\Excel\Excel;
use Session;
use Redirect;
use kpi\User;
use Illuminate\Http\Request;
use Mail;
use Auth;
//use Excel;

class usuarioController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $users = User::all();



        return view('usuarios.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('usuarios.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserCreateRequest $request)
    {
        User::create($request->all());
      /*  $mail_user = $request->email;
          Mail::send('emails.usuarios',$request->all(), function($msj) use ($mail_user){
          $msj->subject('Usuario Intranet KPI Estudios');
          $msj->to($mail_user);
        });
      */
        
        Session::flash('message', 'Usuario Creado Correctamente');
        return Redirect::to('usuarios');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('usuarios.edit',['user'=>$user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, UserUpdateRequest $request)
    {
        $usuario = User::find($id)->update($request->all());
        Session::flash('message', 'Usuario Editado Correctamente');
        return Redirect::to('usuarios');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        Session::flash('message', 'Usuario Eliminado Correctamente');
        return Redirect::to('usuarios');



    }



}

<?php

namespace kpi\Http\Controllers;

use Illuminate\Http\Request;
use kpi\estudio;
use kpi\empresa;
use kpi\clientes;
use kpi\estudio_validaciones;
use kpi\validacion_menor_mayor;
use DB;

class estudio_validacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estudios_validaciones = DB::table('validacion_estudios')
            ->join('clientes','validacion_estudios.cliente_id','=','clientes.id')
            ->join('empresas','clientes.empresa','=','empresas.id')
            ->join('estudios','estudios.id','=','validacion_estudios.estudio_id')
            ->select('validacion_estudios.id','empresas.empresa', 'clientes.nombre', 'clientes.apellido', 'estudios.estudio')
            ->orderBy('validacion_estudios.id', 'desc')
            ->get();


        return view('validaciones.index', compact('estudios_validaciones'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $estudios = estudio::Estudios()->pluck('estudio','id');
        $empresas = empresa::Empresas()->pluck('empresa','id');
        $clientes = clientes::select(DB::raw("CONCAT(nombre,' ',apellido) AS nombre"),'id')->where('status', 1)->pluck('nombre', 'id');


        return view('validaciones.estudios_aux')->with(compact('estudios', 'empresas','clientes'));



    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$validaciones = 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $validacion = estudio_validaciones::findOrfail($id);
        $validacion->delete();
        return redirect('estudio_validate');
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validacion = new estudio_validaciones();
        $validacion->empresa_id = $request->empresa;
        $validacion->estudio_id = $request->estudios;
        $validacion->cliente_id = $request->id_cliente;
       
        if ($request->v1_id_emp == 1){
            $validacion->v1_id_emp = $request->v1_id_emp;
        } else
            $validacion->v1_id_emp = 0;

        if ($request->v2_id_emp == 1){
            $validacion->v2_id_emp = $request->v2_id_emp;
        } else
            $validacion->v2_id_emp = 0;


        if ($request->v3_id_emp == 1){
            $validacion->v3_id_emp = $request->v3_id_emp;
        } else
            $validacion->v3_id_emp = 0;


        if ($request->v4_id_emp == 1){
            $validacion->v4_id_emp = $request->v4_id_emp;
        } else
            $validacion->v4_id_emp = 0;
        
 
        if ($request->v6_id_emp == 1){
            $validacion->v6_id_emp = $request->v6_id_emp;
        } else
            $validacion->v6_id_emp = 0;

        
 
        if ($request->v7_id_emp == 1){
            $validacion->v7_id_emp = $request->v7_id_emp;
        } else
            $validacion->v7_id_emp = 0;


        if ($request->v8_id_emp == 1){
            $validacion->v8_id_emp = $request->v8_id_emp;
        } else
            $validacion->v8_id_emp = 0;

 
        if ($request->v9_id_emp == 1){
            $validacion->v9_id_emp = $request->v9_id_emp;
        } else
            $validacion->v9_id_emp = 0;

        
 
        if ($request->v10_id_emp == 1){
            $validacion->v10_id_emp = $request->v10_id_emp;
        } else
            $validacion->v10_id_emp = 0;

        
 
        if ($request->v11_id_emp == 1){
            $validacion->v11_id_emp = $request->v11_id_emp;
        } else
            $validacion->v11_id_emp = 0;


        if ($request->v12_id_emp == 1){
            $validacion->v12_id_emp = $request->v12_id_emp;
        } else
            $validacion->v12_id_emp = 0;


        if ($request->v12_id_emp == 1){
            $validacion->v12_id_emp = $request->v12_id_emp;
        } else
            $validacion->v12_id_emp = 0;

 
        if ($request->v13_id_emp == 1){
            $validacion->v13_id_emp = $request->v13_id_emp;
        } else
            $validacion->v13_id_emp = 0;

        
 
        if ($request->v16_id_emp == 1){
            $validacion->v16_id_emp = $request->v16_id_emp;
        } else
            $validacion->v16_id_emp = 0;

        
 
        if ($request->v17_id_emp == 1){
            $validacion->v17_id_emp = $request->v17_id_emp;
        } else
            $validacion->v17_id_emp = 0;

        
 
        if ($request->v18_id_emp == 1){
            $validacion->v18_id_emp = $request->v18_id_emp;
        } else
            $validacion->v18_id_emp = 0;

        
 
        if ($request->v19_id_emp == 1){
            $validacion->v19_id_emp = $request->v19_id_emp;
        } else
            $validacion->v19_id_emp = 0;


        if ($request->v20_id_emp == 1){
            $validacion->v20_id_emp = $request->v20_id_emp;
        } else
            $validacion->v20_id_emp= 0;


        if ($request->v21_id_emp== 1){
            $validacion->v21_id_emp= $request->v21_id_emp;
        } else
            $validacion->v21_id_emp= 0;




        /** VALIDACIONES DEL CAMPO NOMBRE **/

        if ($request->v1_nombre == 1){
            $validacion->v1_nombre = $request->v1_nombre;
        } else
            $validacion->v1_nombre = 0;


        if ($request->v2_nombre == 1){
            $validacion->v2_nombre = $request->v2_nombre;
        } else
            $validacion->v2_nombre = 0;


        if ($request->v3_nombre == 1){
            $validacion->v3_nombre = $request->v3_nombre;
        } else
            $validacion->v3_nombre = 0;


        if ($request->v4_nombre == 1){
            $validacion->v4_nombre = $request->v4_nombre;
        } else
            $validacion->v4_nombre = 0;
 
 
        if ($request->v6_nombre == 1){
            $validacion->v6_nombre = $request->v6_nombre;
        } else
            $validacion->v6_nombre = 0;

       
 
        if ($request->v7_nombre == 1){
            $validacion->v7_nombre = $request->v7_nombre;
        } else
            $validacion->v7_nombre = 0;


        if ($request->v8_nombre == 1){
            $validacion->v8_nombre = $request->v8_nombre;
        } else
            $validacion->v8_nombre = 0;
       
 
        if ($request->v9_nombre == 1){
            $validacion->v9_nombre = $request->v9_nombre;
        } else
            $validacion->v9_nombre = 0;

       
 
        if ($request->v10_nombre == 1){
            $validacion->v10_nombre = $request->v10_nombre;
        } else
            $validacion->v10_nombre = 0;

       
 
        if ($request->v11_nombre == 1){
            $validacion->v11_nombre = $request->v11_nombre;
        } else
            $validacion->v11_nombre = 0;


        if ($request->v12_nombre == 1){
            $validacion->v12_nombre = $request->v12_nombre;
        } else
            $validacion->v12_nombre = 0;

        
        if ($request->v12_nombre == 1){
            $validacion->v12_nombre = $request->v12_nombre;
        } else
            $validacion->v12_nombre = 0;

 
        if ($request->v13_nombre == 1){
            $validacion->v13_nombre = $request->v13_nombre;
        } else
            $validacion->v13_nombre = 0;

       
 
        if ($request->v16_nombre == 1){
            $validacion->v16_nombre = $request->v16_nombre;
        } else
            $validacion->v16_nombre = 0;

       
 
        if ($request->v17_nombre == 1){
            $validacion->v17_nombre = $request->v17_nombre;
        } else
            $validacion->v17_nombre = 0;

       
 
        if ($request->v18_nombre == 1){
            $validacion->v18_nombre = $request->v18_nombre;
        } else
            $validacion->v18_nombre = 0;

       
 
        if ($request->v19_nombre == 1){
            $validacion->v19_nombre = $request->v19_nombre;
        } else
            $validacion->v19_nombre = 0;

        if ($request->v20_nombre == 1){
            $validacion->v20_nombre = $request->v20_nombre;
        } else
            $validacion->v20_nombre= 0;

        if ($request->v21_nombre== 1){
            $validacion->v21_nombre= $request->v21_nombre;
        } else
            $validacion->v21_nombre= 0;



        /*******  VALIDACIÓN EMAIL*******/

        if ($request->v1_email == 1){
            $validacion->v1_email = $request->v1_email;
        } else
            $validacion->v1_email = 0;


        if ($request->v2_email == 1){
            $validacion->v2_email = $request->v2_email;
        } else
            $validacion->v2_email = 0;


        if ($request->v3_email == 1){
            $validacion->v3_email = $request->v3_email;
        } else
            $validacion->v3_email = 0;

        if ($request->v4_email == 1){
            $validacion->v4_email = $request->v4_email;
        } else
            $validacion->v4_email = 0;
 
        if ($request->v6_email == 1){
            $validacion->v6_email = $request->v6_email;
        } else
            $validacion->v6_email = 0;

 
        if ($request->v7_email == 1){
            $validacion->v7_email = $request->v7_email;
        } else
            $validacion->v7_email = 0;

        if ($request->v8_email == 1){
            $validacion->v8_email = $request->v8_email;
        } else
            $validacion->v8_email = 0;


        if ($request->v9_email == 1){
            $validacion->v9_email = $request->v9_email;
        } else
            $validacion->v9_email = 0;

 
        if ($request->v10_email == 1){
            $validacion->v10_email = $request->v10_email;
        } else
            $validacion->v10_email = 0;

 
        if ($request->v11_email == 1){
            $validacion->v11_email = $request->v11_email;
        } else
            $validacion->v11_email = 0;


        if ($request->v12_email == 1){
            $validacion->v12_email = $request->v12_email;
        } else
            $validacion->v12_email = 0;

        if ($request->v12_email == 1){
            $validacion->v12_email = $request->v12_email;
        } else
            $validacion->v12_email = 0;

 
        if ($request->v13_email == 1){
            $validacion->v13_email = $request->v13_email;
        } else
            $validacion->v13_email = 0;

 
        if ($request->v16_email == 1){
            $validacion->v16_email = $request->v16_email;
        } else
            $validacion->v16_email = 0;

 
        if ($request->v17_email == 1){
            $validacion->v17_email = $request->v17_email;
        } else
            $validacion->v17_email = 0;

 
        if ($request->v18_email == 1){
            $validacion->v18_email = $request->v18_email;
        } else
            $validacion->v18_email = 0;

 
        if ($request->v19_email == 1){
            $validacion->v19_email = $request->v19_email;
        } else
            $validacion->v19_email = 0;

        if ($request->v20_email == 1){
            $validacion->v20_email = $request->v20_email;
        } else
            $validacion->v20_email= 0;

        if ($request->v21_email== 1){
            $validacion->v21_email= $request->v21_email;
        } else
            $validacion->v21_email= 0;



        /** VALIDACIONES DEL CAMPO GENERO **/
    


        if ($request->v1_genero == 1){
            $validacion->v1_genero = $request->v1_genero;
        } else
            $validacion->v1_genero = 0;


        if ($request->v2_genero == 1){
            $validacion->v2_genero = $request->v2_genero;
        } else
            $validacion->v2_genero = 0;


        if ($request->v3_genero == 1){
            $validacion->v3_genero = $request->v3_genero;
        } else
            $validacion->v3_genero = 0;

        if ($request->v4_genero == 1){
            $validacion->v4_genero = $request->v4_genero;
        } else
            $validacion->v4_genero = 0;

 
        if ($request->v6_genero == 1){
            $validacion->v6_genero = $request->v6_genero;
        } else
            $validacion->v6_genero = 0;


 
        if ($request->v7_genero == 1){
            $validacion->v7_genero = $request->v7_genero;
        } else
            $validacion->v7_genero = 0;



        if ($request->v8_genero == 1){
            $validacion->v8_genero = $request->v8_genero;
        } else
            $validacion->v8_genero = 0;
 


        if ($request->v9_genero == 1){
            $validacion->v9_genero = $request->v9_genero;
        } else
            $validacion->v9_genero = 0;


 
        if ($request->v10_genero == 1){
            $validacion->v10_genero = $request->v10_genero;
        } else
            $validacion->v10_genero = 0;


 
        if ($request->v11_genero == 1){
            $validacion->v11_genero = $request->v11_genero;
        } else
            $validacion->v11_genero = 0;


        if ($request->v12_genero == 1){
            $validacion->v12_genero = $request->v12_genero;
        } else
            $validacion->v12_genero = 0;


        if ($request->v12_genero == 1){
            $validacion->v12_genero = $request->v12_genero;
        } else
            $validacion->v12_genero = 0;

 
        if ($request->v13_genero == 1){
            $validacion->v13_genero = $request->v13_genero;
        } else
            $validacion->v13_genero = 0;


 
        if ($request->v16_genero == 1){
            $validacion->v16_genero = $request->v16_genero;
        } else
            $validacion->v16_genero = 0;


 
        if ($request->v17_genero == 1){
            $validacion->v17_genero = $request->v17_genero;
        } else
            $validacion->v17_genero = 0;


 
        if ($request->v18_genero == 1){
            $validacion->v18_genero = $request->v18_genero;
        } else
            $validacion->v18_genero = 0;


 
        if ($request->v19_genero == 1){
            $validacion->v19_genero = $request->v19_genero;
        } else
            $validacion->v19_genero = 0;

        if ($request->v20_genero == 1){
            $validacion->v20_genero = $request->v20_genero;
        } else
            $validacion->v20_genero= 0;

        if ($request->v21_genero== 1){
            $validacion->v21_genero= $request->v21_genero;
        } else
            $validacion->v21_genero= 0;




        /** VALIDACIONES DEL CAMPO FECHA NACIMIENTO **/
        
        if ($request->v1_fec_nac == 1){
            $validacion->v1_fec_nac = $request->v1_fec_nac;
        } else
            $validacion->v1_fec_nac = 0;


        if ($request->v2_fec_nac == 1){
            $validacion->v2_fec_nac = $request->v2_fec_nac;
        } else
            $validacion->v2_fec_nac = 0;

        if ($request->v3_fec_nac == 1){
            $validacion->v3_fec_nac = $request->v3_fec_nac;
        } else
            $validacion->v3_fec_nac = 0;


        if ($request->v4_fec_nac == 1){
            $validacion->v4_fec_nac = $request->v4_fec_nac;
        } else
            $validacion->v4_fec_nac = 0;


        if ($request->v6_fec_nac == 1){
            $validacion->v6_fec_nac = $request->v6_fec_nac;
        } else
            $validacion->v6_fec_nac = 0;

         
        if ($request->v7_fec_nac == 1){
            $validacion->v7_fec_nac = $request->v7_fec_nac;
        } else
            $validacion->v7_fec_nac = 0;



        if ($request->v8_fec_nac == 1){
            $validacion->v8_fec_nac = $request->v8_fec_nac;
        } else
            $validacion->v8_fec_nac = 0;

         
         
        if ($request->v9_fec_nac == 1){
            $validacion->v9_fec_nac = $request->v9_fec_nac;
        } else
            $validacion->v9_fec_nac = 0;

         
        if ($request->v10_fec_nac == 1){
            $validacion->v10_fec_nac = $request->v10_fec_nac;
        } else
            $validacion->v10_fec_nac = 0;

         
        if ($request->v11_fec_nac == 1){
            $validacion->v11_fec_nac = $request->v11_fec_nac;
        } else
            $validacion->v11_fec_nac = 0;


        if ($request->v12_fec_nac == 1){
            $validacion->v12_fec_nac = $request->v12_fec_nac;
        } else
            $validacion->v12_fec_nac = 0;

        
        if ($request->v12_fec_nac == 1){
            $validacion->v12_fec_nac = $request->v12_fec_nac;
        } else
            $validacion->v12_fec_nac = 0;

         
        if ($request->v13_fec_nac == 1){
            $validacion->v13_fec_nac = $request->v13_fec_nac;
        } else
            $validacion->v13_fec_nac = 0;

         
        if ($request->v16_fec_nac == 1){
            $validacion->v16_fec_nac = $request->v16_fec_nac;
        } else
            $validacion->v16_fec_nac = 0;

         
        if ($request->v17_fec_nac == 1){
            $validacion->v17_fec_nac = $request->v17_fec_nac;
        } else
            $validacion->v17_fec_nac = 0;

         
        if ($request->v18_fec_nac == 1){
            $validacion->v18_fec_nac = $request->v18_fec_nac;
        } else
            $validacion->v18_fec_nac = 0;

         
        if ($request->v19_fec_nac == 1){
            $validacion->v19_fec_nac = $request->v19_fec_nac;
        } else
            $validacion->v19_fec_nac = 0;

        if ($request->v20_fec_nac == 1){
            $validacion->v20_fec_nac = $request->v20_fec_nac;
        } else
            $validacion->v20_fec_nac= 0;

        if ($request->v21_fec_nac== 1){
            $validacion->v21_fec_nac= $request->v21_fec_nac;
        } else
            $validacion->v21_fec_nac= 0;




        /** VALIDACIONES DEL CAMPO FECHA DE INGRESO **/
        

        if ($request->v1_fec_ing == 1){
            $validacion->v1_fec_ing = $request->v1_fec_ing;
        } else
            $validacion->v1_fec_ing = 0;


        if ($request->v2_fec_ing == 1){
            $validacion->v2_fec_ing = $request->v2_fec_ing;
        } else
            $validacion->v2_fec_ing = 0;


        if ($request->v3_fec_ing == 1){
            $validacion->v3_fec_ing = $request->v3_fec_ing;
        } else
            $validacion->v3_fec_ing = 0;


        if ($request->v4_fec_ing == 1){
            $validacion->v4_fec_ing = $request->v4_fec_ing;
        } else
            $validacion->v4_fec_ing = 0;


         
        if ($request->v6_fec_ing == 1){
            $validacion->v6_fec_ing = $request->v6_fec_ing;
        } else
            $validacion->v6_fec_ing = 0;

        
         
        if ($request->v7_fec_ing == 1){
            $validacion->v7_fec_ing = $request->v7_fec_ing;
        } else
            $validacion->v7_fec_ing = 0;



        if ($request->v8_fec_ing == 1){
            $validacion->v8_fec_ing = $request->v8_fec_ing;
        } else
            $validacion->v8_fec_ing = 0;


         
        if ($request->v9_fec_ing == 1){
            $validacion->v9_fec_ing = $request->v9_fec_ing;
        } else
            $validacion->v9_fec_ing = 0;

        
         
        if ($request->v10_fec_ing == 1){
            $validacion->v10_fec_ing = $request->v10_fec_ing;
        } else
            $validacion->v10_fec_ing = 0;

        
         
        if ($request->v11_fec_ing == 1){
            $validacion->v11_fec_ing = $request->v11_fec_ing;
        } else
            $validacion->v11_fec_ing = 0;


        if ($request->v12_fec_ing == 1){
            $validacion->v12_fec_ing = $request->v12_fec_ing;
        } else
            $validacion->v12_fec_ing = 0;
        

        if ($request->v12_fec_ing == 1){
            $validacion->v12_fec_ing = $request->v12_fec_ing;
        } else
            $validacion->v12_fec_ing = 0;
        
         
        if ($request->v13_fec_ing == 1){
            $validacion->v13_fec_ing = $request->v13_fec_ing;
        } else
            $validacion->v13_fec_ing = 0;

        
         
        if ($request->v16_fec_ing == 1){
            $validacion->v16_fec_ing = $request->v16_fec_ing;
        } else
            $validacion->v16_fec_ing = 0;

        
         
        if ($request->v17_fec_ing == 1){
            $validacion->v17_fec_ing = $request->v17_fec_ing;
        } else
            $validacion->v17_fec_ing = 0;

        
         
        if ($request->v18_fec_ing == 1){
            $validacion->v18_fec_ing = $request->v18_fec_ing;
        } else
            $validacion->v18_fec_ing = 0;

        
         
        if ($request->v19_fec_ing == 1){
            $validacion->v19_fec_ing = $request->v19_fec_ing;
        } else
            $validacion->v19_fec_ing = 0;

        if ($request->v20_fec_ing == 1){
            $validacion->v20_fec_ing = $request->v20_fec_ing;
        } else
            $validacion->v20_fec_ing= 0;

        if ($request->v21_fec_ing== 1){
            $validacion->v21_fec_ing= $request->v21_fec_ing;
        } else
            $validacion->v21_fec_ing= 0;




        /** VALIDACIONES DEL CAMPO ANTIGUEDAD **/
        

        if ($request->v1_antiguedad == 1){
            $validacion->v1_antiguedad = $request->v1_antiguedad;
        } else
            $validacion->v1_antiguedad = 0;


        if ($request->v2_antiguedad == 1){
            $validacion->v2_antiguedad = $request->v2_antiguedad;
        } else
            $validacion->v2_antiguedad = 0;


        if ($request->v3_antiguedad == 1){
            $validacion->v3_antiguedad = $request->v3_antiguedad;
        } else
            $validacion->v3_antiguedad = 0;


        if ($request->v4_antiguedad == 1){
            $validacion->v4_antiguedad = $request->v4_antiguedad;
        } else
            $validacion->v4_antiguedad = 0;

         
        if ($request->v6_antiguedad == 1){
            $validacion->v6_antiguedad = $request->v6_antiguedad;
        } else
            $validacion->v6_antiguedad = 0;

         
        if ($request->v7_antiguedad == 1){
            $validacion->v7_antiguedad = $request->v7_antiguedad;
        } else
            $validacion->v7_antiguedad = 0;

        
        if ($request->v8_antiguedad == 1){
            $validacion->v8_antiguedad = $request->v8_antiguedad;
        } else
            $validacion->v8_antiguedad = 0;

         
        if ($request->v9_antiguedad == 1){
            $validacion->v9_antiguedad = $request->v9_antiguedad;
        } else
            $validacion->v9_antiguedad = 0;

         
        if ($request->v10_antiguedad == 1){
            $validacion->v10_antiguedad = $request->v10_antiguedad;
        } else
            $validacion->v10_antiguedad = 0;

         
        if ($request->v11_antiguedad == 1){
            $validacion->v11_antiguedad = $request->v11_antiguedad;
        } else
            $validacion->v11_antiguedad = 0;


        if ($request->v12_antiguedad == 1){
            $validacion->v12_antiguedad = $request->v12_antiguedad;
        } else
            $validacion->v12_antiguedad = 0;

        if ($request->v12_antiguedad == 1){
            $validacion->v12_antiguedad = $request->v12_antiguedad;
        } else
            $validacion->v12_antiguedad = 0;

         
        if ($request->v13_antiguedad == 1){
            $validacion->v13_antiguedad = $request->v13_antiguedad;
        } else
            $validacion->v13_antiguedad = 0;

         
        if ($request->v16_antiguedad == 1){
            $validacion->v16_antiguedad = $request->v16_antiguedad;
        } else
            $validacion->v16_antiguedad = 0;

         
        if ($request->v17_antiguedad == 1){
            $validacion->v17_antiguedad = $request->v17_antiguedad;
        } else
            $validacion->v17_antiguedad = 0;

         
        if ($request->v18_antiguedad == 1){
            $validacion->v18_antiguedad = $request->v18_antiguedad;
        } else
            $validacion->v18_antiguedad = 0;

         
        if ($request->v19_antiguedad == 1){
            $validacion->v19_antiguedad = $request->v19_antiguedad;
        } else
            $validacion->v19_antiguedad = 0;

        if ($request->v20_antiguedad == 1){
            $validacion->v20_antiguedad = $request->v20_antiguedad;
        } else
            $validacion->v20_antiguedad= 0;

        if ($request->v21_antiguedad== 1){
            $validacion->v21_antiguedad= $request->v21_antiguedad;
        } else
            $validacion->v21_antiguedad= 0;




        /** VALIDACIONES DEL CAMPO EDAD **/



        if ($request->v1_edad == 1){
            $validacion->v1_edad = $request->v1_edad;
        } else
            $validacion->v1_edad = 0;


        if ($request->v2_edad == 1){
            $validacion->v2_edad = $request->v2_edad;
        } else
            $validacion->v2_edad = 0;


        if ($request->v3_edad == 1){
            $validacion->v3_edad = $request->v3_edad;
        } else
            $validacion->v3_edad = 0;


        if ($request->v4_edad == 1){
            $validacion->v4_edad = $request->v4_edad;
        } else
            $validacion->v4_edad = 0;

         
        if ($request->v6_edad == 1){
            $validacion->v6_edad = $request->v6_edad;
        } else
            $validacion->v6_edad = 0;

        
         
        if ($request->v7_edad == 1){
            $validacion->v7_edad = $request->v7_edad;
        } else
            $validacion->v7_edad = 0;


        
        if ($request->v8_edad == 1){
            $validacion->v8_edad = $request->v8_edad;
        } else
            $validacion->v8_edad = 0;

        

        if ($request->v9_edad == 1){
            $validacion->v9_edad = $request->v9_edad;
        } else
            $validacion->v9_edad = 0;

        
         
        if ($request->v10_edad == 1){
            $validacion->v10_edad = $request->v10_edad;
        } else
            $validacion->v10_edad = 0;

        
         
        if ($request->v11_edad == 1){
            $validacion->v11_edad = $request->v11_edad;
        } else
            $validacion->v11_edad = 0;


        if ($request->v12_edad == 1){
            $validacion->v12_edad = $request->v12_edad;
        } else
            $validacion->v12_edad = 0;


        if ($request->v12_edad == 1){
            $validacion->v12_edad = $request->v12_edad;
        } else
            $validacion->v12_edad = 0;
        
         
        if ($request->v13_edad == 1){
            $validacion->v13_edad = $request->v13_edad;
        } else
            $validacion->v13_edad = 0;

        
         
        if ($request->v16_edad == 1){
            $validacion->v16_edad = $request->v16_edad;
        } else
            $validacion->v16_edad = 0;

        
         
        if ($request->v17_edad == 1){
            $validacion->v17_edad = $request->v17_edad;
        } else
            $validacion->v17_edad = 0;

        
         
        if ($request->v18_edad == 1){
            $validacion->v18_edad = $request->v18_edad;
        } else
            $validacion->v18_edad = 0;

        
         
        if ($request->v19_edad == 1){
            $validacion->v19_edad = $request->v19_edad;
        } else
            $validacion->v19_edad = 0;

        if ($request->v20_edad == 1){
            $validacion->v20_edad = $request->v20_edad;
        } else
            $validacion->v20_edad= 0;

        if ($request->v21_edad== 1){
            $validacion->v21_edad= $request->v21_edad;
        } else
            $validacion->v21_edad= 0;






        /** VALIDACIONES DEL CAMPO EDAD CATEGORIZADA **/


        if ($request->v1_edad_cat == 1){
            $validacion->v1_edad_cat = $request->v1_edad_cat;
        } else
            $validacion->v1_edad_cat = 0;


        if ($request->v2_edad_cat == 1){
            $validacion->v2_edad_cat = $request->v2_edad_cat;
        } else
            $validacion->v2_edad_cat = 0;


        if ($request->v3_edad_cat == 1){
            $validacion->v3_edad_cat = $request->v3_edad_cat;
        } else
            $validacion->v3_edad_cat = 0;

        if ($request->v4_edad_cat == 1){
            $validacion->v4_edad_cat = $request->v4_edad_cat;
        } else
            $validacion->v4_edad_cat = 0;

         
        if ($request->v6_edad_cat == 1){
            $validacion->v6_edad_cat = $request->v6_edad_cat;
        } else
            $validacion->v6_edad_cat = 0;

                
        if ($request->v7_edad_cat == 1){
            $validacion->v7_edad_cat = $request->v7_edad_cat;
        } else
            $validacion->v7_edad_cat = 0;



        if ($request->v8_edad_cat == 1){
            $validacion->v8_edad_cat = $request->v8_edad_cat;
        } else
            $validacion->v8_edad_cat = 0;

       

         
        if ($request->v9_edad_cat == 1){
            $validacion->v9_edad_cat = $request->v9_edad_cat;
        } else
            $validacion->v9_edad_cat = 0;

                
        if ($request->v10_edad_cat == 1){
            $validacion->v10_edad_cat = $request->v10_edad_cat;
        } else
            $validacion->v10_edad_cat = 0; 


         
        if ($request->v11_edad_cat == 1){
            $validacion->v11_edad_cat = $request->v11_edad_cat;
        } else
            $validacion->v11_edad_cat = 0;

        
        if ($request->v12_edad_cat == 1){
            $validacion->v12_edad_cat = $request->v12_edad_cat;
        } else
            $validacion->v12_edad_cat = 0;

     
        if ($request->v13_edad_cat == 1){
            $validacion->v13_edad_cat = $request->v13_edad_cat;
        } else
            $validacion->v13_edad_cat = 0;
        
         
        if ($request->v16_edad_cat == 1){
            $validacion->v16_edad_cat = $request->v16_edad_cat;
        } else
            $validacion->v16_edad_cat = 0;       
        
         
        if ($request->v17_edad_cat == 1){
            $validacion->v17_edad_cat = $request->v17_edad_cat;
        } else
            $validacion->v17_edad_cat = 0;

    
        if ($request->v18_edad_cat == 1){
            $validacion->v18_edad_cat = $request->v18_edad_cat;
        } else
            $validacion->v18_edad_cat = 0;    
         
        if ($request->v19_edad_cat == 1){
            $validacion->v19_edad_cat = $request->v19_edad_cat;
        } else
            $validacion->v19_edad_cat = 0;

        if ($request->v20_edad_cat == 1){
            $validacion->v20_edad_cat = $request->v20_edad_cat;
        } else
            $validacion->v20_edad_cat= 0;

        if ($request->v21_edad_cat== 1){
            $validacion->v21_edad_cat= $request->v21_edad_cat;
        } else
            $validacion->v21_edad_cat= 0;



        /*** VALIDACIÓN CAMPO GENERACIÓN  ***/
 
        if ($request->v1_generacion == 1){
            $validacion->v1_generacion = $request->v1_generacion;
        } else
            $validacion->v1_generacion = 0;

        if ($request->v2_generacion == 1){
            $validacion->v2_generacion = $request->v2_generacion;
        } else
            $validacion->v2_generacion = 0;

        if ($request->v3_generacion == 1){
            $validacion->v3_generacion = $request->v3_generacion;
        } else
            $validacion->v3_generacion = 0;


        if ($request->v4_generacion == 1){
            $validacion->v4_generacion = $request->v4_generacion;
        } else
            $validacion->v4_generacion = 0;

                 
        if ($request->v6_generacion == 1){
            $validacion->v6_generacion = $request->v6_generacion;
        } else
            $validacion->v6_generacion = 0;

                 
        if ($request->v7_generacion == 1){
            $validacion->v7_generacion = $request->v7_generacion;
        } else
            $validacion->v7_generacion = 0;

        if ($request->v8_generacion == 1){
            $validacion->v8_generacion = $request->v8_generacion;
        } else
            $validacion->v8_generacion = 0;
         
        if ($request->v9_generacion == 1){
            $validacion->v9_generacion = $request->v9_generacion;
        } else
            $validacion->v9_generacion = 0;

                 
        if ($request->v10_generacion == 1){
            $validacion->v10_generacion = $request->v10_generacion;
        } else
            $validacion->v10_generacion = 0;

                 
        if ($request->v11_generacion == 1){
            $validacion->v11_generacion = $request->v11_generacion;
        } else
            $validacion->v11_generacion = 0;


        if ($request->v12_generacion == 1){
            $validacion->v12_generacion = $request->v12_generacion;
        } else
            $validacion->v12_generacion = 0;
                 
        if ($request->v13_generacion == 1){
            $validacion->v13_generacion = $request->v13_generacion;
        } else
            $validacion->v13_generacion = 0;

                 
        if ($request->v16_generacion == 1){
            $validacion->v16_generacion = $request->v16_generacion;
        } else
            $validacion->v16_generacion = 0;

                 
        if ($request->v17_generacion == 1){
            $validacion->v17_generacion = $request->v17_generacion;
        } else
            $validacion->v17_generacion = 0;

                 
        if ($request->v18_generacion == 1){
            $validacion->v18_generacion = $request->v18_generacion;
        } else
            $validacion->v18_generacion = 0;

                 
        if ($request->v19_generacion == 1){
            $validacion->v19_generacion = $request->v19_generacion;
        } else
            $validacion->v19_generacion = 0;

        if ($request->v20_generacion == 1){
            $validacion->v20_generacion = $request->v20_generacion;
        } else
            $validacion->v20_generacion= 0;

        if ($request->v21_generacion== 1){
            $validacion->v21_generacion= $request->v21_generacion;
        } else
            $validacion->v21_generacion= 0;







        /** VALIDACIONES DEL CAMPO ANTIGUEDAD CATEGORIZADA **/
  

        if ($request->v1_ant_cat == 1){
            $validacion->v1_ant_cat = $request->v1_ant_cat;
        } else
            $validacion->v1_ant_cat = 0;

        if ($request->v2_ant_cat == 1){
            $validacion->v2_ant_cat = $request->v2_ant_cat;
        } else
            $validacion->v2_ant_cat = 0;

        if ($request->v3_ant_cat == 1){
            $validacion->v3_ant_cat = $request->v3_ant_cat;
        } else
            $validacion->v3_ant_cat = 0;


        if ($request->v4_ant_cat == 1){
            $validacion->v4_ant_cat = $request->v4_ant_cat;
        } else
            $validacion->v4_ant_cat = 0;

         
        if ($request->v6_ant_cat == 1){
            $validacion->v6_ant_cat = $request->v6_ant_cat;
        } else
            $validacion->v6_ant_cat = 0;

         
        if ($request->v7_ant_cat == 1){
            $validacion->v7_ant_cat = $request->v7_ant_cat;
        } else
            $validacion->v7_ant_cat = 0;

        
        if ($request->v8_ant_cat == 1){
            $validacion->v8_ant_cat = $request->v8_ant_cat;
        } else
            $validacion->v8_ant_cat = 0;

         
        if ($request->v9_ant_cat == 1){
            $validacion->v9_ant_cat = $request->v9_ant_cat;
        } else
            $validacion->v9_ant_cat = 0;

         
        if ($request->v10_ant_cat == 1){
            $validacion->v10_ant_cat = $request->v10_ant_cat;
        } else
            $validacion->v10_ant_cat = 0;

         
        if ($request->v11_ant_cat == 1){
            $validacion->v11_ant_cat = $request->v11_ant_cat;
        } else
            $validacion->v11_ant_cat = 0;


        if ($request->v12_ant_cat == 1){
            $validacion->v12_ant_cat = $request->v12_ant_cat;
        } else
            $validacion->v12_ant_cat = 0;
        

        if ($request->v13_ant_cat == 1){
            $validacion->v13_ant_cat = $request->v13_ant_cat;
        } else
            $validacion->v13_ant_cat = 0;

         
        if ($request->v16_ant_cat == 1){
            $validacion->v16_ant_cat = $request->v16_ant_cat;
        } else
            $validacion->v16_ant_cat = 0;

         
        if ($request->v17_ant_cat == 1){
            $validacion->v17_ant_cat = $request->v17_ant_cat;
        } else
            $validacion->v17_ant_cat = 0;

         
        if ($request->v18_ant_cat == 1){
            $validacion->v18_ant_cat = $request->v18_ant_cat;
        } else
            $validacion->v18_ant_cat = 0;

         
        if ($request->v19_ant_cat == 1){
            $validacion->v19_ant_cat = $request->v19_ant_cat;
        } else
            $validacion->v19_ant_cat = 0;



        if ($request->v20_ant_cat == 1){
            $validacion->v20_ant_cat = $request->v20_ant_cat;
        } else
            $validacion->v20_ant_cat= 0;

        if ($request->v21_ant_cat== 1){
            $validacion->v21_ant_cat= $request->v21_ant_cat;
        } else
            $validacion->v21_ant_cat= 0;





        /** VALIDACIONES DEL CAMPO ANTIGUEDAD < 1 AÑO **/

        if ($request->v1_ant_menor == 1){
            $validacion->v1_ant_menor = $request->v1_ant_menor;
        } else
            $validacion->v1_ant_menor = 0;

        if ($request->v2_ant_menor == 1){
            $validacion->v2_ant_menor = $request->v2_ant_menor;
        } else
            $validacion->v2_ant_menor = 0;

        if ($request->v3_ant_menor == 1){
            $validacion->v3_ant_menor = $request->v3_ant_menor;
        } else
            $validacion->v3_ant_menor = 0;

        if ($request->v4_ant_menor == 1){
            $validacion->v4_ant_menor = $request->v4_ant_menor;
        } else
            $validacion->v4_ant_menor = 0;


        if ($request->v6_ant_menor == 1){
            $validacion->v6_ant_menor = $request->v6_ant_menor;
        } else
            $validacion->v6_ant_menor = 0;

         
        if ($request->v7_ant_menor == 1){
            $validacion->v7_ant_menor = $request->v7_ant_menor;
        } else
            $validacion->v7_ant_menor = 0;

        if ($request->v8_ant_menor == 1){
            $validacion->v8_ant_menor = $request->v8_ant_menor;
        } else
            $validacion->v8_ant_menor = 0;
         
        if ($request->v9_ant_menor == 1){
            $validacion->v9_ant_menor = $request->v9_ant_menor;
        } else
            $validacion->v9_ant_menor = 0;

         
        if ($request->v10_ant_menor == 1){
            $validacion->v10_ant_menor = $request->v10_ant_menor;
        } else
            $validacion->v10_ant_menor = 0;

         
        if ($request->v11_ant_menor == 1){
            $validacion->v11_ant_menor = $request->v11_ant_menor;
        } else
            $validacion->v11_ant_menor = 0;


        if ($request->v12_ant_menor == 1){
            $validacion->v12_ant_menor = $request->v12_ant_menor;
        } else
            $validacion->v12_ant_menor = 0;

 
        if ($request->v13_ant_menor == 1){
            $validacion->v13_ant_menor = $request->v13_ant_menor;
        } else
            $validacion->v13_ant_menor = 0;

         
        if ($request->v16_ant_menor == 1){
            $validacion->v16_ant_menor = $request->v16_ant_menor;
        } else
            $validacion->v16_ant_menor = 0;

         
        if ($request->v17_ant_menor == 1){
            $validacion->v17_ant_menor = $request->v17_ant_menor;
        } else
            $validacion->v17_ant_menor = 0;

         
        if ($request->v18_ant_menor == 1){
            $validacion->v18_ant_menor = $request->v18_ant_menor;
        } else
            $validacion->v18_ant_menor = 0;

         
        if ($request->v19_ant_menor == 1){
            $validacion->v19_ant_menor = $request->v19_ant_menor;
        } else
            $validacion->v19_ant_menor = 0;

        if ($request->v20_ant_menor == 1){
            $validacion->v20_ant_menor = $request->v20_ant_menor;
        } else
            $validacion->v20_ant_menor= 0;

        if ($request->v21_ant_menor== 1){
            $validacion->v21_ant_menor= $request->v21_ant_menor;
        } else
            $validacion->v21_ant_menor= 0;






        /** VALIDACIONES DEL TIPO CARGO **/


        if ($request->v1_t_cargo== 1){
            $validacion->v1_t_cargo = $request->v1_t_cargo;
        } else
            $validacion->v1_t_cargo = 0;
         
        if ($request->v2_t_cargo== 1){
            $validacion->v2_t_cargo = $request->v2_t_cargo;
        } else
            $validacion->v2_t_cargo = 0;


        if ($request->v3_t_cargo== 1){
            $validacion->v3_t_cargo = $request->v3_t_cargo;
        } else
            $validacion->v3_t_cargo = 0;


        if ($request->v4_t_cargo== 1){
            $validacion->v4_t_cargo = $request->v4_t_cargo;
        } else
            $validacion->v4_t_cargo = 0;

        if ($request->v6_t_cargo== 1){
            $validacion->v6_t_cargo = $request->v6_t_cargo;
        } else
            $validacion->v6_t_cargo = 0;

        if ($request->v7_t_cargo== 1){
            $validacion->v7_t_cargo = $request->v7_t_cargo;
        } else
            $validacion->v7_t_cargo = 0;


         if ($request->v8_t_cargo== 1){
            $validacion->v8_t_cargo = $request->v8_t_cargo;
        } else
            $validacion->v8_t_cargo = 0;

         
        if ($request->v9_t_cargo== 1){
            $validacion->v9_t_cargo = $request->v9_t_cargo;
        } else
            $validacion->v9_t_cargo = 0;

         
        if ($request->v10_t_cargo== 1){
            $validacion->v10_t_cargo = $request->v10_t_cargo;
        } else
            $validacion->v10_t_cargo = 0;

         
        if ($request->v11_t_cargo== 1){
            $validacion->v11_t_cargo = $request->v11_t_cargo;
        } else
            $validacion->v11_t_cargo = 0;


        if ($request->v12_t_cargo== 1){
            $validacion->v12_t_cargo = $request->v12_t_cargo;
        } else
            $validacion->v12_t_cargo = 0;
 

        if ($request->v13_t_cargo== 1){
            $validacion->v13_t_cargo = $request->v13_t_cargo;
        } else
            $validacion->v13_t_cargo = 0;

         
        if ($request->v16_t_cargo== 1){
            $validacion->v16_t_cargo = $request->v16_t_cargo;
        } else
            $validacion->v16_t_cargo = 0;

         
        if ($request->v17_t_cargo== 1){
            $validacion->v17_t_cargo = $request->v17_t_cargo;
        } else
            $validacion->v17_t_cargo = 0;

         
        if ($request->v18_t_cargo== 1){
            $validacion->v18_t_cargo = $request->v18_t_cargo;
        } else
            $validacion->v18_t_cargo = 0;

         
        if ($request->v19_t_cargo== 1){
            $validacion->v19_t_cargo = $request->v19_t_cargo;
        } else
            $validacion->v19_t_cargo = 0;

        if ($request->v20_t_cargo == 1){
            $validacion->v20_t_cargo = $request->v20_t_cargo;
        } else
            $validacion->v20_t_cargo= 0;

        if ($request->v21_t_cargo== 1){
            $validacion->v21_t_cargo= $request->v21_t_cargo;
        } else
            $validacion->v21_t_cargo= 0;

        /********** VALIDACIONES CARGO *********************/

        if ($request->v1_cargo== 1){
            $validacion->v1_cargo= $request->v1_cargo;
        } else
            $validacion->v1_cargo = 0;

        if ($request->v2_cargo== 1){
            $validacion->v2_cargo= $request->v2_cargo;
        } else
            $validacion->v2_cargo= 0;


        if ($request->v3_cargo== 1){
            $validacion->v3_cargo = $request->v3_cargo;
        } else
            $validacion->v3_cargo = 0;


        if ($request->v4_cargo== 1){
            $validacion->v4_cargo= $request->v4_cargo;
        } else
            $validacion->v4_cargo= 0;

        if ($request->v6_cargo== 1){
            $validacion->v6_cargo= $request->v6_cargo;
        } else
            $validacion->v6_cargo = 0;

        if ($request->v7_cargo== 1){
            $validacion->v7_cargo = $request->v7_cargo;
        } else
            $validacion->v7_cargo = 0;


        if ($request->v8_cargo== 1){
            $validacion->v8_cargo = $request->v8_cargo;
        } else
            $validacion->v8_cargo = 0;


        if ($request->v9_cargo== 1){
            $validacion->v9_cargo = $request->v9_cargo;
        } else
            $validacion->v9_cargo = 0;


        if ($request->v10_cargo== 1){
            $validacion->v10_cargo = $request->v10_cargo;
        } else
            $validacion->v10_cargo = 0;


        if ($request->v11_cargo== 1){
            $validacion->v11_cargo = $request->v11_cargo;
        } else
            $validacion->v11_cargo = 0;


        if ($request->v12_cargo== 1){
            $validacion->v12_cargo = $request->v12_cargo;
        } else
            $validacion->v12_cargo = 0;


        if ($request->v13_cargo== 1){
            $validacion->v13_cargo = $request->v13_cargo;
        } else
            $validacion->v13_cargo = 0;


        if ($request->v16_cargo== 1){
            $validacion->v16_cargo = $request->v16_cargo;
        } else
            $validacion->v16_cargo = 0;


        if ($request->v17_cargo== 1){
            $validacion->v17_cargo = $request->v17_cargo;
        } else
            $validacion->v17_cargo = 0;


        if ($request->v18_cargo== 1){
            $validacion->v18_cargo = $request->v18_cargo;
        } else
            $validacion->v18_cargo = 0;


        if ($request->v19_cargo== 1){
            $validacion->v19_cargo = $request->v19_cargo;
        } else
            $validacion->v19_cargo = 0;

        if ($request->v20_cargo == 1){
            $validacion->v20_cargo = $request->v20_cargo;
        } else
            $validacion->v20_cargo= 0;

        if ($request->v21_cargo== 1){
            $validacion->v21_cargo= $request->v21_cargo;
        } else
            $validacion->v21_cargo= 0;


        /********** VALIDACIONES CARGO 2*********************/


        if ($request->v1_cargo2== 1){
            $validacion->v1_cargo2= $request->v1_cargo2;
        } else
            $validacion->v1_cargo2 = 0;

        if ($request->v2_cargo2== 1){
            $validacion->v2_cargo2= $request->v2_cargo2;
        } else
            $validacion->v2_cargo2= 0;


        if ($request->v3_cargo2== 1){
            $validacion->v3_cargo2 = $request->v3_cargo2;
        } else
            $validacion->v3_cargo2 = 0;


        if ($request->v4_cargo2== 1){
            $validacion->v4_cargo2= $request->v4_cargo2;
        } else
            $validacion->v4_cargo2= 0;

        if ($request->v6_cargo2== 1){
            $validacion->v6_cargo2= $request->v6_cargo2;
        } else
            $validacion->v6_cargo2 = 0;

        if ($request->v7_cargo2== 1){
            $validacion->v7_cargo2 = $request->v7_cargo2;
        } else
            $validacion->v7_cargo2 = 0;


        if ($request->v8_cargo2== 1){
            $validacion->v8_cargo2 = $request->v8_cargo2;
        } else
            $validacion->v8_cargo2 = 0;


        if ($request->v9_cargo2== 1){
            $validacion->v9_cargo2 = $request->v9_cargo2;
        } else
            $validacion->v9_cargo2 = 0;


        if ($request->v10_cargo2== 1){
            $validacion->v10_cargo2 = $request->v10_cargo2;
        } else
            $validacion->v10_cargo2 = 0;


        if ($request->v11_cargo2== 1){
            $validacion->v11_cargo2 = $request->v11_cargo2;
        } else
            $validacion->v11_cargo2 = 0;


        if ($request->v12_cargo2== 1){
            $validacion->v12_cargo2 = $request->v12_cargo2;
        } else
            $validacion->v12_cargo2 = 0;


        if ($request->v13_cargo2== 1){
            $validacion->v13_cargo2 = $request->v13_cargo2;
        } else
            $validacion->v13_cargo2 = 0;


        if ($request->v16_cargo2== 1){
            $validacion->v16_cargo2 = $request->v16_cargo2;
        } else
            $validacion->v16_cargo2 = 0;


        if ($request->v17_cargo2== 1){
            $validacion->v17_cargo2 = $request->v17_cargo2;
        } else
            $validacion->v17_cargo2 = 0;


        if ($request->v18_cargo2== 1){
            $validacion->v18_cargo2 = $request->v18_cargo2;
        } else
            $validacion->v18_cargo2 = 0;


        if ($request->v19_cargo2== 1){
            $validacion->v19_cargo2 = $request->v19_cargo2;
        } else
            $validacion->v19_cargo2 = 0;

        if ($request->v20_cargo2 == 1){
            $validacion->v20_cargo2 = $request->v20_cargo2;
        } else
            $validacion->v20_cargo2= 0;

        if ($request->v21_cargo2== 1){
            $validacion->v21_cargo2= $request->v21_cargo2;
        } else
            $validacion->v21_cargo2= 0;




        if ($request->v1_niv_educ== 1){
            $validacion->v1_niv_educ= $request->v1_niv_educ;
        } else
            $validacion->v1_niv_educ = 0;

        if ($request->v2_niv_educ== 1){
            $validacion->v2_niv_educ= $request->v2_niv_educ;
        } else
            $validacion->v2_niv_educ= 0;


        if ($request->v3_niv_educ== 1){
            $validacion->v3_niv_educ = $request->v3_niv_educ;
        } else
            $validacion->v3_niv_educ = 0;


        if ($request->v4_niv_educ== 1){
            $validacion->v4_niv_educ= $request->v4_niv_educ;
        } else
            $validacion->v4_niv_educ= 0;

        if ($request->v6_niv_educ== 1){
            $validacion->v6_niv_educ= $request->v6_niv_educ;
        } else
            $validacion->v6_niv_educ = 0;

        if ($request->v7_niv_educ== 1){
            $validacion->v7_niv_educ = $request->v7_niv_educ;
        } else
            $validacion->v7_niv_educ = 0;


        if ($request->v8_niv_educ== 1){
            $validacion->v8_niv_educ = $request->v8_niv_educ;
        } else
            $validacion->v8_niv_educ = 0;


        if ($request->v9_niv_educ== 1){
            $validacion->v9_niv_educ = $request->v9_niv_educ;
        } else
            $validacion->v9_niv_educ = 0;


        if ($request->v10_niv_educ== 1){
            $validacion->v10_niv_educ = $request->v10_niv_educ;
        } else
            $validacion->v10_niv_educ = 0;


        if ($request->v11_niv_educ== 1){
            $validacion->v11_niv_educ = $request->v11_niv_educ;
        } else
            $validacion->v11_niv_educ = 0;


        if ($request->v12_niv_educ== 1){
            $validacion->v12_niv_educ = $request->v12_niv_educ;
        } else
            $validacion->v12_niv_educ = 0;


        if ($request->v13_niv_educ== 1){
            $validacion->v13_niv_educ = $request->v13_niv_educ;
        } else
            $validacion->v13_niv_educ = 0;


        if ($request->v16_niv_educ== 1){
            $validacion->v16_niv_educ = $request->v16_niv_educ;
        } else
            $validacion->v16_niv_educ = 0;



        if ($request->v17_niv_educ== 1){
            $validacion->v17_niv_educ = $request->v17_niv_educ;
        } else
            $validacion->v17_niv_educ = 0;


        if ($request->v18_niv_educ== 1){
            $validacion->v18_niv_educ = $request->v18_niv_educ;
        } else
            $validacion->v18_niv_educ = 0;


        if ($request->v19_niv_educ== 1){
            $validacion->v19_niv_educ = $request->v19_niv_educ;
        } else
            $validacion->v19_niv_educ = 0;

        if ($request->v20_niv_educ == 1){
            $validacion->v20_niv_educ = $request->v20_niv_educ;
        } else
            $validacion->v20_niv_educ= 0;

        if ($request->v21_niv_educ== 1){
            $validacion->v21_niv_educ= $request->v21_niv_educ;
        } else
            $validacion->v21_niv_educ= 0;

        if ($request->v1_region== 1){
            $validacion->v1_region= $request->v1_region;
        } else
            $validacion->v1_region = 0;

        if ($request->v2_region== 1){
            $validacion->v2_region= $request->v2_region;
        } else
            $validacion->v2_region= 0;


        if ($request->v3_region== 1){
            $validacion->v3_region = $request->v3_region;
        } else
            $validacion->v3_region = 0;


        if ($request->v4_region== 1){
            $validacion->v4_region= $request->v4_region;
        } else
            $validacion->v4_region= 0;

        if ($request->v6_region== 1){
            $validacion->v6_region= $request->v6_region;
        } else
            $validacion->v6_region = 0;

        if ($request->v7_region== 1){
            $validacion->v7_region = $request->v7_region;
        } else
            $validacion->v7_region = 0;


        if ($request->v8_region== 1){
            $validacion->v8_region = $request->v8_region;
        } else
            $validacion->v8_region = 0;


        if ($request->v9_region== 1){
            $validacion->v9_region = $request->v9_region;
        } else
            $validacion->v9_region = 0;


        if ($request->v10_region== 1){
            $validacion->v10_region = $request->v10_region;
        } else
            $validacion->v10_region = 0;


        if ($request->v11_region== 1){
            $validacion->v11_region = $request->v11_region;
        } else
            $validacion->v11_region = 0;


        if ($request->v12_region== 1){
            $validacion->v12_region = $request->v12_region;
        } else
            $validacion->v12_region = 0;


        if ($request->v13_region== 1){
            $validacion->v13_region = $request->v13_region;
        } else
            $validacion->v13_region = 0;


        if ($request->v16_region== 1){
            $validacion->v16_region = $request->v16_region;
        } else
            $validacion->v16_region = 0;


        if ($request->v17_region== 1){
            $validacion->v17_region = $request->v17_region;
        } else
            $validacion->v17_region = 0;


        if ($request->v18_region== 1){
            $validacion->v18_region = $request->v18_region;
        } else
            $validacion->v18_region = 0;


        if ($request->v19_region== 1){
            $validacion->v19_region = $request->v19_region;
        } else
            $validacion->v19_region = 0;

        if ($request->v20_region == 1){
            $validacion->v20_region = $request->v20_region;
        } else
            $validacion->v20_region= 0;

        if ($request->v21_region== 1){
            $validacion->v21_region= $request->v21_region;
        } else
            $validacion->v21_region= 0;

        /****************** CIUDAD **************************/

        if ($request->v1_ciudad== 1){
            $validacion->v1_ciudad= $request->v1_ciudad;
        } else
            $validacion->v1_ciudad = 0;

        if ($request->v2_ciudad== 1){
            $validacion->v2_ciudad= $request->v2_ciudad;
        } else
            $validacion->v2_ciudad= 0;


        if ($request->v3_ciudad== 1){
            $validacion->v3_ciudad = $request->v3_ciudad;
        } else
            $validacion->v3_ciudad = 0;


        if ($request->v4_ciudad== 1){
            $validacion->v4_ciudad= $request->v4_ciudad;
        } else
            $validacion->v4_ciudad= 0;

        if ($request->v6_ciudad== 1){
            $validacion->v6_ciudad= $request->v6_ciudad;
        } else
            $validacion->v6_ciudad = 0;

        if ($request->v7_ciudad== 1){
            $validacion->v7_ciudad = $request->v7_ciudad;
        } else
            $validacion->v7_ciudad = 0;


        if ($request->v8_ciudad== 1){
            $validacion->v8_ciudad = $request->v8_ciudad;
        } else
            $validacion->v8_ciudad = 0;


        if ($request->v9_ciudad== 1){
            $validacion->v9_ciudad = $request->v9_ciudad;
        } else
            $validacion->v9_ciudad = 0;


        if ($request->v10_ciudad== 1){
            $validacion->v10_ciudad = $request->v10_ciudad;
        } else
            $validacion->v10_ciudad = 0;


        if ($request->v11_ciudad== 1){
            $validacion->v11_ciudad = $request->v11_ciudad;
        } else
            $validacion->v11_ciudad = 0;


        if ($request->v12_ciudad== 1){
            $validacion->v12_ciudad = $request->v12_ciudad;
        } else
            $validacion->v12_ciudad = 0;


        if ($request->v13_ciudad== 1){
            $validacion->v13_ciudad = $request->v13_ciudad;
        } else
            $validacion->v13_ciudad = 0;


        if ($request->v16_ciudad== 1){
            $validacion->v16_ciudad = $request->v16_ciudad;
        } else
            $validacion->v16_ciudad = 0;


        if ($request->v17_ciudad== 1){
            $validacion->v17_ciudad = $request->v17_ciudad;
        } else
            $validacion->v17_ciudad = 0;


        if ($request->v18_ciudad== 1){
            $validacion->v18_ciudad = $request->v18_ciudad;
        } else
            $validacion->v18_ciudad = 0;


        if ($request->v19_ciudad== 1){
            $validacion->v19_ciudad = $request->v19_ciudad;
        } else
            $validacion->v19_ciudad = 0;

        if ($request->v20_ciudad == 1){
            $validacion->v20_ciudad = $request->v20_ciudad;
        } else
            $validacion->v20_ciudad= 0;

        if ($request->v21_ciudad== 1){
            $validacion->v21_ciudad= $request->v21_ciudad;
        } else
            $validacion->v21_ciudad= 0;

        if ($request->v1_es_jefe== 1){
            $validacion->v1_es_jefe= $request->v1_es_jefe;
        } else
            $validacion->v1_es_jefe = 0;

        if ($request->v2_es_jefe== 1){
            $validacion->v2_es_jefe= $request->v2_es_jefe;
        } else
            $validacion->v2_es_jefe= 0;


        if ($request->v3_es_jefe== 1){
            $validacion->v3_es_jefe = $request->v3_es_jefe;
        } else
            $validacion->v3_es_jefe = 0;


        if ($request->v4_es_jefe== 1){
            $validacion->v4_es_jefe= $request->v4_es_jefe;
        } else
            $validacion->v4_es_jefe= 0;

        if ($request->v6_es_jefe== 1){
            $validacion->v6_es_jefe= $request->v6_es_jefe;
        } else
            $validacion->v6_es_jefe = 0;

        if ($request->v7_es_jefe== 1){
            $validacion->v7_es_jefe = $request->v7_es_jefe;
        } else
            $validacion->v7_es_jefe = 0;


        if ($request->v8_es_jefe== 1){
            $validacion->v8_es_jefe = $request->v8_es_jefe;
        } else
            $validacion->v8_es_jefe = 0;


        if ($request->v9_es_jefe== 1){
            $validacion->v9_es_jefe = $request->v9_es_jefe;
        } else
            $validacion->v9_es_jefe = 0;


        if ($request->v10_es_jefe== 1){
            $validacion->v10_es_jefe = $request->v10_es_jefe;
        } else
            $validacion->v10_es_jefe = 0;


        if ($request->v11_es_jefe== 1){
            $validacion->v11_es_jefe = $request->v11_es_jefe;
        } else
            $validacion->v11_es_jefe = 0;


        if ($request->v12_es_jefe== 1){
            $validacion->v12_es_jefe = $request->v12_es_jefe;
        } else
            $validacion->v12_es_jefe = 0;


        if ($request->v13_es_jefe== 1){
            $validacion->v13_es_jefe = $request->v13_es_jefe;
        } else
            $validacion->v13_es_jefe = 0;


        if ($request->v16_es_jefe== 1){
            $validacion->v16_es_jefe = $request->v16_es_jefe;
        } else
            $validacion->v16_es_jefe = 0;


        if ($request->v17_es_jefe== 1){
            $validacion->v17_es_jefe = $request->v17_es_jefe;
        } else
            $validacion->v17_es_jefe = 0;


        if ($request->v18_es_jefe== 1){
            $validacion->v18_es_jefe = $request->v18_es_jefe;
        } else
            $validacion->v18_es_jefe = 0;


        if ($request->v19_es_jefe== 1){
            $validacion->v19_es_jefe = $request->v19_es_jefe;
        } else
            $validacion->v19_es_jefe = 0;

        if ($request->v20_es_jefe == 1){
            $validacion->v20_es_jefe = $request->v20_es_jefe;
        } else
            $validacion->v20_es_jefe= 0;

        if ($request->v21_es_jefe== 1){
            $validacion->v21_es_jefe= $request->v21_es_jefe;
        } else
            $validacion->v21_es_jefe= 0;

        if ($request->v1_id_jefe== 1){
            $validacion->v1_id_jefe= $request->v1_id_jefe;
        } else
            $validacion->v1_id_jefe = 0;

        if ($request->v2_id_jefe== 1){
            $validacion->v2_id_jefe= $request->v2_id_jefe;
        } else
            $validacion->v2_id_jefe= 0;


        if ($request->v3_id_jefe== 1){
            $validacion->v3_id_jefe = $request->v3_id_jefe;
        } else
            $validacion->v3_id_jefe = 0;


        if ($request->v4_id_jefe== 1){
            $validacion->v4_id_jefe= $request->v4_id_jefe;
        } else
            $validacion->v4_id_jefe= 0;

        if ($request->v6_id_jefe== 1){
            $validacion->v6_id_jefe= $request->v6_id_jefe;
        } else
            $validacion->v6_id_jefe = 0;

        if ($request->v7_id_jefe== 1){
            $validacion->v7_id_jefe = $request->v7_id_jefe;
        } else
            $validacion->v7_id_jefe = 0;


        if ($request->v8_id_jefe== 1){
            $validacion->v8_id_jefe = $request->v8_id_jefe;
        } else
            $validacion->v8_id_jefe = 0;


        if ($request->v9_id_jefe== 1){
            $validacion->v9_id_jefe = $request->v9_id_jefe;
        } else
            $validacion->v9_id_jefe = 0;


        if ($request->v10_id_jefe== 1){
            $validacion->v10_id_jefe = $request->v10_id_jefe;
        } else
            $validacion->v10_id_jefe = 0;


        if ($request->v11_id_jefe== 1){
            $validacion->v11_id_jefe = $request->v11_id_jefe;
        } else
            $validacion->v11_id_jefe = 0;


        if ($request->v12_id_jefe== 1){
            $validacion->v12_id_jefe = $request->v12_id_jefe;
        } else
            $validacion->v12_id_jefe = 0;


        if ($request->v13_id_jefe== 1){
            $validacion->v13_id_jefe = $request->v13_id_jefe;
        } else
            $validacion->v13_id_jefe = 0;


        if ($request->v16_id_jefe== 1){
            $validacion->v16_id_jefe = $request->v16_id_jefe;
        } else
            $validacion->v16_id_jefe = 0;


        if ($request->v17_id_jefe== 1){
            $validacion->v17_id_jefe = $request->v17_id_jefe;
        } else
            $validacion->v17_id_jefe = 0;


        if ($request->v18_id_jefe== 1){
            $validacion->v18_id_jefe = $request->v18_id_jefe;
        } else
            $validacion->v18_id_jefe = 0;


        if ($request->v19_id_jefe== 1){
            $validacion->v19_id_jefe = $request->v19_id_jefe;
        } else
            $validacion->v19_id_jefe = 0;

        if ($request->v20_id_jefe == 1){
            $validacion->v20_id_jefe = $request->v20_id_jefe;
        } else
            $validacion->v20_id_jefe= 0;

        if ($request->v21_id_jefe== 1){
            $validacion->v21_id_jefe= $request->v21_id_jefe;
        } else
            $validacion->v21_id_jefe= 0;

        if ($request->v1_cat_jefe== 1){
            $validacion->v1_cat_jefe= $request->v1_cat_jefe;
        } else
            $validacion->v1_cat_jefe = 0;

        if ($request->v2_cat_jefe== 1){
            $validacion->v2_cat_jefe= $request->v2_cat_jefe;
        } else
            $validacion->v2_cat_jefe= 0;


        if ($request->v3_cat_jefe== 1){
            $validacion->v3_cat_jefe = $request->v3_cat_jefe;
        } else
            $validacion->v3_cat_jefe = 0;


        if ($request->v4_cat_jefe== 1){
            $validacion->v4_cat_jefe= $request->v4_cat_jefe;
        } else
            $validacion->v4_cat_jefe= 0;

        if ($request->v6_cat_jefe== 1){
            $validacion->v6_cat_jefe= $request->v6_cat_jefe;
        } else
            $validacion->v6_cat_jefe = 0;

        if ($request->v7_cat_jefe== 1){
            $validacion->v7_cat_jefe = $request->v7_cat_jefe;
        } else
            $validacion->v7_cat_jefe = 0;


        if ($request->v8_cat_jefe== 1){
            $validacion->v8_cat_jefe = $request->v8_cat_jefe;
        } else
            $validacion->v8_cat_jefe = 0;


        if ($request->v9_cat_jefe== 1){
            $validacion->v9_cat_jefe = $request->v9_cat_jefe;
        } else
            $validacion->v9_cat_jefe = 0;


        if ($request->v10_cat_jefe== 1){
            $validacion->v10_cat_jefe = $request->v10_cat_jefe;
        } else
            $validacion->v10_cat_jefe = 0;


        if ($request->v11_cat_jefe== 1){
            $validacion->v11_cat_jefe = $request->v11_cat_jefe;
        } else
            $validacion->v11_cat_jefe = 0;


        if ($request->v12_cat_jefe== 1){
            $validacion->v12_cat_jefe = $request->v12_cat_jefe;
        } else
            $validacion->v12_cat_jefe = 0;


        if ($request->v13_cat_jefe== 1){
            $validacion->v13_cat_jefe = $request->v13_cat_jefe;
        } else
            $validacion->v13_cat_jefe = 0;


        if ($request->v16_cat_jefe== 1){
            $validacion->v16_cat_jefe = $request->v16_cat_jefe;
        } else
            $validacion->v16_cat_jefe = 0;


        if ($request->v17_cat_jefe== 1){
            $validacion->v17_cat_jefe = $request->v17_cat_jefe;
        } else
            $validacion->v17_cat_jefe = 0;


        if ($request->v18_cat_jefe== 1){
            $validacion->v18_cat_jefe = $request->v18_cat_jefe;
        } else
            $validacion->v18_cat_jefe = 0;


        if ($request->v19_cat_jefe== 1){
            $validacion->v19_cat_jefe = $request->v19_cat_jefe;
        } else
            $validacion->v19_cat_jefe = 0;

        if ($request->v20_cat_jefe == 1){
            $validacion->v20_cat_jefe = $request->v20_cat_jefe;
        } else
            $validacion->v20_cat_jefe= 0;

        if ($request->v21_cat_jefe== 1){
            $validacion->v21_cat_jefe= $request->v21_cat_jefe;
        } else
            $validacion->v21_cat_jefe= 0;

        if ($request->v1_n1== 1){
            $validacion->v1_n1= $request->v1_n1;
        } else
            $validacion->v1_n1 = 0;

        if ($request->v2_n1== 1){
            $validacion->v2_n1= $request->v2_n1;
        } else
            $validacion->v2_n1= 0;


        if ($request->v3_n1== 1){
            $validacion->v3_n1 = $request->v3_n1;
        } else
            $validacion->v3_n1 = 0;


        if ($request->v4_n1== 1){
            $validacion->v4_n1= $request->v4_n1;
        } else
            $validacion->v4_n1= 0;

        if ($request->v6_n1== 1){
            $validacion->v6_n1= $request->v6_n1;
        } else
            $validacion->v6_n1 = 0;

        if ($request->v7_n1== 1){
            $validacion->v7_n1 = $request->v7_n1;
        } else
            $validacion->v7_n1 = 0;


        if ($request->v8_n1== 1){
            $validacion->v8_n1 = $request->v8_n1;
        } else
            $validacion->v8_n1 = 0;


        if ($request->v9_n1== 1){
            $validacion->v9_n1 = $request->v9_n1;
        } else
            $validacion->v9_n1 = 0;


        if ($request->v10_n1== 1){
            $validacion->v10_n1 = $request->v10_n1;
        } else
            $validacion->v10_n1 = 0;


        if ($request->v11_n1== 1){
            $validacion->v11_n1 = $request->v11_n1;
        } else
            $validacion->v11_n1 = 0;


        if ($request->v12_n1== 1){
            $validacion->v12_n1 = $request->v12_n1;
        } else
            $validacion->v12_n1 = 0;


        if ($request->v13_n1== 1){
            $validacion->v13_n1 = $request->v13_n1;
        } else
            $validacion->v13_n1 = 0;


        if ($request->v16_n1== 1){
            $validacion->v16_n1 = $request->v16_n1;
        } else
            $validacion->v16_n1 = 0;


        if ($request->v17_n1== 1){
            $validacion->v17_n1 = $request->v17_n1;
        } else
            $validacion->v17_n1 = 0;


        if ($request->v18_n1== 1){
            $validacion->v18_n1 = $request->v18_n1;
        } else
            $validacion->v18_n1 = 0;


        if ($request->v19_n1== 1){
            $validacion->v19_n1 = $request->v19_n1;
        } else
            $validacion->v19_n1 = 0;

        if ($request->v20_n1 == 1){
            $validacion->v20_n1 = $request->v20_n1;
        } else
            $validacion->v20_n1= 0;

        if ($request->v21_n1== 1){
            $validacion->v21_n1= $request->v21_n1;
        } else
            $validacion->v21_n1= 0;

        if ($request->v1_n2== 1){
            $validacion->v1_n2= $request->v1_n2;
        } else
            $validacion->v1_n2 = 0;

        if ($request->v2_n2== 1){
            $validacion->v2_n2= $request->v2_n2;
        } else
            $validacion->v2_n2= 0;


        if ($request->v3_n2== 1){
            $validacion->v3_n2 = $request->v3_n2;
        } else
            $validacion->v3_n2 = 0;


        if ($request->v4_n2== 1){
            $validacion->v4_n2= $request->v4_n2;
        } else
            $validacion->v4_n2= 0;

        if ($request->v6_n2== 1){
            $validacion->v6_n2= $request->v6_n2;
        } else
            $validacion->v6_n2 = 0;

        if ($request->v7_n2== 1){
            $validacion->v7_n2 = $request->v7_n2;
        } else
            $validacion->v7_n2 = 0;


        if ($request->v8_n2== 1){
            $validacion->v8_n2 = $request->v8_n2;
        } else
            $validacion->v8_n2 = 0;


        if ($request->v9_n2== 1){
            $validacion->v9_n2 = $request->v9_n2;
        } else
            $validacion->v9_n2 = 0;


        if ($request->v10_n2== 1){
            $validacion->v10_n2 = $request->v10_n2;
        } else
            $validacion->v10_n2 = 0;


        if ($request->v11_n2== 1){
            $validacion->v11_n2 = $request->v11_n2;
        } else
            $validacion->v11_n2 = 0;


        if ($request->v12_n2== 1){
            $validacion->v12_n2 = $request->v12_n2;
        } else
            $validacion->v12_n2 = 0;


        if ($request->v13_n2== 1){
            $validacion->v13_n2 = $request->v13_n2;
        } else
            $validacion->v13_n2 = 0;


        if ($request->v16_n2== 1){
            $validacion->v16_n2 = $request->v16_n2;
        } else
            $validacion->v16_n2 = 0;


        if ($request->v17_n2== 1){
            $validacion->v17_n2 = $request->v17_n2;
        } else
            $validacion->v17_n2 = 0;


        if ($request->v18_n2== 1){
            $validacion->v18_n2 = $request->v18_n2;
        } else
            $validacion->v18_n2 = 0;


        if ($request->v19_n2== 1){
            $validacion->v19_n2 = $request->v19_n2;
        } else
            $validacion->v19_n2 = 0;

        if ($request->v20_n2 == 1){
            $validacion->v20_n2 = $request->v20_n2;
        } else
            $validacion->v20_n2= 0;

        if ($request->v21_n2== 1){
            $validacion->v21_n2= $request->v21_n2;
        } else
            $validacion->v21_n2= 0;

        if ($request->v1_n3== 1){
            $validacion->v1_n3= $request->v1_n3;
        } else
            $validacion->v1_n3 = 0;

        if ($request->v2_n3== 1){
            $validacion->v2_n3= $request->v2_n3;
        } else
            $validacion->v2_n3= 0;


        if ($request->v3_n3== 1){
            $validacion->v3_n3 = $request->v3_n3;
        } else
            $validacion->v3_n3 = 0;


        if ($request->v4_n3== 1){
            $validacion->v4_n3= $request->v4_n3;
        } else
            $validacion->v4_n3= 0;

        if ($request->v6_n3== 1){
            $validacion->v6_n3= $request->v6_n3;
        } else
            $validacion->v6_n3 = 0;

        if ($request->v7_n3== 1){
            $validacion->v7_n3 = $request->v7_n3;
        } else
            $validacion->v7_n3 = 0;


        if ($request->v8_n3== 1){
            $validacion->v8_n3 = $request->v8_n3;
        } else
            $validacion->v8_n3 = 0;


        if ($request->v9_n3== 1){
            $validacion->v9_n3 = $request->v9_n3;
        } else
            $validacion->v9_n3 = 0;


        if ($request->v10_n3== 1){
            $validacion->v10_n3 = $request->v10_n3;
        } else
            $validacion->v10_n3 = 0;


        if ($request->v11_n3== 1){
            $validacion->v11_n3 = $request->v11_n3;
        } else
            $validacion->v11_n3 = 0;


        if ($request->v12_n3== 1){
            $validacion->v12_n3 = $request->v12_n3;
        } else
            $validacion->v12_n3 = 0;


        if ($request->v13_n3== 1){
            $validacion->v13_n3 = $request->v13_n3;
        } else
            $validacion->v13_n3 = 0;


        if ($request->v16_n3== 1){
            $validacion->v16_n3 = $request->v16_n3;
        } else
            $validacion->v16_n3 = 0;


        if ($request->v17_n3== 1){
            $validacion->v17_n3 = $request->v17_n3;
        } else
            $validacion->v17_n3 = 0;


        if ($request->v18_n3== 1){
            $validacion->v18_n3 = $request->v18_n3;
        } else
            $validacion->v18_n3 = 0;


        if ($request->v19_n3== 1){
            $validacion->v19_n3 = $request->v19_n3;
        } else
            $validacion->v19_n3 = 0;

        if ($request->v20_n3 == 1){
            $validacion->v20_n3 = $request->v20_n3;
        } else
            $validacion->v20_n3= 0;

        if ($request->v21_n3== 1){
            $validacion->v21_n3= $request->v21_n3;
        } else
            $validacion->v21_n3= 0;

        if ($request->v1_n4== 1){
            $validacion->v1_n4= $request->v1_n4;
        } else
            $validacion->v1_n4 = 0;

        if ($request->v2_n4== 1){
            $validacion->v2_n4= $request->v2_n4;
        } else
            $validacion->v2_n4= 0;


        if ($request->v3_n4== 1){
            $validacion->v3_n4 = $request->v3_n4;
        } else
            $validacion->v3_n4 = 0;


        if ($request->v4_n4== 1){
            $validacion->v4_n4= $request->v4_n4;
        } else
            $validacion->v4_n4= 0;

        if ($request->v6_n4== 1){
            $validacion->v6_n4= $request->v6_n4;
        } else
            $validacion->v6_n4 = 0;

        if ($request->v7_n4== 1){
            $validacion->v7_n4 = $request->v7_n4;
        } else
            $validacion->v7_n4 = 0;


        if ($request->v8_n4== 1){
            $validacion->v8_n4 = $request->v8_n4;
        } else
            $validacion->v8_n4 = 0;


        if ($request->v9_n4== 1){
            $validacion->v9_n4 = $request->v9_n4;
        } else
            $validacion->v9_n4 = 0;


        if ($request->v10_n4== 1){
            $validacion->v10_n4 = $request->v10_n4;
        } else
            $validacion->v10_n4 = 0;


        if ($request->v11_n4== 1){
            $validacion->v11_n4 = $request->v11_n4;
        } else
            $validacion->v11_n4 = 0;


        if ($request->v12_n4== 1){
            $validacion->v12_n4 = $request->v12_n4;
        } else
            $validacion->v12_n4 = 0;


        if ($request->v13_n4== 1){
            $validacion->v13_n4 = $request->v13_n4;
        } else
            $validacion->v13_n4 = 0;


        if ($request->v16_n4== 1){
            $validacion->v16_n4 = $request->v16_n4;
        } else
            $validacion->v16_n4 = 0;


        if ($request->v17_n4== 1){
            $validacion->v17_n4 = $request->v17_n4;
        } else
            $validacion->v17_n4 = 0;


        if ($request->v18_n4== 1){
            $validacion->v18_n4 = $request->v18_n4;
        } else
            $validacion->v18_n4 = 0;


        if ($request->v19_n4== 1){
            $validacion->v19_n4 = $request->v19_n4;
        } else
            $validacion->v19_n4 = 0;

        if ($request->v20_n4 == 1){
            $validacion->v20_n4 = $request->v20_n4;
        } else
            $validacion->v20_n4= 0;

        if ($request->v21_n4== 1){
            $validacion->v21_n4= $request->v21_n4;
        } else
            $validacion->v21_n4= 0;

        if ($request->v1_n5== 1){
            $validacion->v1_n5= $request->v1_n5;
        } else
            $validacion->v1_n5 = 0;

        if ($request->v2_n5== 1){
            $validacion->v2_n5= $request->v2_n5;
        } else
            $validacion->v2_n5= 0;


        if ($request->v3_n5== 1){
            $validacion->v3_n5 = $request->v3_n5;
        } else
            $validacion->v3_n5 = 0;


        if ($request->v4_n5== 1){
            $validacion->v4_n5= $request->v4_n5;
        } else
            $validacion->v4_n5= 0;

        if ($request->v6_n5== 1){
            $validacion->v6_n5= $request->v6_n5;
        } else
            $validacion->v6_n5 = 0;

        if ($request->v7_n5== 1){
            $validacion->v7_n5 = $request->v7_n5;
        } else
            $validacion->v7_n5 = 0;


        if ($request->v8_n5== 1){
            $validacion->v8_n5 = $request->v8_n5;
        } else
            $validacion->v8_n5 = 0;


        if ($request->v9_n5== 1){
            $validacion->v9_n5 = $request->v9_n5;
        } else
            $validacion->v9_n5 = 0;


        if ($request->v10_n5== 1){
            $validacion->v10_n5 = $request->v10_n5;
        } else
            $validacion->v10_n5 = 0;


        if ($request->v11_n5== 1){
            $validacion->v11_n5 = $request->v11_n5;
        } else
            $validacion->v11_n5 = 0;


        if ($request->v12_n5== 1){
            $validacion->v12_n5 = $request->v12_n5;
        } else
            $validacion->v12_n5 = 0;


        if ($request->v13_n5== 1){
            $validacion->v13_n5 = $request->v13_n5;
        } else
            $validacion->v13_n5 = 0;


        if ($request->v16_n5== 1){
            $validacion->v16_n5 = $request->v16_n5;
        } else
            $validacion->v16_n5 = 0;


        if ($request->v17_n5== 1){
            $validacion->v17_n5 = $request->v17_n5;
        } else
            $validacion->v17_n5 = 0;


        if ($request->v18_n5== 1){
            $validacion->v18_n5 = $request->v18_n5;
        } else
            $validacion->v18_n5 = 0;


        if ($request->v19_n5== 1){
            $validacion->v19_n5 = $request->v19_n5;
        } else
            $validacion->v19_n5 = 0;

        if ($request->v20_n5 == 1){
            $validacion->v20_n5 = $request->v20_n5;
        } else
            $validacion->v20_n5= 0;

        if ($request->v21_n5== 1){
            $validacion->v21_n5= $request->v21_n5;
        } else
            $validacion->v21_n5= 0;

        if ($request->v1_seg_add_1== 1){
            $validacion->v1_seg_add_1= $request->v1_seg_add_1;
        } else
            $validacion->v1_seg_add_1 = 0;

        if ($request->v2_seg_add_1== 1){
            $validacion->v2_seg_add_1= $request->v2_seg_add_1;
        } else
            $validacion->v2_seg_add_1= 0;


        if ($request->v3_seg_add_1== 1){
            $validacion->v3_seg_add_1 = $request->v3_seg_add_1;
        } else
            $validacion->v3_seg_add_1 = 0;


        if ($request->v4_seg_add_1== 1){
            $validacion->v4_seg_add_1= $request->v4_seg_add_1;
        } else
            $validacion->v4_seg_add_1= 0;

        if ($request->v6_seg_add_1== 1){
            $validacion->v6_seg_add_1= $request->v6_seg_add_1;
        } else
            $validacion->v6_seg_add_1 = 0;

        if ($request->v7_seg_add_1== 1){
            $validacion->v7_seg_add_1 = $request->v7_seg_add_1;
        } else
            $validacion->v7_seg_add_1 = 0;


        if ($request->v8_seg_add_1== 1){
            $validacion->v8_seg_add_1 = $request->v8_seg_add_1;
        } else
            $validacion->v8_seg_add_1 = 0;


        if ($request->v9_seg_add_1== 1){
            $validacion->v9_seg_add_1 = $request->v9_seg_add_1;
        } else
            $validacion->v9_seg_add_1 = 0;


        if ($request->v10_seg_add_1== 1){
            $validacion->v10_seg_add_1 = $request->v10_seg_add_1;
        } else
            $validacion->v10_seg_add_1 = 0;


        if ($request->v11_seg_add_1== 1){
            $validacion->v11_seg_add_1 = $request->v11_seg_add_1;
        } else
            $validacion->v11_seg_add_1 = 0;


        if ($request->v12_seg_add_1== 1){
            $validacion->v12_seg_add_1 = $request->v12_seg_add_1;
        } else
            $validacion->v12_seg_add_1 = 0;


        if ($request->v13_seg_add_1== 1){
            $validacion->v13_seg_add_1 = $request->v13_seg_add_1;
        } else
            $validacion->v13_seg_add_1 = 0;


        if ($request->v16_seg_add_1== 1){
            $validacion->v16_seg_add_1 = $request->v16_seg_add_1;
        } else
            $validacion->v16_seg_add_1 = 0;


        if ($request->v17_seg_add_1== 1){
            $validacion->v17_seg_add_1 = $request->v17_seg_add_1;
        } else
            $validacion->v17_seg_add_1 = 0;


        if ($request->v18_seg_add_1== 1){
            $validacion->v18_seg_add_1 = $request->v18_seg_add_1;
        } else
            $validacion->v18_seg_add_1 = 0;


        if ($request->v19_seg_add_1== 1){
            $validacion->v19_seg_add_1 = $request->v19_seg_add_1;
        } else
            $validacion->v19_seg_add_1 = 0;

        if ($request->v20_seg_add_1 == 1){
            $validacion->v20_seg_add_1 = $request->v20_seg_add_1;
        } else
            $validacion->v20_seg_add_1= 0;

        if ($request->v21_seg_add_1== 1){
            $validacion->v21_seg_add_1= $request->v21_seg_add_1;
        } else
            $validacion->v21_seg_add_1= 0;

        if ($request->v1_seg_add_2== 1){
            $validacion->v1_seg_add_2= $request->v1_seg_add_2;
        } else
            $validacion->v1_seg_add_2 = 0;

        if ($request->v2_seg_add_2== 1){
            $validacion->v2_seg_add_2= $request->v2_seg_add_2;
        } else
            $validacion->v2_seg_add_2= 0;


        if ($request->v3_seg_add_2== 1){
            $validacion->v3_seg_add_2 = $request->v3_seg_add_2;
        } else
            $validacion->v3_seg_add_2 = 0;


        if ($request->v4_seg_add_2== 1){
            $validacion->v4_seg_add_2= $request->v4_seg_add_2;
        } else
            $validacion->v4_seg_add_2= 0;

        if ($request->v6_seg_add_2== 1){
            $validacion->v6_seg_add_2= $request->v6_seg_add_2;
        } else
            $validacion->v6_seg_add_2 = 0;

        if ($request->v7_seg_add_2== 1){
            $validacion->v7_seg_add_2 = $request->v7_seg_add_2;
        } else
            $validacion->v7_seg_add_2 = 0;


        if ($request->v8_seg_add_2== 1){
            $validacion->v8_seg_add_2 = $request->v8_seg_add_2;
        } else
            $validacion->v8_seg_add_2 = 0;


        if ($request->v9_seg_add_2== 1){
            $validacion->v9_seg_add_2 = $request->v9_seg_add_2;
        } else
            $validacion->v9_seg_add_2 = 0;


        if ($request->v10_seg_add_2== 1){
            $validacion->v10_seg_add_2 = $request->v10_seg_add_2;
        } else
            $validacion->v10_seg_add_2 = 0;


        if ($request->v11_seg_add_2== 1){
            $validacion->v11_seg_add_2 = $request->v11_seg_add_2;
        } else
            $validacion->v11_seg_add_2 = 0;


        if ($request->v12_seg_add_2== 1){
            $validacion->v12_seg_add_2 = $request->v12_seg_add_2;
        } else
            $validacion->v12_seg_add_2 = 0;


        if ($request->v13_seg_add_2== 1){
            $validacion->v13_seg_add_2 = $request->v13_seg_add_2;
        } else
            $validacion->v13_seg_add_2 = 0;


        if ($request->v16_seg_add_2== 1){
            $validacion->v16_seg_add_2 = $request->v16_seg_add_2;
        } else
            $validacion->v16_seg_add_2 = 0;


        if ($request->v17_seg_add_2== 1){
            $validacion->v17_seg_add_2 = $request->v17_seg_add_2;
        } else
            $validacion->v17_seg_add_2 = 0;


        if ($request->v18_seg_add_2== 1){
            $validacion->v18_seg_add_2 = $request->v18_seg_add_2;
        } else
            $validacion->v18_seg_add_2 = 0;


        if ($request->v19_seg_add_2== 1){
            $validacion->v19_seg_add_2 = $request->v19_seg_add_2;
        } else
            $validacion->v19_seg_add_2 = 0;

        if ($request->v20_seg_add_2 == 1){
            $validacion->v20_seg_add_2 = $request->v20_seg_add_2;
        } else
            $validacion->v20_seg_add_2= 0;

        if ($request->v21_seg_add_2== 1){
            $validacion->v21_seg_add_2= $request->v21_seg_add_2;
        } else
            $validacion->v21_seg_add_2= 0;

        if ($request->v1_seg_add_3== 1){
            $validacion->v1_seg_add_3= $request->v1_seg_add_3;
        } else
            $validacion->v1_seg_add_3 = 0;

        if ($request->v2_seg_add_3== 1){
            $validacion->v2_seg_add_3= $request->v2_seg_add_3;
        } else
            $validacion->v2_seg_add_3= 0;


        if ($request->v3_seg_add_3== 1){
            $validacion->v3_seg_add_3 = $request->v3_seg_add_3;
        } else
            $validacion->v3_seg_add_3 = 0;


        if ($request->v4_seg_add_3== 1){
            $validacion->v4_seg_add_3= $request->v4_seg_add_3;
        } else
            $validacion->v4_seg_add_3= 0;

        if ($request->v6_seg_add_3== 1){
            $validacion->v6_seg_add_3= $request->v6_seg_add_3;
        } else
            $validacion->v6_seg_add_3 = 0;

        if ($request->v7_seg_add_3== 1){
            $validacion->v7_seg_add_3 = $request->v7_seg_add_3;
        } else
            $validacion->v7_seg_add_3 = 0;


        if ($request->v8_seg_add_3== 1){
            $validacion->v8_seg_add_3 = $request->v8_seg_add_3;
        } else
            $validacion->v8_seg_add_3 = 0;


        if ($request->v9_seg_add_3== 1){
            $validacion->v9_seg_add_3 = $request->v9_seg_add_3;
        } else
            $validacion->v9_seg_add_3 = 0;


        if ($request->v10_seg_add_3== 1){
            $validacion->v10_seg_add_3 = $request->v10_seg_add_3;
        } else
            $validacion->v10_seg_add_3 = 0;


        if ($request->v11_seg_add_3== 1){
            $validacion->v11_seg_add_3 = $request->v11_seg_add_3;
        } else
            $validacion->v11_seg_add_3 = 0;


        if ($request->v12_seg_add_3== 1){
            $validacion->v12_seg_add_3 = $request->v12_seg_add_3;
        } else
            $validacion->v12_seg_add_3 = 0;


        if ($request->v13_seg_add_3== 1){
            $validacion->v13_seg_add_3 = $request->v13_seg_add_3;
        } else
            $validacion->v13_seg_add_3 = 0;


        if ($request->v16_seg_add_3== 1){
            $validacion->v16_seg_add_3 = $request->v16_seg_add_3;
        } else
            $validacion->v16_seg_add_3 = 0;


        if ($request->v17_seg_add_3== 1){
            $validacion->v17_seg_add_3 = $request->v17_seg_add_3;
        } else
            $validacion->v17_seg_add_3 = 0;


        if ($request->v18_seg_add_3== 1){
            $validacion->v18_seg_add_3 = $request->v18_seg_add_3;
        } else
            $validacion->v18_seg_add_3 = 0;


        if ($request->v19_seg_add_3== 1){
            $validacion->v19_seg_add_3 = $request->v19_seg_add_3;
        } else
            $validacion->v19_seg_add_3 = 0;

        if ($request->v20_seg_add_3 == 1){
            $validacion->v20_seg_add_3 = $request->v20_seg_add_3;
        } else
            $validacion->v20_seg_add_3= 0;

        if ($request->v21_seg_add_3== 1){
            $validacion->v21_seg_add_3= $request->v21_seg_add_3;
        } else
            $validacion->v21_seg_add_3= 0;

        if ($request->v1_aus_anio== 1){
            $validacion->v1_aus_anio= $request->v1_aus_anio;
        } else
            $validacion->v1_aus_anio = 0;

        if ($request->v2_aus_anio== 1){
            $validacion->v2_aus_anio= $request->v2_aus_anio;
        } else
            $validacion->v2_aus_anio= 0;


        if ($request->v3_aus_anio== 1){
            $validacion->v3_aus_anio = $request->v3_aus_anio;
        } else
            $validacion->v3_aus_anio = 0;


        if ($request->v4_aus_anio== 1){
            $validacion->v4_aus_anio= $request->v4_aus_anio;
        } else
            $validacion->v4_aus_anio= 0;

        if ($request->v6_aus_anio== 1){
            $validacion->v6_aus_anio= $request->v6_aus_anio;
        } else
            $validacion->v6_aus_anio = 0;

        if ($request->v7_aus_anio== 1){
            $validacion->v7_aus_anio = $request->v7_aus_anio;
        } else
            $validacion->v7_aus_anio = 0;


        if ($request->v8_aus_anio== 1){
            $validacion->v8_aus_anio = $request->v8_aus_anio;
        } else
            $validacion->v8_aus_anio = 0;


        if ($request->v9_aus_anio== 1){
            $validacion->v9_aus_anio = $request->v9_aus_anio;
        } else
            $validacion->v9_aus_anio = 0;


        if ($request->v10_aus_anio== 1){
            $validacion->v10_aus_anio = $request->v10_aus_anio;
        } else
            $validacion->v10_aus_anio = 0;


        if ($request->v11_aus_anio== 1){
            $validacion->v11_aus_anio = $request->v11_aus_anio;
        } else
            $validacion->v11_aus_anio = 0;


        if ($request->v12_aus_anio== 1){
            $validacion->v12_aus_anio = $request->v12_aus_anio;
        } else
            $validacion->v12_aus_anio = 0;


        if ($request->v13_aus_anio== 1){
            $validacion->v13_aus_anio = $request->v13_aus_anio;
        } else
            $validacion->v13_aus_anio = 0;


        if ($request->v16_aus_anio== 1){
            $validacion->v16_aus_anio = $request->v16_aus_anio;
        } else
            $validacion->v16_aus_anio = 0;


        if ($request->v17_aus_anio== 1){
            $validacion->v17_aus_anio = $request->v17_aus_anio;
        } else
            $validacion->v17_aus_anio = 0;


        if ($request->v18_aus_anio== 1){
            $validacion->v18_aus_anio = $request->v18_aus_anio;
        } else
            $validacion->v18_aus_anio = 0;


        if ($request->v19_aus_anio== 1){
            $validacion->v19_aus_anio = $request->v19_aus_anio;
        } else
            $validacion->v19_aus_anio = 0;

        if ($request->v20_aus_anio == 1){
            $validacion->v20_aus_anio = $request->v20_aus_anio;
        } else
            $validacion->v20_aus_anio= 0;

        if ($request->v21_aus_anio== 1){
            $validacion->v21_aus_anio= $request->v21_aus_anio;
        } else
            $validacion->v21_aus_anio= 0;

        if ($request->v1_desempenio== 1){
            $validacion->v1_desempenio= $request->v1_desempenio;
        } else
            $validacion->v1_desempenio = 0;

        if ($request->v2_desempenio== 1){
            $validacion->v2_desempenio= $request->v2_desempenio;
        } else
            $validacion->v2_desempenio= 0;


        if ($request->v3_desempenio== 1){
            $validacion->v3_desempenio = $request->v3_desempenio;
        } else
            $validacion->v3_desempenio = 0;


        if ($request->v4_desempenio== 1){
            $validacion->v4_desempenio= $request->v4_desempenio;
        } else
            $validacion->v4_desempenio= 0;

        if ($request->v6_desempenio== 1){
            $validacion->v6_desempenio= $request->v6_desempenio;
        } else
            $validacion->v6_desempenio = 0;

        if ($request->v7_desempenio== 1){
            $validacion->v7_desempenio = $request->v7_desempenio;
        } else
            $validacion->v7_desempenio = 0;


        if ($request->v8_desempenio== 1){
            $validacion->v8_desempenio = $request->v8_desempenio;
        } else
            $validacion->v8_desempenio = 0;


        if ($request->v9_desempenio== 1){
            $validacion->v9_desempenio = $request->v9_desempenio;
        } else
            $validacion->v9_desempenio = 0;


        if ($request->v10_desempenio== 1){
            $validacion->v10_desempenio = $request->v10_desempenio;
        } else
            $validacion->v10_desempenio = 0;


        if ($request->v11_desempenio== 1){
            $validacion->v11_desempenio = $request->v11_desempenio;
        } else
            $validacion->v11_desempenio = 0;


        if ($request->v12_desempenio== 1){
            $validacion->v12_desempenio = $request->v12_desempenio;
        } else
            $validacion->v12_desempenio = 0;


        if ($request->v13_desempenio== 1){
            $validacion->v13_desempenio = $request->v13_desempenio;
        } else
            $validacion->v13_desempenio = 0;


        if ($request->v16_desempenio== 1){
            $validacion->v16_desempenio = $request->v16_desempenio;
        } else
            $validacion->v16_desempenio = 0;


        if ($request->v17_desempenio== 1){
            $validacion->v17_desempenio = $request->v17_desempenio;
        } else
            $validacion->v17_desempenio = 0;


        if ($request->v18_desempenio== 1){
            $validacion->v18_desempenio = $request->v18_desempenio;
        } else
            $validacion->v18_desempenio = 0;


        if ($request->v19_desempenio== 1){
            $validacion->v19_desempenio = $request->v19_desempenio;
        } else
            $validacion->v19_desempenio = 0;

        if ($request->v20_desempenio == 1){
            $validacion->v20_desempenio = $request->v20_desempenio;
        } else
            $validacion->v20_desempenio= 0;

        if ($request->v21_desempenio== 1){
            $validacion->v21_desempenio= $request->v21_desempenio;
        } else
            $validacion->v21_desempenio= 0;

        if ($request->v1_cump_metas== 1){
            $validacion->v1_cump_metas= $request->v1_cump_metas;
        } else
            $validacion->v1_cump_metas = 0;

        if ($request->v2_cump_metas== 1){
            $validacion->v2_cump_metas= $request->v2_cump_metas;
        } else
            $validacion->v2_cump_metas= 0;


        if ($request->v3_cump_metas== 1){
            $validacion->v3_cump_metas = $request->v3_cump_metas;
        } else
            $validacion->v3_cump_metas = 0;


        if ($request->v4_cump_metas== 1){
            $validacion->v4_cump_metas= $request->v4_cump_metas;
        } else
            $validacion->v4_cump_metas= 0;

        if ($request->v6_cump_metas== 1){
            $validacion->v6_cump_metas= $request->v6_cump_metas;
        } else
            $validacion->v6_cump_metas = 0;

        if ($request->v7_cump_metas== 1){
            $validacion->v7_cump_metas = $request->v7_cump_metas;
        } else
            $validacion->v7_cump_metas = 0;


        if ($request->v8_cump_metas== 1){
            $validacion->v8_cump_metas = $request->v8_cump_metas;
        } else
            $validacion->v8_cump_metas = 0;


        if ($request->v9_cump_metas== 1){
            $validacion->v9_cump_metas = $request->v9_cump_metas;
        } else
            $validacion->v9_cump_metas = 0;


        if ($request->v10_cump_metas== 1){
            $validacion->v10_cump_metas = $request->v10_cump_metas;
        } else
            $validacion->v10_cump_metas = 0;


        if ($request->v11_cump_metas== 1){
            $validacion->v11_cump_metas = $request->v11_cump_metas;
        } else
            $validacion->v11_cump_metas = 0;


        if ($request->v12_cump_metas== 1){
            $validacion->v12_cump_metas = $request->v12_cump_metas;
        } else
            $validacion->v12_cump_metas = 0;


        if ($request->v13_cump_metas== 1){
            $validacion->v13_cump_metas = $request->v13_cump_metas;
        } else
            $validacion->v13_cump_metas = 0;


        if ($request->v16_cump_metas== 1){
            $validacion->v16_cump_metas = $request->v16_cump_metas;
        } else
            $validacion->v16_cump_metas = 0;


        if ($request->v17_cump_metas== 1){
            $validacion->v17_cump_metas = $request->v17_cump_metas;
        } else
            $validacion->v17_cump_metas = 0;


        if ($request->v18_cump_metas== 1){
            $validacion->v18_cump_metas = $request->v18_cump_metas;
        } else
            $validacion->v18_cump_metas = 0;


        if ($request->v19_cump_metas== 1){
            $validacion->v19_cump_metas = $request->v19_cump_metas;
        } else
            $validacion->v19_cump_metas = 0;

        if ($request->v20_cump_metas == 1){
            $validacion->v20_cump_metas = $request->v20_cump_metas;
        } else
            $validacion->v20_cump_metas= 0;

        if ($request->v21_cump_metas== 1){
            $validacion->v21_cump_metas= $request->v21_cump_metas;
        } else
            $validacion->v21_cump_metas= 0;

        if ($request->v1_hr_extra== 1){
            $validacion->v1_hr_extra= $request->v1_hr_extra;
        } else
            $validacion->v1_hr_extra = 0;

        if ($request->v2_hr_extra== 1){
            $validacion->v2_hr_extra= $request->v2_hr_extra;
        } else
            $validacion->v2_hr_extra= 0;


        if ($request->v3_hr_extra== 1){
            $validacion->v3_hr_extra = $request->v3_hr_extra;
        } else
            $validacion->v3_hr_extra = 0;


        if ($request->v4_hr_extra== 1){
            $validacion->v4_hr_extra= $request->v4_hr_extra;
        } else
            $validacion->v4_hr_extra= 0;

        if ($request->v6_hr_extra== 1){
            $validacion->v6_hr_extra= $request->v6_hr_extra;
        } else
            $validacion->v6_hr_extra = 0;

        if ($request->v7_hr_extra== 1){
            $validacion->v7_hr_extra = $request->v7_hr_extra;
        } else
            $validacion->v7_hr_extra = 0;


        if ($request->v8_hr_extra== 1){
            $validacion->v8_hr_extra = $request->v8_hr_extra;
        } else
            $validacion->v8_hr_extra = 0;


        if ($request->v9_hr_extra== 1){
            $validacion->v9_hr_extra = $request->v9_hr_extra;
        } else
            $validacion->v9_hr_extra = 0;


        if ($request->v10_hr_extra== 1){
            $validacion->v10_hr_extra = $request->v10_hr_extra;
        } else
            $validacion->v10_hr_extra = 0;


        if ($request->v11_hr_extra== 1){
            $validacion->v11_hr_extra = $request->v11_hr_extra;
        } else
            $validacion->v11_hr_extra = 0;


        if ($request->v12_hr_extra== 1){
            $validacion->v12_hr_extra = $request->v12_hr_extra;
        } else
            $validacion->v12_hr_extra = 0;


        if ($request->v13_hr_extra== 1){
            $validacion->v13_hr_extra = $request->v13_hr_extra;
        } else
            $validacion->v13_hr_extra = 0;


        if ($request->v16_hr_extra== 1){
            $validacion->v16_hr_extra = $request->v16_hr_extra;
        } else
            $validacion->v16_hr_extra = 0;


        if ($request->v17_hr_extra== 1){
            $validacion->v17_hr_extra = $request->v17_hr_extra;
        } else
            $validacion->v17_hr_extra = 0;


        if ($request->v18_hr_extra== 1){
            $validacion->v18_hr_extra = $request->v18_hr_extra;
        } else
            $validacion->v18_hr_extra = 0;


        if ($request->v19_hr_extra== 1){
            $validacion->v19_hr_extra = $request->v19_hr_extra;
        } else
            $validacion->v19_hr_extra = 0;

        if ($request->v20_hr_extra == 1){
            $validacion->v20_hr_extra = $request->v20_hr_extra;
        } else
            $validacion->v20_hr_extra= 0;

        if ($request->v21_hr_extra== 1){
            $validacion->v21_hr_extra= $request->v21_hr_extra;
        } else
            $validacion->v21_hr_extra= 0;

        if ($request->v1_hr_capaci== 1){
            $validacion->v1_hr_capaci= $request->v1_hr_capaci;
        } else
            $validacion->v1_hr_capaci = 0;

        if ($request->v2_hr_capaci== 1){
            $validacion->v2_hr_capaci= $request->v2_hr_capaci;
        } else
            $validacion->v2_hr_capaci= 0;


        if ($request->v3_hr_capaci== 1){
            $validacion->v3_hr_capaci = $request->v3_hr_capaci;
        } else
            $validacion->v3_hr_capaci = 0;


        if ($request->v4_hr_capaci== 1){
            $validacion->v4_hr_capaci= $request->v4_hr_capaci;
        } else
            $validacion->v4_hr_capaci= 0;

        if ($request->v6_hr_capaci== 1){
            $validacion->v6_hr_capaci= $request->v6_hr_capaci;
        } else
            $validacion->v6_hr_capaci = 0;

        if ($request->v7_hr_capaci== 1){
            $validacion->v7_hr_capaci = $request->v7_hr_capaci;
        } else
            $validacion->v7_hr_capaci = 0;


        if ($request->v8_hr_capaci== 1){
            $validacion->v8_hr_capaci = $request->v8_hr_capaci;
        } else
            $validacion->v8_hr_capaci = 0;


        if ($request->v9_hr_capaci== 1){
            $validacion->v9_hr_capaci = $request->v9_hr_capaci;
        } else
            $validacion->v9_hr_capaci = 0;


        if ($request->v10_hr_capaci== 1){
            $validacion->v10_hr_capaci = $request->v10_hr_capaci;
        } else
            $validacion->v10_hr_capaci = 0;


        if ($request->v11_hr_capaci== 1){
            $validacion->v11_hr_capaci = $request->v11_hr_capaci;
        } else
            $validacion->v11_hr_capaci = 0;


        if ($request->v12_hr_capaci== 1){
            $validacion->v12_hr_capaci = $request->v12_hr_capaci;
        } else
            $validacion->v12_hr_capaci = 0;


        if ($request->v13_hr_capaci== 1){
            $validacion->v13_hr_capaci = $request->v13_hr_capaci;
        } else
            $validacion->v13_hr_capaci = 0;


        if ($request->v16_hr_capaci== 1){
            $validacion->v16_hr_capaci = $request->v16_hr_capaci;
        } else
            $validacion->v16_hr_capaci = 0;


        if ($request->v17_hr_capaci== 1){
            $validacion->v17_hr_capaci = $request->v17_hr_capaci;
        } else
            $validacion->v17_hr_capaci = 0;


        if ($request->v18_hr_capaci== 1){
            $validacion->v18_hr_capaci = $request->v18_hr_capaci;
        } else
            $validacion->v18_hr_capaci = 0;


        if ($request->v19_hr_capaci== 1){
            $validacion->v19_hr_capaci = $request->v19_hr_capaci;
        } else
            $validacion->v19_hr_capaci = 0;

        if ($request->v20_hr_capaci == 1){
            $validacion->v20_hr_capaci = $request->v20_hr_capaci;
        } else
            $validacion->v20_hr_capaci= 0;

        if ($request->v21_hr_capaci== 1){
            $validacion->v21_hr_capaci= $request->v21_hr_capaci;
        } else
            $validacion->v21_hr_capaci= 0;

        if ($request->v1_t_contrato== 1){
            $validacion->v1_t_contrato= $request->v1_t_contrato;
        } else
            $validacion->v1_t_contrato = 0;

        if ($request->v2_t_contrato== 1){
            $validacion->v2_t_contrato= $request->v2_t_contrato;
        } else
            $validacion->v2_t_contrato= 0;


        if ($request->v3_t_contrato== 1){
            $validacion->v3_t_contrato = $request->v3_t_contrato;
        } else
            $validacion->v3_t_contrato = 0;


        if ($request->v4_t_contrato== 1){
            $validacion->v4_t_contrato= $request->v4_t_contrato;
        } else
            $validacion->v4_t_contrato= 0;

        if ($request->v6_t_contrato== 1){
            $validacion->v6_t_contrato= $request->v6_t_contrato;
        } else
            $validacion->v6_t_contrato = 0;

        if ($request->v7_t_contrato== 1){
            $validacion->v7_t_contrato = $request->v7_t_contrato;
        } else
            $validacion->v7_t_contrato = 0;


        if ($request->v8_t_contrato== 1){
            $validacion->v8_t_contrato = $request->v8_t_contrato;
        } else
            $validacion->v8_t_contrato = 0;


        if ($request->v9_t_contrato== 1){
            $validacion->v9_t_contrato = $request->v9_t_contrato;
        } else
            $validacion->v9_t_contrato = 0;


        if ($request->v10_t_contrato== 1){
            $validacion->v10_t_contrato = $request->v10_t_contrato;
        } else
            $validacion->v10_t_contrato = 0;


        if ($request->v11_t_contrato== 1){
            $validacion->v11_t_contrato = $request->v11_t_contrato;
        } else
            $validacion->v11_t_contrato = 0;


        if ($request->v12_t_contrato== 1){
            $validacion->v12_t_contrato = $request->v12_t_contrato;
        } else
            $validacion->v12_t_contrato = 0;


        if ($request->v13_t_contrato== 1){
            $validacion->v13_t_contrato = $request->v13_t_contrato;
        } else
            $validacion->v13_t_contrato = 0;


        if ($request->v16_t_contrato== 1){
            $validacion->v16_t_contrato = $request->v16_t_contrato;
        } else
            $validacion->v16_t_contrato = 0;


        if ($request->v17_t_contrato== 1){
            $validacion->v17_t_contrato = $request->v17_t_contrato;
        } else
            $validacion->v17_t_contrato = 0;


        if ($request->v18_t_contrato== 1){
            $validacion->v18_t_contrato = $request->v18_t_contrato;
        } else
            $validacion->v18_t_contrato = 0;


        if ($request->v19_t_contrato== 1){
            $validacion->v19_t_contrato = $request->v19_t_contrato;
        } else
            $validacion->v19_t_contrato = 0;

        if ($request->v20_t_contrato == 1){
            $validacion->v20_t_contrato = $request->v20_t_contrato;
        } else
            $validacion->v20_t_contrato= 0;

        if ($request->v21_t_contrato== 1){
            $validacion->v21_t_contrato= $request->v21_t_contrato;
        } else
            $validacion->v21_t_contrato= 0;

        if ($request->v1_t_remun== 1){
            $validacion->v1_t_remun= $request->v1_t_remun;
        } else
            $validacion->v1_t_remun = 0;

        if ($request->v2_t_remun== 1){
            $validacion->v2_t_remun= $request->v2_t_remun;
        } else
            $validacion->v2_t_remun= 0;


        if ($request->v3_t_remun== 1){
            $validacion->v3_t_remun = $request->v3_t_remun;
        } else
            $validacion->v3_t_remun = 0;


        if ($request->v4_t_remun== 1){
            $validacion->v4_t_remun= $request->v4_t_remun;
        } else
            $validacion->v4_t_remun= 0;

        if ($request->v6_t_remun== 1){
            $validacion->v6_t_remun= $request->v6_t_remun;
        } else
            $validacion->v6_t_remun = 0;

        if ($request->v7_t_remun== 1){
            $validacion->v7_t_remun = $request->v7_t_remun;
        } else
            $validacion->v7_t_remun = 0;


        if ($request->v8_t_remun== 1){
            $validacion->v8_t_remun = $request->v8_t_remun;
        } else
            $validacion->v8_t_remun = 0;


        if ($request->v9_t_remun== 1){
            $validacion->v9_t_remun = $request->v9_t_remun;
        } else
            $validacion->v9_t_remun = 0;


        if ($request->v10_t_remun== 1){
            $validacion->v10_t_remun = $request->v10_t_remun;
        } else
            $validacion->v10_t_remun = 0;


        if ($request->v11_t_remun== 1){
            $validacion->v11_t_remun = $request->v11_t_remun;
        } else
            $validacion->v11_t_remun = 0;


        if ($request->v12_t_remun== 1){
            $validacion->v12_t_remun = $request->v12_t_remun;
        } else
            $validacion->v12_t_remun = 0;


        if ($request->v13_t_remun== 1){
            $validacion->v13_t_remun = $request->v13_t_remun;
        } else
            $validacion->v13_t_remun = 0;


        if ($request->v16_t_remun== 1){
            $validacion->v16_t_remun = $request->v16_t_remun;
        } else
            $validacion->v16_t_remun = 0;


        if ($request->v17_t_remun== 1){
            $validacion->v17_t_remun = $request->v17_t_remun;
        } else
            $validacion->v17_t_remun = 0;


        if ($request->v18_t_remun== 1){
            $validacion->v18_t_remun = $request->v18_t_remun;
        } else
            $validacion->v18_t_remun = 0;


        if ($request->v19_t_remun== 1){
            $validacion->v19_t_remun = $request->v19_t_remun;
        } else
            $validacion->v19_t_remun = 0;

        if ($request->v20_t_remun == 1){
            $validacion->v20_t_remun = $request->v20_t_remun;
        } else
            $validacion->v20_t_remun= 0;

        if ($request->v21_t_remun== 1){
            $validacion->v21_t_remun= $request->v21_t_remun;
        } else
            $validacion->v21_t_remun= 0;

        if ($request->v1_sindicalizado== 1){
            $validacion->v1_sindicalizado= $request->v1_sindicalizado;
        } else
            $validacion->v1_sindicalizado = 0;

        if ($request->v2_sindicalizado== 1){
            $validacion->v2_sindicalizado= $request->v2_sindicalizado;
        } else
            $validacion->v2_sindicalizado= 0;


        if ($request->v3_sindicalizado== 1){
            $validacion->v3_sindicalizado = $request->v3_sindicalizado;
        } else
            $validacion->v3_sindicalizado = 0;


        if ($request->v4_sindicalizado== 1){
            $validacion->v4_sindicalizado= $request->v4_sindicalizado;
        } else
            $validacion->v4_sindicalizado= 0;

        if ($request->v6_sindicalizado== 1){
            $validacion->v6_sindicalizado= $request->v6_sindicalizado;
        } else
            $validacion->v6_sindicalizado = 0;

        if ($request->v7_sindicalizado== 1){
            $validacion->v7_sindicalizado = $request->v7_sindicalizado;
        } else
            $validacion->v7_sindicalizado = 0;


        if ($request->v8_sindicalizado== 1){
            $validacion->v8_sindicalizado = $request->v8_sindicalizado;
        } else
            $validacion->v8_sindicalizado = 0;


        if ($request->v9_sindicalizado== 1){
            $validacion->v9_sindicalizado = $request->v9_sindicalizado;
        } else
            $validacion->v9_sindicalizado = 0;


        if ($request->v10_sindicalizado== 1){
            $validacion->v10_sindicalizado = $request->v10_sindicalizado;
        } else
            $validacion->v10_sindicalizado = 0;


        if ($request->v11_sindicalizado== 1){
            $validacion->v11_sindicalizado = $request->v11_sindicalizado;
        } else
            $validacion->v11_sindicalizado = 0;


        if ($request->v12_sindicalizado== 1){
            $validacion->v12_sindicalizado = $request->v12_sindicalizado;
        } else
            $validacion->v12_sindicalizado = 0;


        if ($request->v13_sindicalizado== 1){
            $validacion->v13_sindicalizado = $request->v13_sindicalizado;
        } else
            $validacion->v13_sindicalizado = 0;


        if ($request->v16_sindicalizado== 1){
            $validacion->v16_sindicalizado = $request->v16_sindicalizado;
        } else
            $validacion->v16_sindicalizado = 0;


        if ($request->v17_sindicalizado== 1){
            $validacion->v17_sindicalizado = $request->v17_sindicalizado;
        } else
            $validacion->v17_sindicalizado = 0;


        if ($request->v18_sindicalizado== 1){
            $validacion->v18_sindicalizado = $request->v18_sindicalizado;
        } else
            $validacion->v18_sindicalizado = 0;


        if ($request->v19_sindicalizado== 1){
            $validacion->v19_sindicalizado = $request->v19_sindicalizado;
        } else
            $validacion->v19_sindicalizado = 0;

        if ($request->v20_sindicalizado == 1){
            $validacion->v20_sindicalizado = $request->v20_sindicalizado;
        } else
            $validacion->v20_sindicalizado= 0;

        if ($request->v21_sindicalizado== 1){
            $validacion->v21_sindicalizado= $request->v21_sindicalizado;
        } else
            $validacion->v21_sindicalizado= 0;

        if ($request->v1_hr1== 1){
            $validacion->v1_hr1= $request->v1_hr1;
        } else
            $validacion->v1_hr1 = 0;

        if ($request->v2_hr1== 1){
            $validacion->v2_hr1= $request->v2_hr1;
        } else
            $validacion->v2_hr1= 0;


        if ($request->v3_hr1== 1){
            $validacion->v3_hr1 = $request->v3_hr1;
        } else
            $validacion->v3_hr1 = 0;


        if ($request->v4_hr1== 1){
            $validacion->v4_hr1= $request->v4_hr1;
        } else
            $validacion->v4_hr1= 0;

        if ($request->v6_hr1== 1){
            $validacion->v6_hr1= $request->v6_hr1;
        } else
            $validacion->v6_hr1 = 0;

        if ($request->v7_hr1== 1){
            $validacion->v7_hr1 = $request->v7_hr1;
        } else
            $validacion->v7_hr1 = 0;


        if ($request->v8_hr1== 1){
            $validacion->v8_hr1 = $request->v8_hr1;
        } else
            $validacion->v8_hr1 = 0;


        if ($request->v9_hr1== 1){
            $validacion->v9_hr1 = $request->v9_hr1;
        } else
            $validacion->v9_hr1 = 0;


        if ($request->v10_hr1== 1){
            $validacion->v10_hr1 = $request->v10_hr1;
        } else
            $validacion->v10_hr1 = 0;


        if ($request->v11_hr1== 1){
            $validacion->v11_hr1 = $request->v11_hr1;
        } else
            $validacion->v11_hr1 = 0;


        if ($request->v12_hr1== 1){
            $validacion->v12_hr1 = $request->v12_hr1;
        } else
            $validacion->v12_hr1 = 0;


        if ($request->v13_hr1== 1){
            $validacion->v13_hr1 = $request->v13_hr1;
        } else
            $validacion->v13_hr1 = 0;


        if ($request->v16_hr1== 1){
            $validacion->v16_hr1 = $request->v16_hr1;
        } else
            $validacion->v16_hr1 = 0;


        if ($request->v17_hr1== 1){
            $validacion->v17_hr1 = $request->v17_hr1;
        } else
            $validacion->v17_hr1 = 0;


        if ($request->v18_hr1== 1){
            $validacion->v18_hr1 = $request->v18_hr1;
        } else
            $validacion->v18_hr1 = 0;


        if ($request->v19_hr1== 1){
            $validacion->v19_hr1 = $request->v19_hr1;
        } else
            $validacion->v19_hr1 = 0;

        if ($request->v20_hr1 == 1){
            $validacion->v20_hr1 = $request->v20_hr1;
        } else
            $validacion->v20_hr1= 0;

        if ($request->v21_hr1== 1){
            $validacion->v21_hr1= $request->v21_hr1;
        } else
            $validacion->v21_hr1= 0;

        if ($request->v1_hr2== 1){
            $validacion->v1_hr2= $request->v1_hr2;
        } else
            $validacion->v1_hr2 = 0;

        if ($request->v2_hr2== 1){
            $validacion->v2_hr2= $request->v2_hr2;
        } else
            $validacion->v2_hr2= 0;


        if ($request->v3_hr2== 1){
            $validacion->v3_hr2 = $request->v3_hr2;
        } else
            $validacion->v3_hr2 = 0;


        if ($request->v4_hr2== 1){
            $validacion->v4_hr2= $request->v4_hr2;
        } else
            $validacion->v4_hr2= 0;

        if ($request->v6_hr2== 1){
            $validacion->v6_hr2= $request->v6_hr2;
        } else
            $validacion->v6_hr2 = 0;

        if ($request->v7_hr2== 1){
            $validacion->v7_hr2 = $request->v7_hr2;
        } else
            $validacion->v7_hr2 = 0;


        if ($request->v8_hr2== 1){
            $validacion->v8_hr2 = $request->v8_hr2;
        } else
            $validacion->v8_hr2 = 0;


        if ($request->v9_hr2== 1){
            $validacion->v9_hr2 = $request->v9_hr2;
        } else
            $validacion->v9_hr2 = 0;


        if ($request->v10_hr2== 1){
            $validacion->v10_hr2 = $request->v10_hr2;
        } else
            $validacion->v10_hr2 = 0;


        if ($request->v11_hr2== 1){
            $validacion->v11_hr2 = $request->v11_hr2;
        } else
            $validacion->v11_hr2 = 0;


        if ($request->v12_hr2== 1){
            $validacion->v12_hr2 = $request->v12_hr2;
        } else
            $validacion->v12_hr2 = 0;


        if ($request->v13_hr2== 1){
            $validacion->v13_hr2 = $request->v13_hr2;
        } else
            $validacion->v13_hr2 = 0;


        if ($request->v16_hr2== 1){
            $validacion->v16_hr2 = $request->v16_hr2;
        } else
            $validacion->v16_hr2 = 0;


        if ($request->v17_hr2== 1){
            $validacion->v17_hr2 = $request->v17_hr2;
        } else
            $validacion->v17_hr2 = 0;


        if ($request->v18_hr2== 1){
            $validacion->v18_hr2 = $request->v18_hr2;
        } else
            $validacion->v18_hr2 = 0;


        if ($request->v19_hr2== 1){
            $validacion->v19_hr2 = $request->v19_hr2;
        } else
            $validacion->v19_hr2 = 0;

        if ($request->v20_hr2 == 1){
            $validacion->v20_hr2 = $request->v20_hr2;
        } else
            $validacion->v20_hr2= 0;

        if ($request->v21_hr2== 1){
            $validacion->v21_hr2= $request->v21_hr2;
        } else
            $validacion->v21_hr2= 0;

        if ($request->v1_hr3== 1){
            $validacion->v1_hr3= $request->v1_hr3;
        } else
            $validacion->v1_hr3 = 0;

        if ($request->v2_hr3== 1){
            $validacion->v2_hr3= $request->v2_hr3;
        } else
            $validacion->v2_hr3= 0;


        if ($request->v3_hr3== 1){
            $validacion->v3_hr3 = $request->v3_hr3;
        } else
            $validacion->v3_hr3 = 0;


        if ($request->v4_hr3== 1){
            $validacion->v4_hr3= $request->v4_hr3;
        } else
            $validacion->v4_hr3= 0;

        if ($request->v6_hr3== 1){
            $validacion->v6_hr3= $request->v6_hr3;
        } else
            $validacion->v6_hr3 = 0;

        if ($request->v7_hr3== 1){
            $validacion->v7_hr3 = $request->v7_hr3;
        } else
            $validacion->v7_hr3 = 0;


        if ($request->v8_hr3== 1){
            $validacion->v8_hr3 = $request->v8_hr3;
        } else
            $validacion->v8_hr3 = 0;


        if ($request->v9_hr3== 1){
            $validacion->v9_hr3 = $request->v9_hr3;
        } else
            $validacion->v9_hr3 = 0;


        if ($request->v10_hr3== 1){
            $validacion->v10_hr3 = $request->v10_hr3;
        } else
            $validacion->v10_hr3 = 0;


        if ($request->v11_hr3== 1){
            $validacion->v11_hr3 = $request->v11_hr3;
        } else
            $validacion->v11_hr3 = 0;


        if ($request->v12_hr3== 1){
            $validacion->v12_hr3 = $request->v12_hr3;
        } else
            $validacion->v12_hr3 = 0;


        if ($request->v13_hr3== 1){
            $validacion->v13_hr3 = $request->v13_hr3;
        } else
            $validacion->v13_hr3 = 0;


        if ($request->v16_hr3== 1){
            $validacion->v16_hr3 = $request->v16_hr3;
        } else
            $validacion->v16_hr3 = 0;


        if ($request->v17_hr3== 1){
            $validacion->v17_hr3 = $request->v17_hr3;
        } else
            $validacion->v17_hr3 = 0;


        if ($request->v18_hr3== 1){
            $validacion->v18_hr3 = $request->v18_hr3;
        } else
            $validacion->v18_hr3 = 0;


        if ($request->v19_hr3== 1){
            $validacion->v19_hr3 = $request->v19_hr3;
        } else
            $validacion->v19_hr3 = 0;

        if ($request->v20_hr3 == 1){
            $validacion->v20_hr3 = $request->v20_hr3;
        } else
            $validacion->v20_hr3= 0;

        if ($request->v21_hr3== 1){
            $validacion->v21_hr3= $request->v21_hr3;
        } else
            $validacion->v21_hr3= 0;

        if ($request->v1_hr4== 1){
            $validacion->v1_hr4= $request->v1_hr4;
        } else
            $validacion->v1_hr4 = 0;

        if ($request->v2_hr4== 1){
            $validacion->v2_hr4= $request->v2_hr4;
        } else
            $validacion->v2_hr4= 0;


        if ($request->v3_hr4== 1){
            $validacion->v3_hr4 = $request->v3_hr4;
        } else
            $validacion->v3_hr4 = 0;


        if ($request->v4_hr4== 1){
            $validacion->v4_hr4= $request->v4_hr4;
        } else
            $validacion->v4_hr4= 0;

        if ($request->v6_hr4== 1){
            $validacion->v6_hr4= $request->v6_hr4;
        } else
            $validacion->v6_hr4 = 0;

        if ($request->v7_hr4== 1){
            $validacion->v7_hr4 = $request->v7_hr4;
        } else
            $validacion->v7_hr4 = 0;


        if ($request->v8_hr4== 1){
            $validacion->v8_hr4 = $request->v8_hr4;
        } else
            $validacion->v8_hr4 = 0;


        if ($request->v9_hr4== 1){
            $validacion->v9_hr4 = $request->v9_hr4;
        } else
            $validacion->v9_hr4 = 0;


        if ($request->v10_hr4== 1){
            $validacion->v10_hr4 = $request->v10_hr4;
        } else
            $validacion->v10_hr4 = 0;


        if ($request->v11_hr4== 1){
            $validacion->v11_hr4 = $request->v11_hr4;
        } else
            $validacion->v11_hr4 = 0;


        if ($request->v12_hr4== 1){
            $validacion->v12_hr4 = $request->v12_hr4;
        } else
            $validacion->v12_hr4 = 0;


        if ($request->v13_hr4== 1){
            $validacion->v13_hr4 = $request->v13_hr4;
        } else
            $validacion->v13_hr4 = 0;


        if ($request->v16_hr4== 1){
            $validacion->v16_hr4 = $request->v16_hr4;
        } else
            $validacion->v16_hr4 = 0;


        if ($request->v17_hr4== 1){
            $validacion->v17_hr4 = $request->v17_hr4;
        } else
            $validacion->v17_hr4 = 0;


        if ($request->v18_hr4== 1){
            $validacion->v18_hr4 = $request->v18_hr4;
        } else
            $validacion->v18_hr4 = 0;


        if ($request->v19_hr4== 1){
            $validacion->v19_hr4 = $request->v19_hr4;
        } else
            $validacion->v19_hr4 = 0;

        if ($request->v20_hr4 == 1){
            $validacion->v20_hr4 = $request->v20_hr4;
        } else
            $validacion->v20_hr4= 0;

        if ($request->v21_hr4== 1){
            $validacion->v21_hr4= $request->v21_hr4;
        } else
            $validacion->v21_hr4= 0;

        if ($request->v1_hr5== 1){
            $validacion->v1_hr5= $request->v1_hr5;
        } else
            $validacion->v1_hr5 = 0;

        if ($request->v2_hr5== 1){
            $validacion->v2_hr5= $request->v2_hr5;
        } else
            $validacion->v2_hr5= 0;


        if ($request->v3_hr5== 1){
            $validacion->v3_hr5 = $request->v3_hr5;
        } else
            $validacion->v3_hr5 = 0;


        if ($request->v4_hr5== 1){
            $validacion->v4_hr5= $request->v4_hr5;
        } else
            $validacion->v4_hr5= 0;

        if ($request->v6_hr5== 1){
            $validacion->v6_hr5= $request->v6_hr5;
        } else
            $validacion->v6_hr5 = 0;

        if ($request->v7_hr5== 1){
            $validacion->v7_hr5 = $request->v7_hr5;
        } else
            $validacion->v7_hr5 = 0;


        if ($request->v8_hr5== 1){
            $validacion->v8_hr5 = $request->v8_hr5;
        } else
            $validacion->v8_hr5 = 0;


        if ($request->v9_hr5== 1){
            $validacion->v9_hr5 = $request->v9_hr5;
        } else
            $validacion->v9_hr5 = 0;


        if ($request->v10_hr5== 1){
            $validacion->v10_hr5 = $request->v10_hr5;
        } else
            $validacion->v10_hr5 = 0;


        if ($request->v11_hr5== 1){
            $validacion->v11_hr5 = $request->v11_hr5;
        } else
            $validacion->v11_hr5 = 0;


        if ($request->v12_hr5== 1){
            $validacion->v12_hr5 = $request->v12_hr5;
        } else
            $validacion->v12_hr5 = 0;


        if ($request->v13_hr5== 1){
            $validacion->v13_hr5 = $request->v13_hr5;
        } else
            $validacion->v13_hr5 = 0;


        if ($request->v16_hr5== 1){
            $validacion->v16_hr5 = $request->v16_hr5;
        } else
            $validacion->v16_hr5 = 0;


        if ($request->v17_hr5== 1){
            $validacion->v17_hr5 = $request->v17_hr5;
        } else
            $validacion->v17_hr5 = 0;


        if ($request->v18_hr5== 1){
            $validacion->v18_hr5 = $request->v18_hr5;
        } else
            $validacion->v18_hr5 = 0;


        if ($request->v19_hr5== 1){
            $validacion->v19_hr5 = $request->v19_hr5;
        } else
            $validacion->v19_hr5 = 0;

        if ($request->v20_hr5 == 1){
            $validacion->v20_hr5 = $request->v20_hr5;
        } else
            $validacion->v20_hr5= 0;

        if ($request->v21_hr5== 1){
            $validacion->v21_hr5= $request->v21_hr5;
        } else
            $validacion->v21_hr5= 0;

        if ($request->v1_hr6== 1){
            $validacion->v1_hr6= $request->v1_hr6;
        } else
            $validacion->v1_hr6 = 0;

        if ($request->v2_hr6== 1){
            $validacion->v2_hr6= $request->v2_hr6;
        } else
            $validacion->v2_hr6= 0;


        if ($request->v3_hr6== 1){
            $validacion->v3_hr6 = $request->v3_hr6;
        } else
            $validacion->v3_hr6 = 0;


        if ($request->v4_hr6== 1){
            $validacion->v4_hr6= $request->v4_hr6;
        } else
            $validacion->v4_hr6= 0;

        if ($request->v6_hr6== 1){
            $validacion->v6_hr6= $request->v6_hr6;
        } else
            $validacion->v6_hr6 = 0;

        if ($request->v7_hr6== 1){
            $validacion->v7_hr6 = $request->v7_hr6;
        } else
            $validacion->v7_hr6 = 0;


        if ($request->v8_hr6== 1){
            $validacion->v8_hr6 = $request->v8_hr6;
        } else
            $validacion->v8_hr6 = 0;


        if ($request->v9_hr6== 1){
            $validacion->v9_hr6 = $request->v9_hr6;
        } else
            $validacion->v9_hr6 = 0;


        if ($request->v10_hr6== 1){
            $validacion->v10_hr6 = $request->v10_hr6;
        } else
            $validacion->v10_hr6 = 0;


        if ($request->v11_hr6== 1){
            $validacion->v11_hr6 = $request->v11_hr6;
        } else
            $validacion->v11_hr6 = 0;


        if ($request->v12_hr6== 1){
            $validacion->v12_hr6 = $request->v12_hr6;
        } else
            $validacion->v12_hr6 = 0;


        if ($request->v13_hr6== 1){
            $validacion->v13_hr6 = $request->v13_hr6;
        } else
            $validacion->v13_hr6 = 0;


        if ($request->v16_hr6== 1){
            $validacion->v16_hr6 = $request->v16_hr6;
        } else
            $validacion->v16_hr6 = 0;


        if ($request->v17_hr6== 1){
            $validacion->v17_hr6 = $request->v17_hr6;
        } else
            $validacion->v17_hr6 = 0;


        if ($request->v18_hr6== 1){
            $validacion->v18_hr6 = $request->v18_hr6;
        } else
            $validacion->v18_hr6 = 0;


        if ($request->v19_hr6== 1){
            $validacion->v19_hr6 = $request->v19_hr6;
        } else
            $validacion->v19_hr6 = 0;

        if ($request->v20_hr6 == 1){
            $validacion->v20_hr6 = $request->v20_hr6;
        } else
            $validacion->v20_hr6= 0;

        if ($request->v21_hr6== 1){
            $validacion->v21_hr6= $request->v21_hr6;
        } else
            $validacion->v21_hr6= 0;

        if ($request->v1_hr7== 1){
            $validacion->v1_hr7= $request->v1_hr7;
        } else
            $validacion->v1_hr7 = 0;

        if ($request->v2_hr7== 1){
            $validacion->v2_hr7= $request->v2_hr7;
        } else
            $validacion->v2_hr7= 0;


        if ($request->v3_hr7== 1){
            $validacion->v3_hr7 = $request->v3_hr7;
        } else
            $validacion->v3_hr7 = 0;


        if ($request->v4_hr7== 1){
            $validacion->v4_hr7= $request->v4_hr7;
        } else
            $validacion->v4_hr7= 0;

        if ($request->v6_hr7== 1){
            $validacion->v6_hr7= $request->v6_hr7;
        } else
            $validacion->v6_hr7 = 0;

        if ($request->v7_hr7== 1){
            $validacion->v7_hr7 = $request->v7_hr7;
        } else
            $validacion->v7_hr7 = 0;


        if ($request->v8_hr7== 1){
            $validacion->v8_hr7 = $request->v8_hr7;
        } else
            $validacion->v8_hr7 = 0;


        if ($request->v9_hr7== 1){
            $validacion->v9_hr7 = $request->v9_hr7;
        } else
            $validacion->v9_hr7 = 0;


        if ($request->v10_hr7== 1){
            $validacion->v10_hr7 = $request->v10_hr7;
        } else
            $validacion->v10_hr7 = 0;


        if ($request->v11_hr7== 1){
            $validacion->v11_hr7 = $request->v11_hr7;
        } else
            $validacion->v11_hr7 = 0;


        if ($request->v12_hr7== 1){
            $validacion->v12_hr7 = $request->v12_hr7;
        } else
            $validacion->v12_hr7 = 0;


        if ($request->v13_hr7== 1){
            $validacion->v13_hr7 = $request->v13_hr7;
        } else
            $validacion->v13_hr7 = 0;


        if ($request->v16_hr7== 1){
            $validacion->v16_hr7 = $request->v16_hr7;
        } else
            $validacion->v16_hr7 = 0;


        if ($request->v17_hr7== 1){
            $validacion->v17_hr7 = $request->v17_hr7;
        } else
            $validacion->v17_hr7 = 0;


        if ($request->v18_hr7== 1){
            $validacion->v18_hr7 = $request->v18_hr7;
        } else
            $validacion->v18_hr7 = 0;


        if ($request->v19_hr7== 1){
            $validacion->v19_hr7 = $request->v19_hr7;
        } else
            $validacion->v19_hr7 = 0;

        if ($request->v20_hr7 == 1){
            $validacion->v20_hr7 = $request->v20_hr7;
        } else
            $validacion->v20_hr7= 0;

        if ($request->v21_hr7== 1){
            $validacion->v21_hr7= $request->v21_hr7;
        } else
            $validacion->v21_hr7= 0;

        if ($request->v1_hr8== 1){
            $validacion->v1_hr8= $request->v1_hr8;
        } else
            $validacion->v1_hr8 = 0;

        if ($request->v2_hr8== 1){
            $validacion->v2_hr8= $request->v2_hr8;
        } else
            $validacion->v2_hr8= 0;


        if ($request->v3_hr8== 1){
            $validacion->v3_hr8 = $request->v3_hr8;
        } else
            $validacion->v3_hr8 = 0;


        if ($request->v4_hr8== 1){
            $validacion->v4_hr8= $request->v4_hr8;
        } else
            $validacion->v4_hr8= 0;

        if ($request->v6_hr8== 1){
            $validacion->v6_hr8= $request->v6_hr8;
        } else
            $validacion->v6_hr8 = 0;

        if ($request->v7_hr8== 1){
            $validacion->v7_hr8 = $request->v7_hr8;
        } else
            $validacion->v7_hr8 = 0;


        if ($request->v8_hr8== 1){
            $validacion->v8_hr8 = $request->v8_hr8;
        } else
            $validacion->v8_hr8 = 0;


        if ($request->v9_hr8== 1){
            $validacion->v9_hr8 = $request->v9_hr8;
        } else
            $validacion->v9_hr8 = 0;


        if ($request->v10_hr8== 1){
            $validacion->v10_hr8 = $request->v10_hr8;
        } else
            $validacion->v10_hr8 = 0;


        if ($request->v11_hr8== 1){
            $validacion->v11_hr8 = $request->v11_hr8;
        } else
            $validacion->v11_hr8 = 0;


        if ($request->v12_hr8== 1){
            $validacion->v12_hr8 = $request->v12_hr8;
        } else
            $validacion->v12_hr8 = 0;


        if ($request->v13_hr8== 1){
            $validacion->v13_hr8 = $request->v13_hr8;
        } else
            $validacion->v13_hr8 = 0;


        if ($request->v16_hr8== 1){
            $validacion->v16_hr8 = $request->v16_hr8;
        } else
            $validacion->v16_hr8 = 0;


        if ($request->v17_hr8== 1){
            $validacion->v17_hr8 = $request->v17_hr8;
        } else
            $validacion->v17_hr8 = 0;


        if ($request->v18_hr8== 1){
            $validacion->v18_hr8 = $request->v18_hr8;
        } else
            $validacion->v18_hr8 = 0;


        if ($request->v19_hr8== 1){
            $validacion->v19_hr8 = $request->v19_hr8;
        } else
            $validacion->v19_hr8 = 0;

        if ($request->v20_hr8 == 1){
            $validacion->v20_hr8 = $request->v20_hr8;
        } else
            $validacion->v20_hr8= 0;

        if ($request->v21_hr8== 1){
            $validacion->v21_hr8= $request->v21_hr8;
        } else
            $validacion->v21_hr8= 0;

        if ($request->v1_hr9== 1){
            $validacion->v1_hr9= $request->v1_hr9;
        } else
            $validacion->v1_hr9 = 0;

        if ($request->v2_hr9== 1){
            $validacion->v2_hr9= $request->v2_hr9;
        } else
            $validacion->v2_hr9= 0;


        if ($request->v3_hr9== 1){
            $validacion->v3_hr9 = $request->v3_hr9;
        } else
            $validacion->v3_hr9 = 0;


        if ($request->v4_hr9== 1){
            $validacion->v4_hr9= $request->v4_hr9;
        } else
            $validacion->v4_hr9= 0;

        if ($request->v6_hr9== 1){
            $validacion->v6_hr9= $request->v6_hr9;
        } else
            $validacion->v6_hr9 = 0;

        if ($request->v7_hr9== 1){
            $validacion->v7_hr9 = $request->v7_hr9;
        } else
            $validacion->v7_hr9 = 0;


        if ($request->v8_hr9== 1){
            $validacion->v8_hr9 = $request->v8_hr9;
        } else
            $validacion->v8_hr9 = 0;


        if ($request->v9_hr9== 1){
            $validacion->v9_hr9 = $request->v9_hr9;
        } else
            $validacion->v9_hr9 = 0;


        if ($request->v10_hr9== 1){
            $validacion->v10_hr9 = $request->v10_hr9;
        } else
            $validacion->v10_hr9 = 0;


        if ($request->v11_hr9== 1){
            $validacion->v11_hr9 = $request->v11_hr9;
        } else
            $validacion->v11_hr9 = 0;


        if ($request->v12_hr9== 1){
            $validacion->v12_hr9 = $request->v12_hr9;
        } else
            $validacion->v12_hr9 = 0;


        if ($request->v13_hr9== 1){
            $validacion->v13_hr9 = $request->v13_hr9;
        } else
            $validacion->v13_hr9 = 0;


        if ($request->v16_hr9== 1){
            $validacion->v16_hr9 = $request->v16_hr9;
        } else
            $validacion->v16_hr9 = 0;


        if ($request->v17_hr9== 1){
            $validacion->v17_hr9 = $request->v17_hr9;
        } else
            $validacion->v17_hr9 = 0;


        if ($request->v18_hr9== 1){
            $validacion->v18_hr9 = $request->v18_hr9;
        } else
            $validacion->v18_hr9 = 0;


        if ($request->v19_hr9== 1){
            $validacion->v19_hr9 = $request->v19_hr9;
        } else
            $validacion->v19_hr9 = 0;

        if ($request->v20_hr9 == 1){
            $validacion->v20_hr9 = $request->v20_hr9;
        } else
            $validacion->v20_hr9= 0;

        if ($request->v21_hr9== 1){
            $validacion->v21_hr9= $request->v21_hr9;
        } else
            $validacion->v21_hr9= 0;

        if ($request->v1_hr10== 1){
            $validacion->v1_hr10= $request->v1_hr10;
        } else
            $validacion->v1_hr10 = 0;

        if ($request->v2_hr10== 1){
            $validacion->v2_hr10= $request->v2_hr10;
        } else
            $validacion->v2_hr10= 0;


        if ($request->v3_hr10== 1){
            $validacion->v3_hr10 = $request->v3_hr10;
        } else
            $validacion->v3_hr10 = 0;


        if ($request->v4_hr10== 1){
            $validacion->v4_hr10= $request->v4_hr10;
        } else
            $validacion->v4_hr10= 0;

        if ($request->v6_hr10== 1){
            $validacion->v6_hr10= $request->v6_hr10;
        } else
            $validacion->v6_hr10 = 0;

        if ($request->v7_hr10== 1){
            $validacion->v7_hr10 = $request->v7_hr10;
        } else
            $validacion->v7_hr10 = 0;


        if ($request->v8_hr10== 1){
            $validacion->v8_hr10 = $request->v8_hr10;
        } else
            $validacion->v8_hr10 = 0;


        if ($request->v9_hr10== 1){
            $validacion->v9_hr10 = $request->v9_hr10;
        } else
            $validacion->v9_hr10 = 0;


        if ($request->v10_hr10== 1){
            $validacion->v10_hr10 = $request->v10_hr10;
        } else
            $validacion->v10_hr10 = 0;


        if ($request->v11_hr10== 1){
            $validacion->v11_hr10 = $request->v11_hr10;
        } else
            $validacion->v11_hr10 = 0;


        if ($request->v12_hr10== 1){
            $validacion->v12_hr10 = $request->v12_hr10;
        } else
            $validacion->v12_hr10 = 0;


        if ($request->v13_hr10== 1){
            $validacion->v13_hr10 = $request->v13_hr10;
        } else
            $validacion->v13_hr10 = 0;


        if ($request->v16_hr10== 1){
            $validacion->v16_hr10 = $request->v16_hr10;
        } else
            $validacion->v16_hr10 = 0;


        if ($request->v17_hr10== 1){
            $validacion->v17_hr10 = $request->v17_hr10;
        } else
            $validacion->v17_hr10 = 0;


        if ($request->v18_hr10== 1){
            $validacion->v18_hr10 = $request->v18_hr10;
        } else
            $validacion->v18_hr10 = 0;


        if ($request->v19_hr10== 1){
            $validacion->v19_hr10 = $request->v19_hr10;
        } else
            $validacion->v19_hr10 = 0;

        if ($request->v20_hr10 == 1){
            $validacion->v20_hr10 = $request->v20_hr10;
        } else
            $validacion->v20_hr10= 0;

        if ($request->v21_hr10== 1){
            $validacion->v21_hr10= $request->v21_hr10;
        } else
            $validacion->v21_hr10= 0;

            
        $validacion -> save();


        /** VALIDACIONES QUE GUARDAN MAS DATOS**/


        /** VALIDACIONES SI INGRESO UN VALOR MAXIMO **/

        if ($request->v6_id_emp== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_id_emp_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'ID_EMPLEADO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_nombre== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_nombre_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'NOMBRE';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_email== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_email_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'EMAIL';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_genero== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_genero_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'GENERO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_fec_nac== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_fec_nac_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'FECHA_NACIMIENTO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_fec_ing== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_fec_ing_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'FECHA_INGRESO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_antiguedad== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_antiguedad_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'ANTIGUEDAD_N';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_edad== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_edad_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'EDAD_N';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_edad_cat== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_edad_cat_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'EDAD_CATEGORIZADA';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_generacion== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_generacion_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'GENERACION';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_ant_cat== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_ant_cat_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'ANTIGUEDAD_CATEGORIZADA';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_ant_menor== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_ant_menor_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'ANTIGUEDAD_MENOR_A_1_ANIO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_t_cargo== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_t_cargo_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'TIPO_DE_CARGO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_cargo== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_cargo_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'CARGO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_cargo_2== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_cargo_2_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'CARGO_2';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_niv_educ== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_niv_educ_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'NIVEL_EDUCACIONAL';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_region== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_region_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'REGION';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_ciudad== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_ciudad_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'CIUDAD';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_es_jefe== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_es_jefe_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'ES_JEFE';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_id_jefe== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_id_jefe_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'ID_JEFE';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_cat_jefe== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_cat_jefe_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'CAT_JEFE';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_n1== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_n1_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'N1';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_n2== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_n2_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'N2';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_n3== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_n3_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'N3';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_n4== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_n4_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'N4';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_n5== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_n5_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'N5';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_seg_add_1== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_seg_add_1_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'SEGMENTO_ADICIONAL_1';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_seg_add_2== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_seg_add_2_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'SEGMENTO_ADICIONAL_2';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_seg_add_3== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_seg_add_3_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'SEGMENTO_ADICIONAL_3';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_aus_anio== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_aus_anio_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'AUSENTISMO_ANIO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_desempenio== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_desempenio_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'DESEMPENIO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_cump_metas== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_cump_metas_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'CUMPLIMIENTO_METAS';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_hr_extra== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_hr_extra_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'HORAS_EXTRAS_ANIO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_hr_capaci== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_hr_capaci_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'HORAS_CAPACITACION';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_t_contrato== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_t_contrato_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'TIPO_CONTRATO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_t_remun== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_t_remun_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'TIPO_REMUNERACION';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_sindicalizado== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_sindicalizado_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'SINDICALIZADO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_hr1== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_hr1_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR1';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_hr2== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_hr2_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR2';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_hr3== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_hr3_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR3';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_hr4== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_hr4_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR4';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_hr5== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_hr5_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR5';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_hr6== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_hr6_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR6';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_hr7== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_hr7_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR7';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_hr8== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_hr8_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR8';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_hr9== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_hr9_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR9';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v6_hr10== 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v6_hr10_valor_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR10';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 1;
            $validacion_maximo_minimo->save();

            
        } 


        


         /** VALIDACIONES SI INGRESO UN VALOR MINIMO **/

        if ($request->v7_id_emp == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_id_emp_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'ID_EMPLEADO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_nombre == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_nombre_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'NOMBRE';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_email == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_email_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'EMAIL';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_genero == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_genero_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'GENERO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_fec_nac == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_fec_nac_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'FECHA_NACIMIENTO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_fec_ing == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_fec_ing_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'FECHA_INGRESO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_antiguedad == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_antiguedad_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'ANTIGUEDAD_N';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_edad == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_edad_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'EDAD_N';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_edad_cat == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_edad_cat_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'EDAD_CATEGORIZADA';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_generacion == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_generacion_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'GENERACION';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_ant_cat == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_ant_cat_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'ANTIGUEDAD_CATEGORIZADA';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_ant_menor == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_ant_menor_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'ANTIGUEDAD_MENOR_A_1_ANIO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_t_cargo == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_t_cargo_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'TIPO_DE_CARGO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_cargo == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_cargo_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'CARGO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_cargo_2 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_cargo_2_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'CARGO_2';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_niv_educ == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_niv_educ_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'NIVEL_EDUCACIONAL';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_region == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_region_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'REGION';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_ciudad == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_ciudad_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'CIUDAD';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_es_jefe == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_es_jefe_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'ES_JEFE';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_id_jefe == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_id_jefe_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'ID_JEFE';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_cat_jefe == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_cat_jefe_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'CAT_JEFE';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_n1 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_n1_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'N1';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_n2 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_n2_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'N2';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_n3 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_n3_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'N3';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_n4 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_n4_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'N4';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_n5 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_n5_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'N5';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_seg_add_1 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_seg_add_1_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'SEGMENTO_ADICIONAL_1';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_seg_add_2 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_seg_add_2_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'SEGMENTO_ADICIONAL_2';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_seg_add_3 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_seg_add_3_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'SEGMENTO_ADICIONAL_3';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_aus_anio == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_aus_anio_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'AUSENTISMO_ANIO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_desempenio == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_desempenio_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'DESEMPENIO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_cump_metas == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_cump_metas_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'CUMPLIMIENTO_METAS';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_hr_extra == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_hr_extra_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'HORAS_EXTRAS_ANIO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_hr_capaci == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_hr_capaci_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'HORAS_CAPACITACION';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_t_contrato == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_t_contrato_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'TIPO_CONTRATO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_t_remun == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_t_remun_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'TIPO_REMUNERACION';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_sindicalizado == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_sindicalizado_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'SINDICALIZADO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_hr1 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_hr1_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR1';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_hr2 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_hr2_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR2';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_hr3 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_hr3_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR3';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_hr4 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_hr4_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR4';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_hr5 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_hr5_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR5';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_hr6 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_hr6_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR6';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_hr7 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_hr7_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR7';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_hr8 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_hr8_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR8';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_hr9 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_hr9_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR9';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v7_hr10 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->numero = $request->v7_hr10_valor_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR10';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 2;
            $validacion_maximo_minimo->save();

            
        } 





        /** VALIDACIONES SI INGRESO UN VALOR VALIDO **/

        if ($request->v8_id_emp== 1){

            $count_valores_validos = explode(',', $request->v8_id_emp_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'ID_EMPLEADO';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_nombre== 1){

            $count_valores_validos = explode(',', $request->v8_nombre_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'NOMBRE';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_email== 1){

            $count_valores_validos = explode(',', $request->v8_email_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'EMAIL';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_genero== 1){

            $count_valores_validos = explode(',', $request->v8_genero_valores_validos);
            
            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'GENERO';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_fec_nac== 1){

            $count_valores_validos = explode(',', $request->v8_fec_nac_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'FECHA_NACIMIENTO';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_fec_ing== 1){

            $count_valores_validos = explode(',', $request->v8_fec_ing_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'FECHA_INGRESO';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_antiguedad== 1){

            $count_valores_validos = explode(',', $request->v8_antiguedad_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'ANTIGUEDAD_N';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_edad== 1){

            $count_valores_validos = explode(',', $request->v8_edad_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'EDAD_N';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_edad_cat== 1){

            $count_valores_validos = explode(',', $request->v8_edad_cat_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'EDAD_CATEGORIZADA';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_generacion== 1){

            $count_valores_validos = explode(',', $request->v8_generacion_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'GENERACION';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_ant_cat== 1){

            $count_valores_validos = explode(',', $request->v8_ant_cat_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'ANTIGUEDAD_CATEGORIZADA';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_ant_menor== 1){

            $count_valores_validos = explode(',', $request->v8_ant_menor_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'ANTIGUEDAD_MENOR_A_1_ANIO';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_t_cargo== 1){

            $count_valores_validos = explode(',', $request->v8_t_cargo_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'TIPO_DE_CARGO';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_cargo== 1){

            $count_valores_validos = explode(',', $request->v8_cargo_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'CARGO';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_cargo2== 1){

            $count_valores_validos = explode(',', $request->v8_cargo2_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'cargo2';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_niv_educ== 1){

            $count_valores_validos = explode(',', $request->v8_niv_educ_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'NIVEL_EDUCACIONAL';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_region== 1){

            $count_valores_validos = explode(',', $request->v8_region_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'REGION';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_ciudad== 1){

            $count_valores_validos = explode(',', $request->v8_ciudad_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'CIUDAD';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_es_jefe== 1){

            $count_valores_validos = explode(',', $request->v8_es_jefe_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'ES_JEFE';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_id_jefe== 1){

            $count_valores_validos = explode(',', $request->v8_id_jefe_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'ID_JEFE';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_cat_jefe== 1){

            $count_valores_validos = explode(',', $request->v8_cat_jefe_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'CAT_JEFE';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_n1== 1){

            $count_valores_validos = explode(',', $request->v8_n1_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'N1';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_n2== 1){

            $count_valores_validos = explode(',', $request->v8_n2_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'N2';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_n3== 1){

            $count_valores_validos = explode(',', $request->v8_n3_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'N3';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_n4== 1){

            $count_valores_validos = explode(',', $request->v8_n4_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'N4';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_n5== 1){

            $count_valores_validos = explode(',', $request->v8_n5_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'N5';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_seg_add_1== 1){

            $count_valores_validos = explode(',', $request->v8_seg_add_1_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'SEGMENTO_ADICIONAL_1';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_seg_add_2== 1){

            $count_valores_validos = explode(',', $request->v8_seg_add_2_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'SEGMENTO_ADICIONAL_2';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_seg_add_3== 1){

            $count_valores_validos = explode(',', $request->v8_seg_add_3_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'SEGMENTO_ADICIONAL_3';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_aus_anio== 1){

            $count_valores_validos = explode(',', $request->v8_aus_anio_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'AUSENTISMO_ANIO';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_desempenio== 1){

            $count_valores_validos = explode(',', $request->v8_desempenio_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'DESEMPENIO';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_cump_metas== 1){

            $count_valores_validos = explode(',', $request->v8_cump_metas_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'CUMPLIMIENTO_METAS';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_hr_extra== 1){

            $count_valores_validos = explode(',', $request->v8_hr_extra_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'HORAS_EXTRAS_ANIO';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_hr_capaci== 1){

            $count_valores_validos = explode(',', $request->v8_hr_capaci_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'HORAS_CAPACITACION';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_t_contrato== 1){

            $count_valores_validos = explode(',', $request->v8_t_contrato_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'TIPO_CONTRATO';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_t_remun== 1){

            $count_valores_validos = explode(',', $request->v8_t_remun_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'TIPO_REMUNERACION';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_sindicalizado== 1){

            $count_valores_validos = explode(',', $request->v8_sindicalizado_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'SINDICALIZADO';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_hr1== 1){

            $count_valores_validos = explode(',', $request->v8_hr1_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'VAR_HR1';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_hr2== 1){

            $count_valores_validos = explode(',', $request->v8_hr2_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'VAR_HR2';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_hr3== 1){

            $count_valores_validos = explode(',', $request->v8_hr3_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'VAR_HR3';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_hr4== 1){

            $count_valores_validos = explode(',', $request->v8_hr4_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'VAR_HR4';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_hr5== 1){

            $count_valores_validos = explode(',', $request->v8_hr5_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'VAR_HR5';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_hr6== 1){

            $count_valores_validos = explode(',', $request->v8_hr6_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'VAR_HR6';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_hr7== 1){

            $count_valores_validos = explode(',', $request->v8_hr7_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'VAR_HR7';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_hr8== 1){

            $count_valores_validos = explode(',', $request->v8_hr8_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'VAR_HR8';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_hr9== 1){

            $count_valores_validos = explode(',', $request->v8_hr9_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'VAR_HR9';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 
        if ($request->v8_hr10== 1){

            $count_valores_validos = explode(',', $request->v8_hr10_valores_validos);

            foreach ($count_valores_validos as $key => $value) 
            {

                $validacion_maximo_minimo = new validacion_menor_mayor();
                $validacion_maximo_minimo->valor_valido = $value;
                $validacion_maximo_minimo->campo_a_validar = 'VAR_HR10';
                $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
                $validacion_maximo_minimo->id_tipo_validacion = 5;
                $validacion_maximo_minimo->save();
            }
            
        }
 




         /** VALIDACIONES SI INGRESO UNA FECHA MAXIMA **/

        if ($request->v9_id_emp == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_id_emp_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'ID_EMPLEADO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_nombre == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_nombre_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'NOMBRE';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_email == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_email_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'EMAIL';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_genero == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_genero_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'GENERO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_fec_nac == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_fec_nac_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'FECHA_NACIMIENTO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_fec_ing == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_fec_ing_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'FECHA_INGRESO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_antiguedad == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_antiguedad_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'ANTIGUEDAD_N';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_edad == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_edad_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'EDAD_N';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_edad_cat == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_edad_cat_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'EDAD_CATEGORIZADA';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_generacion == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_generacion_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'GENERACION';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_ant_cat == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_ant_cat_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'ANTIGUEDAD_CATEGORIZADA';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_ant_menor == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_ant_menor_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'ANTIGUEDAD_MENOR_A_1_ANIO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_t_cargo == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_t_cargo_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'TIPO_DE_CARGO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_cargo == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_cargo_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'CARGO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_cargo_2 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_cargo_2_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'CARGO_2';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_niv_educ == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_niv_educ_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'NIVEL_EDUCACIONAL';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_region == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_region_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'REGION';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_ciudad == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_ciudad_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'CIUDAD';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_es_jefe == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_es_jefe_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'ES_JEFE';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_id_jefe == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_id_jefe_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'ID_JEFE';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_cat_jefe == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_cat_jefe_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'CAT_JEFE';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_n1 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_n1_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'N1';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_n2 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_n2_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'N2';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_n3 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_n3_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'N3';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_n4 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_n4_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'N4';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_n5 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_n5_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'N5';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_seg_add_1 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_seg_add_1_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'SEGMENTO_ADICIONAL_1';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_seg_add_2 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_seg_add_2_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'SEGMENTO_ADICIONAL_2';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_seg_add_3 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_seg_add_3_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'SEGMENTO_ADICIONAL_3';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_aus_anio == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_aus_anio_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'AUSENTISMO_ANIO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_desempenio == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_desempenio_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'DESEMPENIO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_cump_metas == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_cump_metas_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'CUMPLIMIENTO_METAS';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_hr_extra == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_hr_extra_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'HORAS_EXTRAS_ANIO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_hr_capaci == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_hr_capaci_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'HORAS_CAPACITACION';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_t_contrato == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_t_contrato_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'TIPO_CONTRATO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_t_remun == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_t_remun_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'TIPO_REMUNERACION';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_sindicalizado == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_sindicalizado_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'SINDICALIZADO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_hr1 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_hr1_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR1';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_hr2 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_hr2_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR2';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_hr3 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_hr3_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR3';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_hr4 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_hr4_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR4';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_hr5 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_hr5_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR5';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_hr6 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_hr6_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR6';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_hr7 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_hr7_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR7';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_hr8 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_hr8_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR8';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_hr9 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_hr9_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR9';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v9_hr10 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v9_hr10_valor_fecha_maximo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR10';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 3;
            $validacion_maximo_minimo->save();

            
        } 





        /** VALIDACIONES SI INGRESO UNA FECHA MINIMA **/

        if ($request->v10_id_emp == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_id_emp_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'ID_EMPLEADO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_nombre == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_nombre_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'NOMBRE';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_email == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_email_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'EMAIL';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_genero == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_genero_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'GENERO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_fec_nac == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_fec_nac_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'FECHA_NACIMIENTO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_fec_ing == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_fec_ing_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'FECHA_INGRESO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_antiguedad == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_antiguedad_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'ANTIGUEDAD_N';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_edad == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_edad_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'EDAD_N';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_edad_cat == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_edad_cat_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'EDAD_CATEGORIZADA';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_generacion == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_generacion_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'GENERACION';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_ant_cat == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_ant_cat_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'ANTIGUEDAD_CATEGORIZADA';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_ant_menor == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_ant_menor_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'ANTIGUEDAD_MENOR_A_1_ANIO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_t_cargo == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_t_cargo_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'TIPO_DE_CARGO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_cargo == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_cargo_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'CARGO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_cargo_2 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_cargo_2_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'CARGO_2';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_niv_educ == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_niv_educ_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'NIVEL_EDUCACIONAL';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_region == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_region_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'REGION';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_ciudad == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_ciudad_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'CIUDAD';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_es_jefe == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_es_jefe_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'ES_JEFE';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_id_jefe == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_id_jefe_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'ID_JEFE';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_cat_jefe == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_cat_jefe_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'CAT_JEFE';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_n1 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_n1_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'N1';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_n2 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_n2_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'N2';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_n3 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_n3_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'N3';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_n4 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_n4_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'N4';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_n5 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_n5_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'N5';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_seg_add_1 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_seg_add_1_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'SEGMENTO_ADICIONAL_1';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_seg_add_2 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_seg_add_2_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'SEGMENTO_ADICIONAL_2';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_seg_add_3 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_seg_add_3_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'SEGMENTO_ADICIONAL_3';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_aus_anio == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_aus_anio_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'AUSENTISMO_ANIO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_desempenio == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_desempenio_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'DESEMPENIO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_cump_metas == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_cump_metas_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'CUMPLIMIENTO_METAS';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_hr_extra == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_hr_extra_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'HORAS_EXTRAS_ANIO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_hr_capaci == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_hr_capaci_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'HORAS_CAPACITACION';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_t_contrato == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_t_contrato_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'TIPO_CONTRATO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_t_remun == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_t_remun_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'TIPO_REMUNERACION';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_sindicalizado == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_sindicalizado_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'SINDICALIZADO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_hr1 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_hr1_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR1';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_hr2 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_hr2_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR2';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_hr3 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_hr3_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR3';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_hr4 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_hr4_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR4';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_hr5 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_hr5_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR5';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_hr6 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_hr6_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR6';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_hr7 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_hr7_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR7';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_hr8 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_hr8_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR8';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_hr9 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_hr9_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR9';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 
        if ($request->v10_hr10 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->fecha = $request->v10_hr10_valor_fecha_minimo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR10';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 4;
            $validacion_maximo_minimo->save();

            
        } 




         /** VALIDACIONES PADRE E HIJO **/

                if ($request->v16_id_emp == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_id_emp_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'ID_EMPLEADO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_nombre == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_nombre_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'NOMBRE';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_email == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_email_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'EMAIL';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_genero == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_genero_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'GENERO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_fec_nac == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_fec_nac_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'FECHA_NACIMIENTO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_fec_ing == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_fec_ing_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'FECHA_INGRESO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_antiguedad == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_antiguedad_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'ANTIGUEDAD_N';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_edad == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_edad_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'EDAD_N';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_edad_cat == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_edad_cat_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'EDAD_CATEGORIZADA';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_generacion == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_generacion_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'GENERACION';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 dd($request->v16_ant_cat_padre_hijo);
        if ($request->v16_ant_cat == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_ant_cat_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'ANTIGUEDAD_CAT';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_ant_menor == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_ant_menor_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'ANTIG_MENOR_ANIO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_t_cargo == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_t_cargo_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'TIPO_DE_CARGO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_cargo == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_cargo_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'CARGO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_cargo2 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_cargo2_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'cargo2';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_niv_educ == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_niv_educ_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'NIVEL_EDUCACIONAL';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_region == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_region_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'REGION';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_ciudad == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_ciudad_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'CIUDAD';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_es_jefe == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_es_jefe_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'ES_JEFE';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_id_jefe == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_id_jefe_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'ID_JEFE';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_cat_jefe == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_cat_jefe_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'CAT_JEFE';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_n1 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_n1_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'N1';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_n2 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_n2_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'N2';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_n3 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_n3_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'N3';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_n4 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_n4_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'N4';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_n5 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_n5_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'N5';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_seg_add_1 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_seg_add_1_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'SEGMENTO_ADICIONAL_1';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_seg_add_2 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_seg_add_2_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'SEGMENTO_ADICIONAL_2';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_seg_add_3 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_seg_add_3_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'SEGMENTO_ADICIONAL_3';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_aus_anio == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_aus_anio_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'AUSENTISMO_ANIO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_desempenio == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_desempenio_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'DESEMPENIO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_cump_metas == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_cump_metas_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'CUMPLIMIENTO_METAS';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_hr_extra == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_hr_extra_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'HORAS_EXTRAS_ANIO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_hr_capaci == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_hr_capaci_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'HORAS_CAPACITACION';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_t_contrato == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_t_contrato_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'TIPO_CONTRATO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_t_remun == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_t_remun_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'TIPO_REMUNERACION';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_sindicalizado == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_sindicalizado_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'SINDICALIZADO';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_hr1 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_hr1_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR1';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_hr2 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_hr2_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR2';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_hr3 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_hr3_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR3';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_hr4 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_hr4_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR4';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_hr5 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_hr5_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR5';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_hr6 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_hr6_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR6';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_hr7 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_hr7_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR7';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_hr8 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_hr8_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR8';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_hr9 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_hr9_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR9';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 
        if ($request->v16_hr10 == 1){

            $validacion_maximo_minimo = new validacion_menor_mayor();
            $validacion_maximo_minimo->campo_hijo = $request->v16_hr10_padre_hijo;
            $validacion_maximo_minimo->campo_a_validar = 'VAR_HR10';
            $validacion_maximo_minimo->id_validacion_estudio = $validacion->id;
            $validacion_maximo_minimo->id_tipo_validacion = 6;
            $validacion_maximo_minimo->save();
            
        }
 

 
        
        return view('admin');

    }

    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }


}

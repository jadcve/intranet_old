<?php

namespace kpi\Http\Controllers;

use kpi\Http\Requests;
use kpi\Http\Requests\TipoareasCreateRequest;
use kpi\Http\Requests\TipoareasUpdateRequest;
use kpi\Http\Controllers\Controller;
use Session;
use Redirect;
use kpi\tipo_area;
use Illuminate\Http\Request;

class tipo_areaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipo_areas = tipo_area::paginate(10);
        return view('tipo_areas.index', compact('tipo_areas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TipoareasCreateRequest $request)
    {
        tipo_area::create([
            'tipo_area' => $request['tipo_area'],
            'status' => $request['status'],            
        ]);
        
        Session::flash('message', 'Tipo de &Aacute;rea Creado Correctamente');
        return Redirect::to('tipo_areas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipo_area = tipo_area::find($id);
        return view('tipo_areas.edit',['tipo_area'=>$tipo_area]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TipoareasUpdateRequest $request, $id)
    {
        $tipo_area = tipo_area::find($id)->update($request->all());
        Session::flash('message', 'Tipo de &Aacute;rea Editada Correctamente');
        return Redirect::to('tipo_areas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tipo_area = tipo_area::find($id);
        $tipo_area->delete();

        Session::flash('message', 'Tipo de &Aacute;rea Eliminada Correctamente');
        return Redirect::to('rubros');
    }
}

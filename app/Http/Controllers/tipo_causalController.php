<?php

namespace kpi\Http\Controllers;

use kpi\Http\Requests;
use kpi\Http\Requests\TipocausalesCreateRequest;
use kpi\Http\Requests\TipocausalesUpdateRequest;
use kpi\Http\Controllers\Controller;
use Session;
use Redirect;
use kpi\tipo_causal;
use Illuminate\Http\Request;

class tipo_causalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipo_causales = tipo_causal::paginate(10);
        return view('tipo_causales.index', compact('tipo_causales'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TipocausalesCreateRequest $request)
    {
        tipo_causal::create([
            'tipo_causal' => $request['tipo_causal'],
            'status' => $request['status'],            
        ]);
        
        Session::flash('message', 'Tipo de Causal Creado Correctamente');
        return Redirect::to('tipo_causales');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipo_causal = tipo_causal::find($id);
        return view('tipo_causales.edit',['tipo_causal'=>$tipo_causal]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TipocausalesUpdateRequest $request, $id)
    {
        $tipo_causal = tipo_causal::find($id)->update($request->all());
        Session::flash('message', 'Tipo de Causal Editada Correctamente');
        return Redirect::to('tipo_causales');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tipo_causal = tipo_causal::find($id);
        $tipo_causal->delete();

        Session::flash('message', 'Tipo de Causal Eliminada Correctamente');
        return Redirect::to('tipo_causales');
    }
}

<?php

namespace kpi\Http\Controllers;

use kpi\Http\Requests;
use kpi\Http\Requests\RubroCreateRequest;
use kpi\Http\Requests\RubroUpdateRequest;
use kpi\Http\Controllers\Controller;
use Session;
use Redirect;
use kpi\rubro;
use kpi\subrubro;
use Illuminate\Http\Request;

class rubroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rubros = rubro::paginate(10);
        return view('rubros.index', compact('rubros'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RubroCreateRequest $request)
    {
        rubro::create([
            'rubro' => $request['rubro'],
            'status' => $request['status'],            
        ]);
        
        Session::flash('message', 'Rubro Creado Correctamente');
        return Redirect::to('rubros');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rubro = rubro::find($id);
        return view('rubros.edit',['rubro'=>$rubro]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, RubroUpdateRequest $request)
    {
        $rubro = rubro::find($id)->update($request->all());
        Session::flash('message', 'Rubro Editado Correctamente');
        return Redirect::to('/rubros');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rubro = rubro::find($id);
        $rubro->delete();

        Session::flash('message', 'Rubro Eliminado Correctamente');
        return Redirect::to('rubros');
    }
}

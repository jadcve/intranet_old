<?php

namespace kpi\Http\Controllers;

use kpi\Http\Requests;
use kpi\Http\Requests\EncuestaCreateRequest;
use kpi\Http\Requests\EncuestaUpdateRequest;
use kpi\Http\Controllers\Controller;
use Session;
use Redirect;
use kpi\encuesta;
use kpi\empresa;
use kpi\clientes;
use Illuminate\Http\Request;
use DB;
use Mail;


class encuestaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$clientes = clientes::select(DB::raw("CONCAT(nombre,' ',apellido) AS nombre"),'id')->where('status', 1)->pluck('nombre', 'id');
        $encuestas = encuesta::encuestas();
        return view('encuestas.index')->with(compact('encuestas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clientes = clientes::select(DB::raw("CONCAT(nombre,' ',apellido) AS nombre"),'id')->where('status', 1)->pluck('nombre', 'id');
        return view('encuestas.crear')->with(compact('clientes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EncuestaCreateRequest $request)
    { 
        $usuarios = $_POST["usuario"];
        $enviar_mail = $request->enviar_mail;    

        if ($enviar_mail == 'SI') 
        {
            foreach ($usuarios as $usuario)
            {
                $query = db::insert("INSERT INTO encuestas (encuesta, descripcion, document, envio_mail, status, created_at, usuario) VALUES ('$request->encuesta','$request->descripcion','$request->document','$request->enviar_mail','$request->status',NOW(),'{$usuario}')");

                $id_cliente = $usuario;
                $results = DB::select("SELECT email FROM clientes WHERE id = '$id_cliente'");
                $results_mail = $results[0];
                $mail_cliente=$results_mail->email; 

                Mail::send('emails.nuevaEncuesta',$request->all(), function($msj) use ($mail_cliente){
                $msj->subject('Nuevo encuesta disponible - KPI Estudios');
                $msj->to($mail_cliente);
                });
            }
            Session::flash('message', 'Encuesta Creada y Correos enviados Correctamente');
            return Redirect::to('encuestas');                
        }else{
            foreach ($usuarios as $usuario)
            {
                $query = db::insert("INSERT INTO encuestas (encuesta, descripcion, document, envio_mail, status, created_at, usuario) VALUES ('$request->encuesta','$request->descripcion','$request->document','NO','$request->status',NOW(),'{$usuario}')");                    
            }
            Session::flash('message', 'Encuesta Creada Correctamente');
            return Redirect::to('encuestas');
        }
    }    
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $encuesta = encuesta::find($id);
        $clientes = clientes::select(DB::raw("CONCAT(nombre,' ',apellido) AS nombre"),'id')->where('status', 1)->pluck('nombre', 'id');
        return view('encuestas.edit',['encuesta'=>$encuesta])->with(compact('clientes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $encuesta = encuesta::find($id)->update($request->all());
        Session::flash('message', 'Encuesta Editada Correctamente');
        return Redirect::to('encuestas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $encuesta = encuesta::find($id);
        $encuesta->delete();

        Session::flash('message', 'Encuesta Eliminada Correctamente');
        return Redirect::to('encuestas');
    }
}
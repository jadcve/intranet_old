<?php

namespace kpi\Http\Controllers;
use kpi\carousel;
USE kpi\imagenes_carousel;
use kpi\Http\Requests;
use kpi\Http\Controllers\Controller;
use Session;
use Redirect;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use File;



class uploadimagenesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('imagenesCarousel.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $carousel = carousel::find($id);
        return view('imagenesCarousel.edit',['id'=>$id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
    $destinationPath = public_path('img/carousel');
    $files = $request->file('imagen');


    foreach ($files as $file) {//this statement will loop through all files.
            $file_name = Carbon::now()->second.$file->getClientOriginalName(); //Get file original name
            $file->move($destinationPath , $file_name); // move files to destination folder
            
            $query = db::insert("INSERT INTO imagenes_carousels (carousel, imagen, status, created_at) VALUES ('$request->carousel','$file_name','1',NOW())");
        }
            Session::flash('message', 'Imagenes Cargadas Correctamente');
            return Redirect::to('carousels'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $carousel = carousel::find($id);
        return view('imagenesCarousel.edit',['carousel'=>$carousel]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    $img_delete = DB::table('imagenes_carousels')
        ->join('carousels','carousels.id','=','imagenes_carousels.carousel')
        ->select('imagenes_carousels.imagen')
        ->where('imagenes_carousels.carousel','=',$id)
        ->get();
        $destinationPath = public_path('img/carousel/');
         $files = array_collapse([$img_delete]);
        
        $i=0;
        foreach ($files as $file) {
            
            file::delete($destinationPath.$files[$i]->imagen);
            
        $i++;
        }
     
        $imagenes = imagenes_carousel::where('carousel', $id);
        $imagenes->delete();
        
        Session::flash('message', 'Imagenes Eliminadas Correctamente');
        return Redirect::to('carousels');  
    }
}

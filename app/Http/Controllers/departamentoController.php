<?php

namespace kpi\Http\Controllers;

use kpi\Http\Requests;
use kpi\Http\Requests\DepartamentoCreateRequest;
use kpi\Http\Requests\DepartamentoUpdateRequest;
use kpi\Http\Controllers\Controller;
use Session;
use Redirect;
use kpi\departamento;
use Illuminate\Http\Request;

class departamentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departamentos = departamento::paginate(10);
        return view('departamentos.index', compact('departamentos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DepartamentoCreateRequest $request)
    {
        departamento::create([
            'departamento' => $request['departamento'],
            'status' => $request['status'],            
        ]);
        
        Session::flash('message', 'Departamento Creado Correctamente');
        return Redirect::to('/departamentos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $departamento = departamento::find($id);
        return view('departamentos.edit',['departamento'=>$departamento]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, DepartamentoUpdateRequest $request)
    {
        $departamento = departamento::find($id)->update($request->all());
        Session::flash('message', 'Departamento Editado Correctamente');
        return Redirect::to('departamentos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $departamentos = departamento::find($id);
        $departamentos->delete();

        Session::flash('message', 'Departamento Eliminado Correctamente');
        return Redirect::to('departamentos');
    }
}

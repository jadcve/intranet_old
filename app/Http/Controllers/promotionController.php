<?php

namespace kpi\Http\Controllers;

use kpi\Http\Requests;
use kpi\Http\Requests\PromotionCreateRequest;
use kpi\Http\Requests\PromotionUpdateRequest;
use kpi\Http\Controllers\Controller;
use Session;
use Redirect;
use kpi\promotion;
use kpi\clientes;
use kpi\carousel;
use Illuminate\Http\Request;
use DB;
use Mail;


class promotionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promotions = promotion::Promotions();
        $carousels = DB::table('carousels')->pluck('titulo','id');
        return view('promotions.index')->with(compact('promotions','carousels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clientes = clientes::select(DB::raw("CONCAT(nombre,' ',apellido) AS nombre"),'id')->where('status', 1)->pluck('nombre', 'id');
        $carousels = DB::table('carousels')->pluck('titulo','id');
        return view('promotions.crear')->with(compact('clientes','carousels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PromotionCreateRequest $request)
{ 
        $usuarios = $_POST["usuario"];
        $enviar_mail = $request->enviar_mail;
        


        if(!empty($request->imagen)){
            $file = $request->file('imagen');
            $nombre_archivo = $file->getClientOriginalName();
            \Storage::disk('carousel')->put($nombre_archivo,  \File::get($file));
        }else{
            $nombre_archivo = NULL;
        }


        if ($enviar_mail == 'SI')
        {
            foreach ($usuarios as $usuario)
            {

                $query = db::insert("INSERT INTO promotions (titulo, texto_corto, envio_mail, mensaje, usuario, tipo, carousel, imagen, video, texto_wizard, url_wizard, status, created_at) VALUES ('$request->titulo', '$request->texto_corto','$enviar_mail', '$request->mensaje', '{$usuario}', '$request->tipo', '$request->carousel', '$nombre_archivo', '$request->video', '$request->texto_wizard', '$request->url_wizard', '$request->status', NOW())");  

                $id_cliente = $usuario;
                $results = DB::select("SELECT email FROM clientes WHERE id = '$id_cliente'");
                $results_mail = $results[0];
                $mail_cliente=$results_mail->email; 

                Mail::send('emails.nuevaPromocion',$request->all(), function($msj) use ($mail_cliente){
                $msj->subject('Novedades - KPI Estudios');
                $msj->to($mail_cliente);
                });                    
            }
            Session::flash('message', 'Promoci&oacute;n Creada y Correos enviados Correctamente');
            return Redirect::to('promotions');
        }else{
            foreach ($usuarios as $usuario)
            {

                $query = db::insert("INSERT INTO promotions (titulo, texto_corto, envio_mail, mensaje, usuario, tipo, carousel, imagen, video, texto_wizard, url_wizard, status, created_at) VALUES ('$request->titulo', '$request->texto_corto','NO', '$request->mensaje', '{$usuario}', '$request->tipo', '$request->carousel', '$nombre_archivo', '$request->video', '$request->texto_wizard', '$request->url_wizard', '$request->status', NOW())");  
            }
            Session::flash('message', 'Promoci&oacute;n Creada Correctamente');
            return Redirect::to('promotions'); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $promotion = promotion::find($id);
        $carousels = DB::table('carousels')->pluck('titulo','id');
        $clientes = clientes::select(DB::raw("CONCAT(nombre,' ',apellido) AS nombre"),'id')->where('status', 1)->pluck('nombre', 'id');
        return view('promotions.edit',['promotion'=>$promotion])->with(compact('clientes','carousels'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PromotionUpdateRequest $request, $id)
    {
        $promotion = promotion::find($id)->update($request->all());
        Session::flash('message', 'Promocion Editada Correctamente');
        return Redirect::to('promotions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $promotion = promotion::find($id);
        $promotion->delete();

        Session::flash('message', 'Promoción Eliminado Correctamente');
        return Redirect::to('promotions');
    }
}

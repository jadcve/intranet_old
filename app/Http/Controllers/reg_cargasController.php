<?php

namespace kpi\Http\Controllers;

use kpi\Http\Requests;
use kpi\Http\Requests\reg_cargasCreateRequest;
use kpi\Http\Requests\reg_cargasUpdateRequest;
use kpi\Http\Controllers\Controller;
use Redirect;
use Session;
use kpi\clientes;
use kpi\empresa;
use kpi\reg_cargas;
use Illuminate\Http\Request;
use Mail;
use DB;
use Auth;

class reg_cargasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reg_cargas = reg_cargas::reg_datos_carga();
        return view('reg_cargas.index', compact('reg_cargas'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $clientes = clientes::select(DB::raw("CONCAT(nombre,' ',apellido) AS nombre"),'id')->where('status', 1)->pluck('nombre', 'id');
        $reg_carga = reg_cargas::find($id);
        return view('reg_cargas.edit',['reg_carga'=>$reg_carga])->with(compact('clientes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reg_carga = reg_cargas::find($id);
        $reg_carga->delete();

        $query_delete = db::delete("DELETE FROM $reg_carga->tipo_carga WHERE empresa = '$reg_carga->empresa' AND mes_carga = '$reg_carga->mes' AND anio_carga = '$reg_carga->anio'");
        

        Session::flash('message', 'Carga Eliminada Correctamente');
        return Redirect::to('reg_cargas');
    }
}

<?php

namespace kpi\Http\Controllers;

use kpi\Http\Requests;
use Illuminate\Support\Facades\Hash;
use kpi\Http\Requests\ClienteCreateRequest;
use kpi\Http\Requests\ClienteUpdateRequest;
use kpi\Http\Controllers\Controller;
use Redirect;
use Session;
use kpi\clientes;
use kpi\empresa;
use kpi\cargo;
use Illuminate\Http\Request;
use Mail;
use Auth;
use DB;

class clienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {

        $clientes = clientes::clientesEmpre();
        return view('clientes.index', compact('clientes'));
    }

    public function getcliente(Request $request, $id){

            $clientes = clientes::clientesReg($id);
            return Response()->json($clientes);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $empresas = empresa::pluck('empresa','id');
        $cargos = cargo::pluck('cargo','id');
        return view('clientes.crear')->with(compact('empresas','cargos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(ClienteCreateRequest $request)
    {
        
        $enviar_mail = $request->enviar_mail;
        $mail_cliente = $request->email;
        if ($enviar_mail == 'SI') {
            clientes::create($request->all());
            Mail::send('emails.registroCliente',$request->all(), function($msj) use ($mail_cliente){
            $msj->subject('Registro de Usuario en KPI Estudios');
            $msj->to($mail_cliente);
            });
        }
        else{
            clientes::create($request->all());
        }
        
        Session::flash('message', 'Cliente Creado Correctamente');
        return Redirect::to('clientes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cliente = clientes::find($id);
        $empresas = empresa::pluck('empresa','id');
        $cargos = cargo::pluck('cargo','id');
        return view('clientes.edit',['cliente'=>$cliente])->with(compact('empresas','cargos'));;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClienteUpdateRequest $request, $id)
    {
        $cliente = clientes::find($id)->update($request->all());
        Session::flash('message', 'Cliente Editado Correctamente');
        return Redirect::to('clientes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cliente = clientes::find($id);
        $cliente->delete();

        Session::flash('message', 'Cliente Eliminado Correctamente');
        return Redirect::to('clientes');
    }

    public function actualizarClientes(Request $request)
    {
        $empresa_id = $request->input('empresa');
        $clientes = clientes::clientesParaSelect($empresa_id);

        return $clientes;
    }
}


<?php

namespace kpi\Http\Controllers;

use kpi\Http\Requests;
use kpi\Http\Requests\PaisCreateRequest;
use kpi\Http\Requests\PaisUpdateRequest;
use kpi\Http\Controllers\Controller;
use Session;
use Redirect;
use kpi\pais;
use Illuminate\Http\Request;

class paisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paises = pais::all();
        return view('paises.index', compact('paises'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PaisCreateRequest $request)
    {
        pais::create([
            'pais' => $request['pais'],
            'ab' => $request['ab'],
            'status' => $request['status'],            
        ]);
        
        Session::flash('message', 'Pais Creado Correctamente');
        return Redirect::to('/paises');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pais = pais::find($id);
        return view('paises.edit',['pais'=>$pais]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, PaisUpdateRequest $request)
    {
        $pais = pais::find($id)->update($request->all());
        Session::flash('message', 'Pa&iacute;s Editado Correctamente');
        return Redirect::to('/paises');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pais = pais::find($id);
        $pais->delete();

        Session::flash('message', 'Pa&iacute;s Eliminado Correctamente');
        return Redirect::to('/paises');
    }
}

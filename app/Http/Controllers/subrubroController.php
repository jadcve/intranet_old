<?php

namespace kpi\Http\Controllers;

use kpi\Http\Requests;
use kpi\Http\Requests\SubrubroCreateRequest;
use kpi\Http\Requests\SubrubroUpdateRequest;
use kpi\Http\Controllers\Controller;
use Session;
use Redirect;
use kpi\subrubro;
use kpi\rubro;
use Illuminate\Http\Request;

class subrubroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subrubros = subrubro::subrubros();
        $rubros = rubro::pluck('rubro','id');
        //$subrubros = subrubro::paginate(10);
        return view('subrubros.index', compact('subrubros','rubros'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubrubroCreateRequest $request)
    {
        subrubro::create([
            'rubro' => $request['rubro'],
            'subrubro' => $request['subrubro'],
            'status' => $request['status'],            
        ]);
        
        Session::flash('message', 'Sub-rubro Creado Correctamente');
        return Redirect::to('subrubros');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rubros = rubro::pluck('rubro','id');
        $subrubros = subrubro::find($id);
        return view('subrubros.edit',['subrubro'=>$subrubros])->with(compact('rubros'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, subrubroUpdateRequest $request)
    {
        $subrubro = subrubro::find($id)->update($request->all());
        Session::flash('message', 'Sub-rubro Editado Correctamente');
        return Redirect::to('subrubros');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subrubro = subrubro::find($id);
        $subrubro->delete();

        Session::flash('message', 'Sub-rubro Eliminado Correctamente');
        return Redirect::to('subrubros');
    }
}

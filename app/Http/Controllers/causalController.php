<?php

namespace kpi\Http\Controllers;

use kpi\Http\Requests;
use kpi\Http\Requests\CausalCreateRequest;
use kpi\Http\Requests\CausalUpdateRequest;
use kpi\Http\Controllers\Controller;
use Session;
use Redirect;
use kpi\causal;
use kpi\tipo_causal;
use Illuminate\Http\Request;

class causalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $causales = causal::causales();
        $tipo_causales = tipo_causal::pluck('tipo','id');
        return view('causales.index', compact('causales','tipo_causales'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CausalCreateRequest $request)
    {
        causal::create([
            'tipo_causal' => $request['tipo_causal'],
            'causal' => $request['causal'],
            'status' => $request['status'],            
        ]);
        
        Session::flash('message', 'Causal Creada Correctamente');
        return Redirect::to('causales');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $causal = causal::find($id);
        $tipo_causales = tipo_causal::pluck('tipo','id');
        return view('causales.edit',['causal'=>$causal])->with(compact('tipo_causales'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $causal = causal::find($id)->update($request->all());
        Session::flash('message', 'Causal Editada Correctamente');
        return Redirect::to('causales');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $causal = causal::find($id);
        $causal->delete();

        Session::flash('message', 'Causal Eliminada Correctamente');
        return Redirect::to('causales');
    }
}

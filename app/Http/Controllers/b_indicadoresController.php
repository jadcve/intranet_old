<?php

namespace kpi\Http\Controllers;

use Illuminate\Http\Request;
use kpi\b_indicadores;
use kpi\Http\Requests\B_IndicadoresUpdateRequest;
use Redirect;
use Session;
use kpi\clientes;
use kpi\empresa;
use kpi\trackin;
use Mail;
use DB;

class b_indicadoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $b_indicadores = b_indicadores::b_indicadores();
        return view('b_indicadores.index', compact('b_indicadores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $empresas = empresa::pluck('empresa','id');
        return view('b_indicadores.crear', compact('empresas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $usuarios = $_POST["id_cliente"];


        $enviar_mail = $request->enviar_mail;


        if ($enviar_mail == 'SI')
        {
            foreach ($usuarios as $usuario)
            {

                $query = db::insert("INSERT INTO b_indicadores (empresa, historial_carga, modalidad, plan, es_admin, url_bi, status, created_at, usuario) VALUES ('$request->empresa','$request->historial_carga','$request->modalidad', '0', '$request->es_admin','$request->url_bi','$request->status',NOW(),'{$usuario}')");

                $id_cliente = $usuario;
                $results = DB::select("SELECT email FROM clientes WHERE id = '$id_cliente'");
                $results_mail = $results[0];
                $mail_cliente=$results_mail->email;

                Mail::send('emails.nuevoKpianalytics',$request->all(), function($msj) use ($mail_cliente){
                    $msj->subject('Registro Benchmarks de Indicadores - KPI Estudios');
                    $msj->to($mail_cliente);
                });
            }
            Session::flash('message', 'Servicio y Correos enviados Correctamente');
            return Redirect::to('bench_indicadores');
        }else{

                $query = db::insert("INSERT INTO b_indicadores (empresa, historial_carga, modalidad, plan, es_admin, url_bi, status, created_at, usuario) VALUES ('$request->empresa','$request->historial_carga','$request->modalidad', '0', '$request->es_admin','$request->url_bi','$request->status',NOW(),'{$usuarios}')");

            Session::flash('message', 'Servicio Activado Correctamente');
            return Redirect::to('bench_indicadores');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $b_indicadores = b_indicadores::find($id);
        $clientes = clientes::select(DB::raw("CONCAT(nombre,' ',apellido) AS nombre"),'id')->where('status', 1)->pluck('nombre', 'id');
        return view('b_indicadores.edit',['b_indicadores'=>$b_indicadores])->with(compact('clientes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, B_IndicadoresUpdateRequest $request)
    {
        $b_indicadores = b_indicadores::find($id)->update($request->all());
        Session::flash('message', 'Servicio de Benchmark de Indicadores editado correctamente');
        return Redirect::to('bench_indicadores');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $trackin = b_indicadores::find($id);
        $trackin->delete();

        Session::flash('message', 'Servicio Benchmark de Indicadores eliminado correctamente');
        return Redirect::to('bench_indicadores');
    }
}

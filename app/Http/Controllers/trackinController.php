<?php

namespace kpi\Http\Controllers;

use kpi\Http\Requests;
use kpi\Http\Requests\TrackinCreateRequest;
use kpi\Http\Requests\TrackinUpdateRequest;
use kpi\Http\Controllers\Controller;
use Redirect;
use Session;
use kpi\clientes;
use kpi\empresa;
use kpi\trackin;
use Illuminate\Http\Request;
use Mail;
use DB;
use Auth;

class trackinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trackins = trackin::trackins();
        return view('trackins.index', compact('trackins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $empresas = empresa::pluck('empresa','id');
        return view('trackins.crear', compact('empresas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $usuarios = $_POST["id_cliente"];

        $enviar_mail = $request->enviar_mail;
        

        if ($enviar_mail == 'SI')
        {
            foreach ($usuarios as $usuario)
            {

                $query = db::insert("INSERT INTO trackin (empresa, historial_carga, modalidad, plan, es_admin, url_bi, status, created_at, usuario) VALUES ('$request->empresa','$request->historial_carga','$request->modalidad', '0', '$request->es_admin','$request->url_bi','$request->status',NOW(),'{$usuario}')");

                $id_cliente = $usuario;
                $results = DB::select("SELECT email FROM clientes WHERE id = '$id_cliente'");
                $results_mail = $results[0];
                $mail_cliente=$results_mail->email; 

                Mail::send('emails.nuevoKpianalytics',$request->all(), function($msj) use ($mail_cliente){
                $msj->subject('Registro KPI Analytics - KPI Estudios');
                $msj->to($mail_cliente);
                });                    
            }
            Session::flash('message', 'Servicio y Correos enviados Correctamente');
            return Redirect::to('trackins');
        }else{
            //dd($usuarios);

                $query = db::insert("INSERT INTO trackin (empresa, historial_carga, modalidad, plan, es_admin, url_bi, status, created_at, usuario) VALUES ('$request->empresa','$request->historial_carga','$request->modalidad', '0', '$request->es_admin','$request->url_bi','$request->status',NOW(),'{$usuarios}')");

            Session::flash('message', 'Servicio Activado Correctamente');
            return Redirect::to('trackins'); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $trackin = trackin::find($id);
        $clientes = clientes::select(DB::raw("CONCAT(nombre,' ',apellido) AS nombre"),'id')->where('status', 1)->pluck('nombre', 'id');
        return view('trackins.edit',['trackin'=>$trackin])->with(compact('clientes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, TrackinUpdateRequest $request)
    {
        $trackin = trackin::find($id)->update($request->all());
        Session::flash('message', 'Servicio de KPI Analytics editado correctamente');
        return Redirect::to('trackins');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $trackin = trackin::find($id);
        $trackin->delete();

        Session::flash('message', 'Servicio KPI Analytics eliminado correctamente');
        return Redirect::to('trackins');
    }
}

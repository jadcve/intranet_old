<?php

namespace kpi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CarouselCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'titulo' => 'required|unique:carousels',
            'descripcion' => 'required',
            'status' => 'required',
        ];
    }
}

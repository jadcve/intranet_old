<?php

namespace kpi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EstudioCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'estudio' => 'required',
            'descripcion' => 'required',
            'tipo' => 'required',
            'id_cliente' => 'required',
            'empresa' => 'required',
            'status' => 'required',
            'carga_datos' => 'required',
        ];
    }
}

<?php

namespace kpi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PromotionCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo' => 'required',
            'texto_corto' => 'required',
            'usuario' => 'required',
            'tipo' => 'required',
            'status' => 'required'
        ];
    }
}

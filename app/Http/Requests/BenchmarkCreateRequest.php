<?php

namespace kpi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BenchmarkCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'benchmark' => 'required',
            'descripcion' => 'required',
            'tipo' => 'required',
            'usuario' => 'required',
            'status' => 'required',
        ];
    }
}

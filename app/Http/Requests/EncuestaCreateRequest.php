<?php

namespace kpi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EncuestaCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'encuesta' => 'required',
            'descripcion' => 'required',
            'document' => 'required',
            'usuario' => 'required',
            'status' => 'required',
        ];
    }
}

<?php

namespace kpi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class B_IndicadoresCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'empresa' => 'required',
            'usuario' => 'required',
            'historial_carga' => 'required',
            'modalidad' => 'required',
            'es_admin' => 'required',
            'url_bi' => 'required',
            'status' => 'required',
        ];
    }
}
<?php

namespace kpi;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;

class bitacora extends Model
{


	protected $table = 'bitacoras';

    protected $fillable = [
        'usuario', 'empresa', 'pagina', 'fecha',
    ];


    public static function bitacoras(){
    return DB::table('bitacoras')
    ->join('clientes','clientes.id','=','bitacoras.usuario')
    ->join('empresas','empresas.id','=','bitacoras.empresa')
    ->select('bitacoras.*','clientes.nombre','clientes.apellido','empresas.empresa')
    ->whereNotIn('bitacoras.usuario', [87,95,96,97,98,99,299])
    ->orderBy('bitacoras.fecha','desc')
    ->get();
    }
}


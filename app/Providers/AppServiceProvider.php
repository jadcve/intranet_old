<?php

namespace kpi\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        if(env('REDIRECT_HTTPS')) {
            $url->formatScheme('https');
        }

        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {


        if (env('APP_ENV') === 'production') {
            $this->app['url']->forceScheme('https');
        }
    }
}
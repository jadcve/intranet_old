<?php

namespace kpi;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;

class imagenes_carousel extends Model
{
    protected $table = 'imagenes_carousels';

    protected $fillable = [
        'carousel', 'imagen', 'status'
    ];
}

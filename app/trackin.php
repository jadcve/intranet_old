<?php

namespace kpi;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use DB;

class trackin extends Model
{
    protected $table = 'trackin';

    protected $fillable = [
        'empresa', 'usuario', 'historial_carga', 'modalidad', 'plan', 'es_admin', 'url_bi', 'status',
    ];

    public static function trackins(){
    return DB::table('trackin')
    ->join('empresas','empresas.id','=','trackin.empresa')
    ->join('clientes', 'clientes.id', '=', 'trackin.usuario')
    ->select('trackin.*','empresas.empresa', 'clientes.nombre', 'clientes.apellido')
    ->get();
    }
}
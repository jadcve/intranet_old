<?php

namespace kpi;

use Illuminate\Database\Eloquent\Model;

class tipo_causal extends Model
{
	protected $table = 'tipo_causal';

    protected $fillable = [
        'tipo', 'status'
    ];
}

<?php

namespace kpi;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class rubro extends Model
{
	use SoftDeletes;

	protected $table = 'rubros';

    protected $fillable = [
        'rubro', 'status'
    ];
}

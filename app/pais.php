<?php

namespace kpi;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class pais extends Model
{
	use SoftDeletes;

    protected $fillable = [
        'pais', 'ab', 'status'
    ];
}

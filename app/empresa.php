<?php
	
namespace kpi;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;

class empresa extends Model
{
	protected $table = 'empresas';

    protected $fillable = [
        'empresa', 'rubro', 'subrubro', 'telefono', 'origen', 'pais', 'web', 'path', 'status',
    ];

    public function setPathAttribute($path){
        if(!empty($path)){
	    $this->attributes['path'] = Carbon::now()->second.$path->GetClientOriginalName();
	    $name = Carbon::now()->second.$path->GetClientOriginalName();
	    \Storage::disk('logos')->put($name, \File::get($path));
        }

    }
        public static function Empresas(){
        return DB::table('empresas')
        ->join('rubros','rubros.id','=','empresas.rubro')
        ->join('subrubros','subrubros.id','=','empresas.subrubro')
        ->join('pais','pais.id','=','empresas.pais')
        ->select('empresas.*','rubros.rubro','subrubros.subrubro','pais.pais')
        ->orderBy('empresas.empresa','asc')
        ->get();
    }


}

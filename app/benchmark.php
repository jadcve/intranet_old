<?php

namespace kpi;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;

class benchmark extends Model
{
    protected $table = 'benchmarks';

    protected $fillable = [
        'benchmark', 'descripcion', 'tipo', 'path', 'url', 'envio_mail', 'empresa', 'usuario', 'grupo', 'status', 'created_at'
    ];

    public function setPathAttribute($path){
        if(!empty($path)){
            $this->attributes['path'] = Carbon::now()->second.$path->GetClientOriginalName();
            $name = Carbon::now()->second.$path->GetClientOriginalName();
            \Storage::disk('archivos')->put($name, \File::get($path));
        }
    }

    public static function Benchmarks(){
        return DB::table('benchmarks')
        ->join('clientes','clientes.id','=','benchmarks.usuario')
        ->select('benchmarks.*','clientes.nombre', 'clientes.apellido')
        ->get();
    }
}

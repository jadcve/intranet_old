<?php

namespace kpi;

use Illuminate\Database\Eloquent\Model;
use DB;

class b_indicadores extends Model
{
    protected $table = 'b_indicadores';

    protected $fillable = [
        'empresa', 'usuario', 'historial_carga', 'modalidad', 'plan', 'es_admin', 'url_bi', 'status',
    ];

    public static function b_indicadores(){
        return DB::table('b_indicadores')
            ->join('empresas','empresas.id','=','b_indicadores.empresa')
            ->join('clientes', 'clientes.id', '=', 'b_indicadores.usuario')
            ->select('b_indicadores.*','empresas.empresa', 'clientes.nombre', 'clientes.apellido')
            ->get();
    }
}

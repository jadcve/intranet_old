<?php

namespace kpi;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use DB;

class reg_cargas extends Model
{
    
    protected $table = 'reg_carga';

	protected $fillable = [
    'empresa', 'usuario', 'mes', 'anio', 'tipo_carga', 'created_at',
    ];

    public static function reg_datos_carga(){
    return DB::table('reg_carga')
    ->join('empresas','empresas.id','=','reg_carga.empresa')
    ->join('clientes', 'clientes.id', '=', 'reg_carga.usuario')
    ->select('reg_carga.*','empresas.empresa', 'clientes.nombre', 'clientes.apellido')
    ->get();
    }
}
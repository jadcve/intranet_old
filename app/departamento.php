<?php

namespace kpi;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class departamento extends Model
{
	use SoftDeletes;

    protected $fillable = [
        'departamento', 'status'
    ];
}

<?php

namespace kpi;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;

class promotion extends Model
{
    use Notifiable;

    protected $table = 'promotions';

    protected $fillable = [
        'titulo', 'texto_corto', 'usuario', 'tipo', 'envio_mail', 'mensaje', 'carousel', 'imagen', 'video', 'texto_wizard', 'url_wizard', 'status', 'created_at'
    ];

    public function setPathAttribute($imagen){
        if(!empty($imagen)){
        $this->attributes['imagen'] = Carbon::now()->second.$imagen->GetClientOriginalName();
        $name = Carbon::now()->second.$imagen->GetClientOriginalName();
        \Storage::disk('carousel')->put($name, \File::get($imagen));
        }
    }

  

    public static function promotions(){
        return DB::table('promotions')
        ->join('clientes','clientes.id','=','promotions.usuario')
        ->select('promotions.*','clientes.nombre', 'clientes.apellido')
        ->get();
    }

}

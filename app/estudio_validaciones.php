<?php

namespace kpi;

use Illuminate\Database\Eloquent\Model;
use DB;

class estudio_validaciones extends Model
{
    protected $table = 'validacion_estudios';
    protected $primaryKey = 'id';
    public $timestamps = false;

}

@extends('layouts.principal')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Dashboard Paises</h2>
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Formulario</li>
                <li class="active"><strong>Editar Pa&iacute;s</strong></li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">            
        <div class="col-lg-5">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Editar Pa&iacute;s</h5>
                </div>
                <div class="ibox-content">
                	@include('alerts.request')
    				{!! Form::model($pais,['route'=> ['paises.update',$pais->id], 'method'=>'PUT']) !!}
    	    			<div class="form-group">
                            {!! Form::label('Nombre del pa&iacute;s') !!}
                            {!! Form::text('pais', null, ['placeholder'=>'Pa&iacute;s', 'class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Abreviatura del pa&iacute;s') !!}
                            {!! Form::text('ab', null, ['placeholder'=>'Abreviatura', 'class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Estatus del pa&iacute;s') !!}
                            {!! Form::select('status', ['1' => 'Activo', '0' => 'Inactivo'], '1',['class' => 'form-control']) !!}
                        </div>
    	            	{!! Form::submit('Actualizar', ['class' => 'btn btn-primary block full-width m-b']) !!}				            
    				{!! Form::close() !!}
    				
    				@if ( Auth::user()->rol == 1) 
                    {!! Form::model($pais,['route'=> ['paises.destroy',$pais->id], 'method'=>'DELETE']) !!}
    	    			
    	            	{!! Form::submit('Eliminar', ['class' => 'btn btn-danger block full-width m-b']) !!}				            
    				{!! Form::close() !!}
                    @endif
                </div>
            </div>
        </div>
    </div>
    </div>
@stop
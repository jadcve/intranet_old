@extends('layouts.principal')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Dashboard Cargos</h2>
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Formulario</li>
                <li class="active"><strong>Editar Cargos</strong></li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">            
        <div class="col-lg-5">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Editar Cargo</h5>
                </div>
                <div class="ibox-content">
                	@include('alerts.request')
    				{!! Form::model($cargo,['route'=> ['cargos.update',$cargo->id], 'method'=>'PUT']) !!}
    	    			<div class="form-group">
                            {!! Form::label('Nombre del Cargo') !!}
                            {!! Form::text('cargo', null, ['placeholder'=>'Cargo', 'class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Estatus del Cargo') !!}
                            {!! Form::select('status', ['1' => 'Activo', '0' => 'Inactivo'], $cargo->status, ['class' => 'form-control']) !!}
                        </div>
    	            	{!! Form::submit('Actualizar', ['class' => 'btn btn-primary block full-width m-b']) !!}				            
    				{!! Form::close() !!}
    				@if ( Auth::user()->rol == 1) 
    				{!! Form::model($cargo,['route'=> ['cargos.destroy',$cargo->id], 'method'=>'DELETE']) !!}
    	    			
    	            	{!! Form::submit('Eliminar', ['class' => 'btn btn-danger block full-width m-b']) !!}				            
    				{!! Form::close() !!}
                    @endif
                </div>
            </div>
        </div>
    </div>
    </div>
@stop
@extends('layouts.principal')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Dashboard Usuarios</h2>
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Formulario</li>
                <li class="active"><strong>Editar Usuario</strong></li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">            
        <div class="col-lg-5">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Editar Usuario</h5>
                </div>
                <div class="ibox-content">
                	@include('alerts.request')
					{!! Form::model($user,['route'=> ['usuarios.update',$user->id], 'method'=>'PUT', 'files' => true]) !!}
                        <div class="form-group">
                            {!! Form::label('Nombre del usuario') !!}
                            {!! Form::text('nombre', null, ['placeholder'=>'Nombre', 'class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Apellido del usuario') !!}
                            {!! Form::text('apellido', null, ['placeholder'=>'Apellido', 'class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Tel&eacute;fono del usuario') !!}
                            {!! Form::text('telefono', null,['placeholder'=>'Tel&eacute;fono', 'class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Foto del usuario') !!}
                            {!! Form::file('path', ['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Email del usuario') !!}
                            {!! Form::email('email', null,['placeholder'=>'Email','class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Password del usuario') !!}
                             {{ Form::password('password',array('placeholder'=>'Password','class' => 'form-control')) }}
                        </div>
                        @if ( Auth::user()->rol == 1) 
                        <div class="form-group">
                            {!! Form::label('Nivel') !!}
                            {!! Form::select('rol', ['1' => 'Administrador', '2' => 'Usuario'], '2',['class' => 'form-control']) !!}
                        </div>
                        @endif
                        <div class="form-group">
                            {!! Form::label('Cambiar Password al iniciar sesi&oacute;n') !!}
                            {!! Form::select('password_new', ['1' => 'Si', '0' => 'No'], '0',['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Estatus del Usuario') !!}
                            {!! Form::select('status', ['1' => 'Activo', '0' => 'Inactivo'], '1',['class' => 'form-control']) !!}
                        </div>
		            	{!! Form::submit('Actualizar', ['class' => 'btn btn-primary block full-width m-b']) !!}				            
					{!! Form::close() !!}


                    @if ( Auth::user()->rol == 1) 
					{!! Form::model($user,['route'=> ['usuarios.destroy',$user->id], 'method'=>'DELETE']) !!}
		    			
		            	{!! Form::submit('Eliminar', ['onClick'=>"javascript: return confirm('¿Esta seguro que desea eliminar el registro? todos los datos asociados a este ID de usuario se perderan');",'class' => 'btn btn-danger block full-width m-b']) !!}				            
					{!! Form::close() !!}
                    @endif</td>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
<div class="form-group">
	{!! Form::label('Nombre del usuario') !!}
    {!! Form::text('nombre', null, ['placeholder'=>'Nombre', 'class'=>'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('Apellido del usuario') !!}
    {!! Form::text('apellido', null, ['placeholder'=>'Apellido', 'class'=>'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('Tel&eacute;fono del usuario') !!}
    {!! Form::text('telefono', null,['placeholder'=>'Tel&eacute;fono', 'class'=>'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('Foto del usuario') !!}
    {!! Form::file('path', ['class'=>'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('Email del usuario') !!}
    {!! Form::email('email', null,['placeholder'=>'Email','class'=>'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('Password del usuario') !!}
     {{ Form::password('password',array('placeholder'=>'Password','class' => 'form-control')) }}
</div>
<div class="form-group">
	{!! Form::label('Nivel') !!}
    {!! Form::select('rol', ['1' => 'Administrador', '2' => 'Usuario'], '2',['class' => 'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('Cambiar Password al iniciar sesi&oacute;n') !!}
    {!! Form::select('password_new', ['1' => 'Si', '0' => 'No'], '0',['class' => 'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('Estatus del Usuario') !!}
    {!! Form::select('status', ['1' => 'Activo', '0' => 'Inactivo'], '1',['class' => 'form-control']) !!}
</div>
@extends('layouts.principal')

@section('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
@endsection

@section('content')

	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2>Dashboard Usuarios</h2>
	        <ol class="breadcrumb">
	            <li>Home</li>
	            <li>Formulario</li>
	            <li class="active"><strong>Ver Usuarios</strong></li>
	        </ol>
	    </div>
	</div>

	@if(Session::has('message'))
	<div class="alert alert-success alert-dismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  {{ Session::get('message') }}
	</div>
	@endif

        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Listado de Usuarios</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
					<table id="table_user" class="display">
						<thead>
							<tr>
								<th>Id</th>
								<th>Foto</th>
								<th>Nombre</th>
								<th>Apellido</th>
								<th>Tel&eacute;fono</th>
								<th>Email</th>
								<th>Nivel</th>
								<th>Estatus</th>
								<th>Acci&oacute;n</th>
							</tr>
						</thead>
						<tbody>
							@foreach($users as $user)
							<tr>
								<td>{{ $user->id }}</td>
								<td><img src="{!! asset('img/fotos') !!}/{{ $user->path }}" width="50px" height="50px" /></td>
								<td>{{ $user->nombre }}</td>
								<td>{{ $user->apellido }}</td>
								<td>{{ $user->telefono }}</td>
								<td>{{ $user->email }}</td>
								<td>@if ( $user->rol == 1) Administrador @else Usuario @endif</td>
								<td>@if ( $user->status == 1) <p class="text-success">Activo</p> @else <p class="text-danger">Inactivo</p> @endif</td>
								@if ( Auth::user()->rol == 1 or $user->id == Auth::user()->id) <td>{!! link_to_route('usuarios.edit', $title = 'Editar', $parameters = $user->id, $attributes = ['class'=>'btn btn-primary']) !!}</td> @else <td>SIN PERMISO PARA EDITAR OTROS PERFILES</td> @endif
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
@endsection

@section('javascripts')
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
	<script>
		$(document).ready( function () {
		    $('#table_user').DataTable();
		} );
	</script>
@endsection
@extends('layouts.principal')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Dashboard Tipo de &Aacute;rea</h2>
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Formulario</li>
                <li class="active"><strong>Editar Tipo de &Aacute;rea</strong></li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">            
        <div class="col-lg-5">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Editar Tipo de &Aacute;rea</h5>
                </div>
                <div class="ibox-content">
                	@include('alerts.request')
    				{!! Form::model($tipo_area,['route'=> ['tipo_areas.update',$tipo_area->id], 'method'=>'PUT']) !!}
                        <div class="form-group">
                            {!! Form::label('Nombre del tipo de &aacute;rea') !!}
                            {!! Form::text('tipo', null, ['placeholder'=>'Tipo de &Aacute;are', 'class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Estatus del Tipo de &Aacute;rea') !!}
                            {!! Form::select('status', ['1' => 'Activo', '0' => 'Inactivo'], '1',['class' => 'form-control']) !!}
                        </div>
    	            	{!! Form::submit('Actualizar', ['class' => 'btn btn-primary block full-width m-b']) !!}				            
    				{!! Form::close() !!}
    				@if ( Auth::user()->rol == 1)
    				{!! Form::model($tipo_area,['route'=> ['tipo_areas.destroy',$tipo_area->id], 'method'=>'DELETE']) !!}
    	    			
    	            	{!! Form::submit('Eliminar', ['class' => 'btn btn-danger block full-width m-b']) !!}				            
    				{!! Form::close() !!}
                    @endif
                </div>
            </div>
        </div>
    </div>
    </div>
@stop
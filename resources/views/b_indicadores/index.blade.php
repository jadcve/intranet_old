@extends('layouts.principal')

@section('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Dashboard Benchmarks Indicadores</h2>
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Formulario</li>
                <li class="active"><strong>Benchmarks Indicadores</strong></li>
            </ol>
        </div>
    </div>

    @if(Session::has('message'))
    <div class="alert alert-success alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ Session::get('message') }}
    </div>
    @endif

    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Listado de Clientes</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <table id="table_id" class="display">
                    <thead>
                        <tr>
                            <th>Id Registro</th>
                            <th>Empresa</th>
                            <th>Cliente</th>
                            <th>Administrador</th>
                            <th>Historial de Carga</th>
                            <th>Modalidad</th>
                            <th>Informe</th>
                            <th>Estatus</th>
                            <th>Acci&oacute;n</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        @foreach($b_indicadores as $b)
                        <tr>
                            <td>{{ $b->id }}</td>
                            <td>{{ $b->empresa }}</td>
                            <td>{{ $b->nombre }} {{ $b->apellido }}</td>
                            <td>@if($b->es_admin == 1) Si @else No @endif</td>
                            <td>@if($b->historial_carga == 1) Si @else No @endif</td>
                            <td>@if($b->modalidad == 1) Paga @else Gratis @endif</td>
                            @if(!empty($b->url_bi)) <td><a href="{!! $b->url_bi !!}" target="_blank"><button class="btn btn-primary">Ver Informe</button></a></td> @else <td> <p class="text-danger">Sin Informe</p> @endif
                            <td>@if($b->status == 1) <p class="text-success">Activo</p> @else <p class="text-danger">Inactivo</p> @endif</td>
                            <td>{!! link_to_route('bench_indicadores.edit', $title = 'Editar', $parameters = $b->id, $attributes = ['class'=>'btn btn-primary']) !!}</td>
                        </tr>
                        @endforeach
                    </tbody>                        
                </table>                    
            </div>
        </div>
    </div>
@endsection

@section('javascripts')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script>

        $(document).ready(function() {
            $('#table_id').DataTable( {
                "lengthMenu": [[100, 50, 20, -1], [100, 50, 20, "Todos"]]
            } );
        } );
    </script>
@endsection
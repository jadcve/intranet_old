@extends('layouts.principal')

@section('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Dashboard Benchmark de Indicadores</h2>
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Formulario</li>
                <li class="active"><strong>Benchmark de Indicadores</strong></li>
            </ol>
        </div>
    </div>

    @if(Session::has('message'))
    <div class="alert alert-success alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ Session::get('message') }}
    </div>
    @endif

    <div class="row">
        <div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Editar Servicio Benchmark de Indicadores</h5>
                </div>
                <div class="ibox-content">
                    @include('alerts.request')
                    {!! Form::model($b_indicadores,['route'=> ['bench_indicadores.update',$b_indicadores->id], 'method'=>'PUT', 'files' => true]) !!}
                        <div class="form-group">
                            {!! Form::label('Usuario') !!}
                            {!! Form::select('usuario',  $clientes, null,['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Historial de Carga') !!}
                            {!! Form::select('historial_carga', ['1' => 'Activo', '0' => 'Inactivo'], $b_indicadores->historial_carga,['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Modalidad') !!}
                            {!! Form::select('modalidad', ['1' => 'Paga', '0' => 'Gratis'], $b_indicadores->modalidad,['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Administrador') !!}
                            {!! Form::select('es_admin', ['1' => 'Si', '0' => 'No'], $b_indicadores->es_admin,['class' => 'form-control']) !!}
                        </div> 
                        <div class="form-group">
                            {!! Form::label('Dashboard') !!}
                            {!! Form::text('url_bi', null,['class'=>'form-control']) !!}
                        </div>                                                 
                        <div class="form-group">
                            {!! Form::label('Estatus del Servicio') !!}
                            {!! Form::select('status', ['1' => 'Activo', '0' => 'Inactivo'], $b_indicadores->status,['class' => 'form-control']) !!}
                        </div>
                        {!! Form::submit('Actualizar', ['class' => 'btn btn-primary block full-width m-b']) !!}                          
                    {!! Form::close() !!}
                    @if ( Auth::user()->rol == 1)
                    {!! Form::model($b_indicadores,['route'=> ['bench_indicadores.destroy',$b_indicadores->id], 'method'=>'DELETE']) !!}
                        
                        {!! Form::submit('Eliminar', ['onClick'=>"javascript: return confirm('¿Esta seguro que desea eliminarlo?');",'class' => 'btn btn-danger block full-width m-b']) !!}
                    {!! Form::close() !!}
                    @endif
                </div>
            </div>
        </div>        
    </div>
@stop
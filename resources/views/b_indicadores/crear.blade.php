@extends('layouts.principal')

@section('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Benchmark de Indicadores</h2>
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Formulario</li>
                <li class="active"><strong>Benchmark de Indicadores</strong></li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Activar Usuario</h5>
                </div>
                <div class="ibox-content">
                    @include('alerts.request')
                    {!! Form::open(['route'=> 'bench_indicadores.store', 'method'=>'POST', 'files' => true]) !!}
                        <div class="form-group">
                            <input type="checkbox" name="enviar_mail" value="SI"> &nbsp;{!! Form::label('Enviar correo de Activaci&oacute;n de Servicio') !!}
                        </div>

                    <div class="form-group">
                        <font color="red">* </font>{!! Form::label('Seleccionar Empresa') !!}
                        {!! Form::select('empresa',$empresas,null,['placeholder'=>'Selecciona','class'=>'form-control', 'id'=>'empresa', 'onchange' => 'actualizar_cliente()']) !!}
                    </div>
                    <div class="form-group">
                        <font color="red">* </font>{!! Form::label('Seleccionar Usuario') !!}
                        {!! Form::select('id_cliente',[],null,['placeholder'=>'Selecciona','class'=>'form-control', 'id'=>'cliente', 'required']) !!}
                    </div>



                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Historial de Carga') !!}
                            {!! Form::select('historial_carga', ['placeholder'=>'Selecciona', '1' => 'Activo', '0' => 'Inactivo'], null,['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Modalidad') !!}
                            {!! Form::select('modalidad', ['placeholder'=>'Selecciona', '1' => 'Paga', '0' => 'Gratis'], null,['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Administrador') !!}
                            {!! Form::select('es_admin', ['placeholder'=>'Selecciona', '1' => 'Si', '0' => 'No'], null,['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('URL Informe') !!}
                            {!! Form::text('url_bi', null, ['placeholder'=>'Link del Informe del Cliente', 'class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Estatus del Servicio') !!}
                            {!!Form::select('status', ['placeholder'=>'Selecciona', '1' => 'Activo', '0' => 'Inactivo'], null, ['class'=>'form-control'] )!!}
                        </div>
                        {!! Form::submit('Activar', ['class' => 'btn btn-primary block full-width m-b']) !!}                          
                    {!! Form::close() !!}
                </div>
            </div>
        </div>        
    </div>

@endsection
@section('javascripts')
{!!Html::script('js/dropdown.js')!!}
{!!Html::script('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js')!!}
<script>
    // In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    $('.basic-single').select2();
});
</script>
@endsection

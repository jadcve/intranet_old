@extends('layouts.principal')
@section('content')
<!--Funcion para ocultar y mostrar input segun seleccion-->
    <script language="javascript" type="text/javascript">
    function d1(selectTag){
     if(selectTag.value == '1'){
        $('#bloque_archivo').show(); 
        $('#bloque_url').hide();
     document.getElementById('archivo').disabled = false;
      document.getElementById('url').disabled = true;
     }else if(selectTag.value == '2'){
        $('#bloque_archivo').hide(); 
        $('#bloque_url').show();
        document.getElementById('archivo').disabled = true;
        document.getElementById('url').disabled = false;
     } else {
        $('#bloque_archivo').hide(); 
        $('#bloque_url').hide();
     }
    }
    </script>
<!--Fin Funcion para ocultar y mostrar input segun seleccion-->

<!--CCS para ocultar div antes de seleccionar-->
    <style type="text/css">
        .bloque_archivo{
            display:none;
        }
        .bloque_url{
            display:none;
        }

    </style>
<!--FIN CCS para ocultar div antes de seleccionar-->
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Dashboard Benchmarks</h2>
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Formulario</li>
                <li class="active"><strong>Benchmarks</strong></li>
            </ol>
        </div>
    </div>

    <div class="row">
    	<div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Crear Benchmark</h5>
                </div>
                <div class="ibox-content">
    				@include('alerts.request')
    				{!! Form::open(['route'=> 'benchmarks.store', 'method'=>'POST', 'files' => true]) !!}
                        <div class="form-group">
                            <input type="checkbox" name="enviar_mail" value="SI"> &nbsp;{!! Form::label('Enviar correo Creaci&oacute;n de Benchmark') !!}
                        </div>
    					<div class="form-group">
    						<font color="red">* </font>{!! Form::label('Nombre del Benchmark') !!}
    					    {!! Form::text('benchmark', null, ['placeholder'=>'Benchmark', 'class'=>'form-control']) !!}
    					</div>
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Descripci&oacute;n del Benchmark') !!}
                            {!! Form::text('descripcion', null, ['placeholder'=>'Descripci&oacute;n', 'class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Tipo de Documento') !!}
                            <select name="tipo" onchange="d1(this)" class="form-control">
                                <option value="0">Seleccionar</option>
                                <option value="1">Archivo</option>
                                <option value='2'>URL</option>
                            </select><br>
                            <div id="bloque_archivo" class="bloque_archivo">                              
                                <font color="red">* </font>{!! Form::label('Archivo') !!}
                                <div title="Archivos admitidos: pdf, xlsx, pptx, docx, rar - Tamaño máximo 100 Megas"><input type='file' id="archivo" name='path' disabled="true" class="form-control"></div>
                            </div>
                            <div id="bloque_url" class="bloque_url">                                   
                                <font color="red">* </font>{!! Form::label('URL') !!}
                                <input type='text' id="url" name='url' disabled="true" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Usuarios') !!}
                            {{Form::select('usuario[]',$clientes,null,array('multiple'=>true,'class'=>'form-control'))}}
                        </div>
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Estatus del Benchmark') !!}
                            {!! Form::select('status', ['1' => 'Activo', '0' => 'Inactivo'], '1',['class' => 'form-control']) !!}
                        </div>
    	            	{!! Form::submit('Crear', ['class' => 'btn btn-primary block full-width m-b']) !!}				            
    				{!! Form::close() !!}
                </div>
            </div>
        </div>        
    </div>
@stop
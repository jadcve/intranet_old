@extends('layouts.principal')

@section('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
@endsection

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Dashboard Estudios</h2>
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Formulario</li>
                <li class="active"><strong>Estudios</strong></li>
            </ol>
        </div>
    </div>

    @if(Session::has('message'))
    <div class="alert alert-success alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ Session::get('message') }}
    </div>
    @endif

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Listado de Estudios</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <a href="{!! URL::to('estudios/create') !!}"><button class="btn btn-primary">Crear Estudio</button></a>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <table id="table_id" class="display">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Descripci&oacute;n</th>
                                <th>Tipo</th>
                                <th></th>
                                <th>Usuario</th>
                                <th>Estatus</th>
                                <th>Acci&oacute;n</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($estudios as $estudio)
                            <tr>                        
                                <td>{{ $estudio->estudio }}</td>
                                <td>{{ $estudio->descripcion }}</td>
                                <td>
                                <!-- Inicio Cargar el tipo de archivo -->
                                <?php
                                        $file = $estudio->path;
                                        $archivo = $file;
                                        $valores = explode(".", $archivo);

                                        
                                        //Formato valores
                                        $valores[0];
                                        $extension = $valores[count($valores)-1];

                                    if ( $estudio->tipo == 1){
                                        if($extension == "pdf"){echo '<img src="img/file/pdf.png" width="35px">';}
                                        elseif($extension == "rar"){echo '<img src="img/file/rar.png" width="35px">';}
                                        elseif($extension == "xlsx"){echo '<img src="img/file/excel.png" width="35px">';}
                                        elseif($extension == "pptx"){echo '<img src="img/file/power_point.png" width="35px">';}
                                        elseif($extension == "docx"){echo '<img src="img/file/word.png" width="35px">';}
                                        else{echo "<td>NO DEFINIDO</td>";}
                                    }else{
                                        echo'<img src="img/file/power_bi.png" width="35px">';
                                    }

                                    $ruta = "archivos/".$estudio->path;
                                    $url = file_exists($ruta);
                                    
                                    if($extension == "pdf"){
                                        echo '<td><a href="'.$ruta.'" target="_blank"><button class="btn btn-primary">Ver Estudio</button></a></td>';
                                    }elseif($extension == "rar"){
                                        echo '<td><a href="'.$ruta.'"><button type="submit" class="btn btn-primary" style="width:90px; height:35px">Descargar</button></a></td>';
                                    }elseif($extension == "xlsx"){
                                        echo '<td><a href="'.$ruta.'"><button type="submit" class="btn btn-primary" style="width:90px; height:35px">Descargar</button></a></td>';
                                    }elseif($extension == "pptx"){
                                        echo '<td><a href="'.$ruta.'"><button type="submit" class="btn btn-primary" style="width:90px; height:35px">Descargar</button></a></td>';
                                    }elseif($extension == "docx"){
                                        echo '<td><a href="'.$ruta.'"><button type="submit" class="btn btn-primary" style="width:90px; height:35px">Descargar</button></a></td>';
                                    }elseif(!is_file($url)){
                                        echo '<td><a href="'.$estudio->url.'" target="_blank"><button class="btn btn-primary">Ver Estudio</button></a></td>';
                                     }else{
                                        echo "<td>NO DEFINIDO</td>";
                                    }
                                ?>
                                </td>
                                <!-- Fin Cargar el tipo de archivo -->                            
                                <td>{{ $estudio->nombre }} {{ $estudio->apellido }}</td>
                                <td>@if ( $estudio->status == 1) <p class="text-success">Activo</p> @else <p class="text-danger">Inactivo</p> @endif</td>
                                <td>{!! link_to_route('estudios.edit', $title = 'Editar', $parameters = $estudio->id, $attributes = ['class'=>'btn btn-primary']) !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>    
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascripts')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script>
    
    $(document).ready(function() {
    $('#table_id').DataTable( {
        "lengthMenu": [[100, 50, 20, -1], [100, 50, 20, "Todos"]]
    } );
} );
    </script>
@endsection
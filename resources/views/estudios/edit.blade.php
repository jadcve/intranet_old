@extends('layouts.principal')
@section('content')

<!--Funcion para ocultar y mostrar input segun seleccion-->
    <script language="javascript" type="text/javascript">
    function d1(selectTag){
     if(selectTag.value == '1'){
        $('#bloque_archivo').show(); 
        $('#bloque_url').hide();
     document.getElementById('archivo').disabled = false;
      document.getElementById('url').disabled = true;
     }else if(selectTag.value == '2'){
        $('#bloque_archivo').hide(); 
        $('#bloque_url').show();
        document.getElementById('archivo').disabled = true;
        document.getElementById('url').disabled = false;
     } else {
        $('#bloque_archivo').hide(); 
        $('#bloque_url').hide();
     }
    }
    </script>
<!--Fin Funcion para ocultar y mostrar input segun seleccion-->

<!--CCS para ocultar div antes de seleccionar-->
    <style type="text/css">
        .bloque_archivo{
            display:none;
        }
        .bloque_url{
            display:none;
        }

    </style>
<!--FIN CCS para ocultar div antes de seleccionar-->
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Dashboard Estudios</h2>
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Formulario</li>
                <li class="active"><strong>Estudios</strong></li>
            </ol>
        </div>
    </div>

    @if(Session::has('message'))
    <div class="alert alert-success alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ Session::get('message') }}
    </div>
    @endif

    <div class="row">
        <div class="col-lg-5">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Crear Estudio</h5>
                </div>
                <div class="ibox-content">
                    @include('alerts.request')
                    {!! Form::model($estudio,['route'=> ['estudios.update',$estudio->id], 'method'=>'PUT', 'files' => true]) !!}
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Nombre del Estudio') !!}
                            {!! Form::text('estudio', null, ['placeholder'=>'Estudio', 'class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Descripci&oacute;n del estudio') !!}
                            {!! Form::text('descripcion', null, ['placeholder'=>'Descripci&oacute;n', 'class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Tipo de Documento') !!}
                            {!! Form::select('tipo', ['0' => 'Seleccionar', '1' => 'Archivo', '2' => 'URL'], $estudio->tipo,['onchange'=>'d1(this)','class' => 'form-control']) !!}<br>
                            <div id="bloque_archivo" class="bloque_archivo">                              
                                <font color="red">* </font>{!! Form::label('Archivo') !!}
                                <div title="Archivos admitidos: pdf, xlsx, pptx, docx, rar - Tamaño máximo 100 Megas"><input type='file' id="archivo" name='path' disabled="true" class="form-control"></div>
                            </div>
                            <div id="bloque_url" class="bloque_url">                                   
                                <font color="red">* </font>{!! Form::label('URL') !!}
                                <input type='text' id="url" name='url' disabled="true" class="form-control">
                            </div>
                        </div>

                    <div class="form-group">
                        <font color="red">* </font>{!! Form::label('Seleccionar Empresa') !!}
                        {!! Form::select('empresa',$empresa,null,['class'=>'basic-single form-control', 'id'=>'empresa']) !!}
                    </div>


                    <div class="form-group">
                        {!! Form::label('Usuario') !!}
                        {!! Form::select('usuario', $clientes, null,['class'=>'form-control']) !!}
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Estatus del Estudio') !!}
                            {!! Form::select('status', ['1' => 'Activo', '0' => 'Inactivo'], '1',['class' => 'form-control']) !!}
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Habilitar Carga de Datos') !!}
                            {!! Form::select('carga_datos', ['1' => 'Si', '0' => 'No'], '1',['class' => 'form-control']) !!}
                        </div>
                    </div>


                    <div class="col-lg-6">
                        {!! Form::submit('Actualizar', ['class' => 'btn btn-primary block full-width m-b']) !!}                          
                        {!! Form::close() !!}
                    </div>
                    @if ( Auth::user()->rol == 1)
                        <div class="col-lg-6">
                        {!! Form::model($estudio,['route'=> ['estudios.destroy',$estudio->id], 'method'=>'DELETE']) !!}
                        {!! Form::submit('Eliminar', ['onClick'=>"javascript: return confirm('¿Esta seguro que desea eliminar el estudio?');",'class' => 'btn btn-danger block full-width m-b']) !!}                            
                        {!! Form::close() !!}
                        </div>
                    @endif
                </div>
            </div>
        </div>        
    </div>
@stop

@section('javascripts')
    {!!Html::script('js/dropdown.js')!!}
    {!!Html::script('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js')!!}
    <script>
        // In your Javascript (external .js resource or <script> tag)
        $(document).ready(function() {
            $('.basic-single').select2();
        });
    </script>
@endsection
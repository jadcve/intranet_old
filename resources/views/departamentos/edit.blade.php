@extends('layouts.principal')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Dashboard Departamentos</h2>
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Formulario</li>
                <li class="active"><strong>Editar Departamentos</strong></li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">            
        <div class="col-lg-5">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Editar Departamento</h5>
                </div>
                <div class="ibox-content">
                	@include('alerts.request')
    				{!! Form::model($departamento,['route'=> ['departamentos.update',$departamento->id], 'method'=>'PUT']) !!}
    	    			<div class="form-group">
                            {!! Form::label('Nombre del Departamento') !!}
                            {!! Form::text('departamento', null, ['placeholder'=>'Departamento', 'class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Estatus del Departamento') !!}
                            {!! Form::select('status', ['1' => 'Activo', '0' => 'Inactivo'], '1',['class' => 'form-control']) !!}
                        </div>
    	            	{!! Form::submit('Actualizar', ['class' => 'btn btn-primary block full-width m-b']) !!}				            
    				{!! Form::close() !!}
    				
    				{!! Form::model($departamento,['route'=> ['departamentos.destroy',$departamento->id], 'method'=>'DELETE']) !!}
    	    			
    	            	{!! Form::submit('Eliminar', ['class' => 'btn btn-danger block full-width m-b']) !!}				            
    				{!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    </div>
@stop
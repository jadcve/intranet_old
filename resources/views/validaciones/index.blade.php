@extends('layouts.principal')
@section('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
@endsection

@section('content')




<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Dashboard Validaciones</h2>
        <ol class="breadcrumb">
            <li>Home</li>
            <li>Validaciones</li>
            <li class="active"><strong>Ver Validaciones</strong></li>
        </ol>
    </div>
</div>

@if(Session::has('message'))
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        {{ Session::get('message') }}
    </div>

@endif
<div class="col-lg-12">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Listado de Validaciones</h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
    
        <br>
        <div >
            <a href="{!!url('estudio_validate/create')!!}" class = 'btn btn-primary'><i class="fa fa-pencil-square-o"></i> Crear Nueva Validación </a>
        </div>
        <br>

        <div class="ibox-content">
            <table id="table_id" class="display">

                <thead>
                <tr>

                    <th>Estudio</th>
                    <th>Nombre de la empresa</th>
                    <th>Cliente asignado</th>
                    <th>Acción</th>

                </tr>
                </thead>
                <tbody>
                @foreach($estudios_validaciones as $validaciones)
                    <tr>

                        <td>{{ $validaciones->estudio }}</td>
                        <td>{{ $validaciones->empresa }}</td>
                        <td>{{ $validaciones->nombre." ".$validaciones->apellido }}</td>
                        <td>

                            <a href = 'estudio_validate/delete/{!!$validaciones->id!!}' class = "btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"> Eliminar</i></a>

                        </td>
                    </tr>
                @endforeach


                </tbody>
            </table>

        </div>

    </div>
</div>
@endsection


@section('javascripts')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script>

        $(document).ready(function() {
            $('#table_id').DataTable( {
                "lengthMenu": [[100, 50, 20, -1], [100, 50, 20, "Todos"]],
                "order": "desc"
            } );
        } );
    </script>
@endsection
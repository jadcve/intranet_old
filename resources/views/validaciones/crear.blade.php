@extends('layouts.principal')
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Dashboard Validacioness</h2>
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Formulario</li>
                <li class="active"><strong>Validaciones</strong></li>
            </ol>
        </div>
    </div>

    @if(Session::has('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{ Session::get('message') }}
        </div>
    @endif

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Validaciones Estudio</h5>
                </div>
                <div class="ibox-content">
                    @include('alerts.request')
                    {!! Form::open(['route'=> 'estudio_validate.store', 'method'=>'POST', 'files' => true]) !!}
                    {!! csrf_field() !!}

                    <div class="col-lg-4">
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Estudio') !!}
                            {!! Form::select('estudios',$estudios,null,['placeholder'=>'Selecciona','class'=>'form-control', 'id'=>'estudios','required']) !!}
                        </div>
                    </div>




                    <div class="col-lg-4">
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Empresa') !!}
                            {!! Form::select('empresa',$empresas,null,['placeholder'=>'Selecciona','class'=>'basic-single form-control', 'id'=>'empresa', 'required']) !!}
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Seleccionar Usuario(s)') !!}
                            {!! Form::select('id_cliente[]',['placeholder'=>'Selecciona'],null,array('multiple'=>true, 'class'=>'form-control', 'id'=>'cliente')) !!}
                        </div>
                    </div>





                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Activación</th>
                            <th scope="col">Campo</th>
                            <th scope="col"><center>Validaciósssn 1</center></th>
                            <th scope="col"><center>Validación 2</center></th>
                            <th scope="col"><center>Validación 3</center></th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row">ID Empleado</th>
                            <td><center><input type="checkbox" name="v1_id_emp" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_id_emp" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_id_emp" value="1"></center></td>


                        </tr>
                        <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row">Nombre</th>
                            <td><center><input type="checkbox" name="v1_nombre" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_nombre" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_nombre" value="1"></center></td>


                        </tr>
                        <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row">Email</th>
                            <td><center><input type="checkbox" name="v1_email" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_email" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_email" value="1"></center></td>

                        </tr>


                        </tbody>
                    </table>
                </div>


                <div class="col-lg-2">
                    {!! Form::submit('Guardar Validaciones', ['class' => 'btn btn-primary block full-width m-b']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    {!!Html::script('https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js')!!}


    <script>
        $('tr td:first-child input[type="checkbox"]').click( function() {
            //enable/disable all except checkboxes, based on the row is checked or not
            $(this).closest('tr').find(":input:not(:first)").attr('disabled', !this.checked);
        });
    </script>


@stop

@section('javascripts')
    {!!Html::script('js/dropdown.js')!!}
    {!!Html::script('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js')!!}
    <script>
        // In your Javascript (external .js resource or <script> tag)
        $(document).ready(function() {
            $('.basic-single').select2();
        });
    </script>
@endsection








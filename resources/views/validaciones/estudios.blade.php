@extends('layouts.principal')
@section('content')


    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Dashboard Validacioness</h2>
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Formulario</li>
                <li class="active"><strong>Validaciones</strong></li>
            </ol>
        </div>
    </div>

    @if(Session::has('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{ Session::get('message') }}
        </div>
    @endif

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Validaciones Estudio</h5>
                </div>
                <div class="ibox-content">
                    @include('alerts.request')

                    {!! Form::open(['route'=> 'estudio_validate.store', 'method'=>'POST', 'files' => true]) !!}
                    {!! csrf_field() !!}

                    <div class="col-lg-4">
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Estudio') !!}
                            {!! Form::select('estudios',$estudios,null,['placeholder'=>'Selecciona','class'=>'form-control', 'id'=>'estudios','required']) !!}
                        </div>
                    </div>


                    <div class="col-lg-4">
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Empresa') !!}
                            {!! Form::select('empresa',$empresas,null,['placeholder'=>'Selecciona','class'=>'basic-single form-control', 'id'=>'empresa', 'required']) !!}
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Cliente') !!}
                            {!! Form::select('id_cliente',$clientes,null,['placeholder'=>'Selecciona','class'=>'form-control', 'id'=>'cliente', 'required']) !!}
                        </div>
                    </div>


                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Activación</th>
                            <th scope="col">Columna</th>
                            <th scope="col"><center>Campo Vacio</center></th>
                            <th scope="col"><center>Ceros (0)</center></th>
                            <th scope="col"><center>Valores Negativos</center></th>
                            <th scope="col"><center>Caracteres Esp.</center></th>
                            <th scope="col"><center>Valor Maximo</center></th>
                            <th scope="col"><center>Valor Minimo</center></th>
                            <th scope="col"><center>Valores Validos</center></th>
                            <th scope="col"><center>Fecha Maxima</center></th>
                            <th scope="col"><center>Fecha Minima</center></th>
                            <th scope="col"><center>Formato Correo</center></th>
                            <th scope="col"><center>Valores Duplicados</center></th>
                            <th scope="col"><center>Nombre Propio</center></th>
                            <th scope="col"><center>Padre e Hijo</center></th>
                            <th scope="col"><center>Formato Fecha</center></th>
                            <th scope="col"><center>Formato Numero</center></th>
                            <th scope="col"><center>Formato texto</center></th>


                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row">ID Empleado</th>
                            <td><center><input type="checkbox" name="v1_id_emp" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_id_emp" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_id_emp" value="1"></center></td>
                            <td><center><input type="checkbox" name="v4_id_emp" value="1"></center></td>

                            <td><center><input type="checkbox" name="v6_id_emp" value="1" onclick="insertar_input_mayor(this)" id="v6_id_emp" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v7_id_emp" value="1" onclick="insertar_input_menor(this)" id="v7_id_emp" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v8_id_emp" value="1" onclick="insertar_input_valor_valido(this)" id="v8_id_emp" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v9_id_emp" value="1" onclick="insertar_input_fecha_mayor(this)" id="v9_id_emp" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v10_id_emp" value="1" onclick="insertar_input_fecha_menor(this)" id="v10_id_emp" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v11_id_emp" value="1"></center></td>
                            <td><center><input type="checkbox" name="v12_id_emp" value="1"></center></td>
                            <td><center><input type="checkbox" name="v13_id_emp" value="1"></center></td>
                            <td><center><input type="checkbox" name="v16_id_emp" value="1" onclick="insertar_select_padre_hijo(this)" id="v16_id_emp" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v17_id_emp" value="1"></center></td>
                            <td><center><input type="checkbox" name="v18_id_emp" value="1"></center></td>
                            <td><center><input type="checkbox" name="v19_id_emp" value="1"></center></td>


                        </tr>
                        <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row">Nombre</th>
                            <td><center><input type="checkbox" name="v1_nombre" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_nombre" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_nombre" value="1"></center></td>
                            <td><center><input type="checkbox" name="v4_nombre" value="1"></center></td>

                            <td><center><input type="checkbox" name="v6_nombre" value="1" onclick="insertar_input_mayor(this)" id="v6_nombre" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v7_nombre" value="1" onclick="insertar_input_menor(this)" id="v7_nombre" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v8_nombre" value="1" onclick="insertar_input_valor_valido(this)" id="v8_nombre" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v9_nombre" value="1" onclick="insertar_input_fecha_mayor(this)" id="v9_nombre" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v10_nombre" value="1" onclick="insertar_input_fecha_menor(this)" id="v10_nombre" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v11_nombre" value="1"></center></td>
                            <td><center><input type="checkbox" name="v12_nombre" value="1"></center></td>
                            <td><center><input type="checkbox" name="v13_nombre" value="1"></center></td>
                            <td><center><input type="checkbox" name="v16_nombre" value="1" onclick="insertar_select_padre_hijo(this)" id="v16_nombre" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v17_nombre" value="1"></center></td>
                            <td><center><input type="checkbox" name="v18_nombre" value="1"></center></td>
                            <td><center><input type="checkbox" name="v19_nombre" value="1"></center></td>


                        </tr>
                        <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row">Email</th>
                            <td><center><input type="checkbox" name="v1_email" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_email" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_email" value="1"></center></td>
                            <td><center><input type="checkbox" name="v4_email" value="1"></center></td>

                            <td><center><input type="checkbox" name="v6_email" value="1" onclick="insertar_input_mayor(this)" id="v6_email" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v7_email" value="1" onclick="insertar_input_menor(this)" id="v7_email" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v8_email" value="1" onclick="insertar_input_valor_valido(this)" id="v8_email" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v9_email" value="1" onclick="insertar_input_fecha_mayor(this)" id="v9_email" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v10_email" value="1" onclick="insertar_input_fecha_menor(this)" id="v10_email" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v11_email" value="1"></center></td>
                            <td><center><input type="checkbox" name="v12_email" value="1"></center></td>
                            <td><center><input type="checkbox" name="v13_email" value="1"></center></td>
                            <td><center><input type="checkbox" name="v16_email" value="1" onclick="insertar_select_padre_hijo(this)" id="v16_email" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v17_email" value="1"></center></td>
                            <td><center><input type="checkbox" name="v18_email" value="1"></center></td>
                            <td><center><input type="checkbox" name="v19_email" value="1"></center></td>


                        </tr>

                        <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row">Genero</th>
                            <td><center><input type="checkbox" name="v1_genero" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_genero" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_genero" value="1"></center></td>
                            <td><center><input type="checkbox" name="v4_genero" value="1"></center></td>

                            <td><center><input type="checkbox" name="v6_genero" value="1" onclick="insertar_input_mayor(this)" id="v6_genero" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v7_genero" value="1" onclick="insertar_input_menor(this)" id="v7_genero" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v8_genero" value="1" onclick="insertar_input_valor_valido(this)" id="v8_genero" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v9_genero" value="1" onclick="insertar_input_fecha_mayor(this)" id="v9_genero" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v10_genero" value="1" onclick="insertar_input_fecha_menor(this)" id="v10_genero" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v11_genero" value="1"></center></td>
                            <td><center><input type="checkbox" name="v12_genero" value="1"></center></td>
                            <td><center><input type="checkbox" name="v13_genero" value="1"></center></td>
                            <td><center><input type="checkbox" name="v16_genero" value="1" onclick="insertar_select_padre_hijo(this)" id="v16_genero" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v17_genero" value="1"></center></td>
                            <td><center><input type="checkbox" name="v18_genero" value="1"></center></td>
                            <td><center><input type="checkbox" name="v19_genero" value="1"></center></td>


                        </tr>

                        <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row">Fecha de Nacimiento</th>
                            <td><center><input type="checkbox" name="v1_fec_nac" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_fec_nac" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_fec_nac" value="1"></center></td>
                            <td><center><input type="checkbox" name="v4_fec_nac" value="1"></center></td>

                            <td><center><input type="checkbox" name="v6_fec_nac" value="1" onclick="insertar_input_mayor(this)" id="v6_fec_nac" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v7_fec_nac" value="1" onclick="insertar_input_menor(this)" id="v7_fec_nac" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v8_fec_nac" value="1" onclick="insertar_input_valor_valido(this)" id="v8_fec_nac" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v9_fec_nac" value="1" onclick="insertar_input_fecha_mayor(this)" id="v9_fec_nac" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v10_fec_nac" value="1" onclick="insertar_input_fecha_menor(this)" id="v10_fec_nac" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v11_fec_nac" value="1"></center></td>
                            <td><center><input type="checkbox" name="v12_fec_nac" value="1"></center></td>
                            <td><center><input type="checkbox" name="v13_fec_nac" value="1"></center></td>
                            <td><center><input type="checkbox" name="v16_fec_nac" value="1" onclick="insertar_select_padre_hijo(this)" id="v16_fec_nac" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v17_fec_nac" value="1"></center></td>
                            <td><center><input type="checkbox" name="v18_fec_nac" value="1"></center></td>
                            <td><center><input type="checkbox" name="v19_fec_nac" value="1"></center></td>
                        </tr>

                        <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row">Fecha de ingreso</th>
                            <td><center><input type="checkbox" name="v1_fec_ing" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_fec_ing" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_fec_ing" value="1"></center></td>
                            <td><center><input type="checkbox" name="v4_fec_ing" value="1"></center></td>

                            <td><center><input type="checkbox" name="v6_fec_ing" value="1" onclick="insertar_input_mayor(this)" id="v6_fec_ing" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v7_fec_ing" value="1" onclick="insertar_input_menor(this)" id="v7_fec_ing" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v8_fec_ing" value="1" onclick="insertar_input_valor_valido(this)" id="v8_fec_ing" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v9_fec_ing" value="1" onclick="insertar_input_fecha_mayor(this)" id="v9_fec_ing" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v10_fec_ing" value="1" onclick="insertar_input_fecha_menor(this)" id="v10_fec_ing" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v11_fec_ing" value="1"></center></td>
                            <td><center><input type="checkbox" name="v12_fec_ing" value="1"></center></td>
                            <td><center><input type="checkbox" name="v13_fec_ing" value="1"></center></td>
                            <td><center><input type="checkbox" name="v16_fec_ing" value="1" onclick="insertar_select_padre_hijo(this)" id="v16_fec_ing" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v17_fec_ing" value="1"></center></td>
                            <td><center><input type="checkbox" name="v18_fec_ing" value="1"></center></td>
                            <td><center><input type="checkbox" name="v19_fec_ing" value="1"></center></td>


                        </tr>

                        <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row">Antiguedad</th>
                            <td><center><input type="checkbox" name="v1_antiguedad" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_antiguedad" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_antiguedad" value="1"></center></td>
                            <td><center><input type="checkbox" name="v4_antiguedad" value="1"></center></td>

                            <td><center><input type="checkbox" name="v6_antiguedad" value="1" onclick="insertar_input_mayor(this)" id="v6_antiguedad" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v7_antiguedad" value="1" onclick="insertar_input_menor(this)" id="v7_antiguedad" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v8_antiguedad" value="1" onclick="insertar_input_valor_valido(this)" id="v8_antiguedad" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v9_antiguedad" value="1" onclick="insertar_input_fecha_mayor(this)" id="v9_antiguedad" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v10_antiguedad" value="1" onclick="insertar_input_fecha_menor(this)" id="v10_antiguedad" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v11_antiguedad" value="1"></center></td>
                            <td><center><input type="checkbox" name="v12_antiguedad" value="1"></center></td>
                            <td><center><input type="checkbox" name="v13_antiguedad" value="1"></center></td>
                            <td><center><input type="checkbox" name="v16_antiguedad" value="1" onclick="insertar_select_padre_hijo(this)" id="v16_antiguedad" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v17_antiguedad" value="1"></center></td>
                            <td><center><input type="checkbox" name="v18_antiguedad" value="1"></center></td>
                            <td><center><input type="checkbox" name="v19_antiguedad" value="1"></center></td>
                        </tr>

                        <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row">Edad</th>
                            <td><center><input type="checkbox" name="v1_edad" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_edad" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_edad" value="1"></center></td>
                            <td><center><input type="checkbox" name="v4_edad" value="1"></center></td>

                            <td><center><input type="checkbox" name="v6_edad" value="1" onclick="insertar_input_mayor(this)" id="v6_edad" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v7_edad" value="1" onclick="insertar_input_menor(this)" id="v7_edad" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v8_edad" value="1" onclick="insertar_input_valor_valido(this)" id="v8_edad" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v9_edad" value="1" onclick="insertar_input_fecha_mayor(this)" id="v9_edad" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v10_edad" value="1" onclick="insertar_input_fecha_menor(this)" id="v10_edad" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v11_edad" value="1"></center></td>
                            <td><center><input type="checkbox" name="v12_edad" value="1"></center></td>
                            <td><center><input type="checkbox" name="v13_edad" value="1"></center></td>
                            <td><center><input type="checkbox" name="v16_edad" value="1" onclick="insertar_select_padre_hijo(this)" id="v16_edad" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v17_edad" value="1"></center></td>
                            <td><center><input type="checkbox" name="v18_edad" value="1"></center></td>
                            <td><center><input type="checkbox" name="v19_edad" value="1"></center></td>
                        </tr>

                        <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row">Edad Categorizada</th>
                            <td><center><input type="checkbox" name="v1_edad_cat" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_edad_cat" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_edad_cat" value="1"></center></td>
                            <td><center><input type="checkbox" name="v4_edad_cat" value="1"></center></td>

                            <td><center><input type="checkbox" name="v6_edad_cat" value="1" onclick="insertar_input_mayor(this)" id="v6_edad_cat" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v7_edad_cat" value="1" onclick="insertar_input_menor(this)" id="v7_edad_cat" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v8_edad_cat" value="1" onclick="insertar_input_valor_valido(this)" id="v8_edad_cat" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v9_edad_cat" value="1" onclick="insertar_input_fecha_mayor(this)" id="v9_edad_cat" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v10_edad_cat" value="1" onclick="insertar_input_fecha_menor(this)" id="v10_edad_cat" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v11_edad_cat" value="1"></center></td>
                            <td><center><input type="checkbox" name="v12_edad_cat" value="1"></center></td>
                            <td><center><input type="checkbox" name="v13_edad_cat" value="1"></center></td>
                            <td><center><input type="checkbox" name="v16_edad_cat" value="1" onclick="insertar_select_padre_hijo(this)" id="v16_edad_cat" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v17_edad_cat" value="1"></center></td>
                            <td><center><input type="checkbox" name="v18_edad_cat" value="1"></center></td>
                            <td><center><input type="checkbox" name="v19_edad_cat" value="1"></center></td>
                        </tr>

                        <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row">Generación</th>
                            <td><center><input type="checkbox" name="v1_generacion" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_generacion" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_generacion" value="1"></center></td>
                            <td><center><input type="checkbox" name="v4_generacion" value="1"></center></td>

                            <td><center><input type="checkbox" name="v6_generacion" value="1" onclick="insertar_input_mayor(this)" id="v6_generacion" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v7_generacion" value="1" onclick="insertar_input_menor(this)" id="v7_generacion" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v8_generacion" value="1" onclick="insertar_input_valor_valido(this)" id="v8_generacion" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v9_generacion" value="1" onclick="insertar_input_fecha_mayor(this)" id="v9_generacion" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v10_generacion" value="1" onclick="insertar_input_fecha_menor(this)" id="v10_generacion" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v11_generacion" value="1"></center></td>
                            <td><center><input type="checkbox" name="v12_generacion" value="1"></center></td>
                            <td><center><input type="checkbox" name="v13_generacion" value="1"></center></td>
                            <td><center><input type="checkbox" name="v16_generacion" value="1" onclick="insertar_select_padre_hijo(this)" id="v16_generacion" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v17_generacion" value="1"></center></td>
                            <td><center><input type="checkbox" name="v18_generacion" value="1"></center></td>
                            <td><center><input type="checkbox" name="v19_generacion" value="1"></center></td>
                        </tr>

                        <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row">Antiguedad Categorizada</th>
                            <td><center><input type="checkbox" name="v1_ant_cat" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_ant_cat" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_ant_cat" value="1"></center></td>
                            <td><center><input type="checkbox" name="v4_ant_cat" value="1"></center></td>

                            <td><center><input type="checkbox" name="v6_ant_cat" value="1" onclick="insertar_input_mayor(this)" id="v6_ant_cat" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v7_ant_cat" value="1" onclick="insertar_input_menor(this)" id="v7_ant_cat" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v8_ant_cat" value="1" onclick="insertar_input_valor_valido(this)" id="v8_ant_cat" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v9_ant_cat" value="1" onclick="insertar_input_fecha_mayor(this)" id="v9_ant_cat" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v10_ant_cat" value="1" onclick="insertar_input_fecha_menor(this)" id="v10_ant_cat" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v11_ant_cat" value="1"></center></td>
                            <td><center><input type="checkbox" name="v12_ant_cat" value="1"></center></td>
                            <td><center><input type="checkbox" name="v13_ant_cat" value="1"></center></td>
                            <td><center><input type="checkbox" name="v16_ant_cat" value="1" onclick="insertar_select_padre_hijo(this)" id="v16_ant_cat" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v17_ant_cat" value="1"></center></td>
                            <td><center><input type="checkbox" name="v18_ant_cat" value="1"></center></td>
                            <td><center><input type="checkbox" name="v19_ant_cat" value="1"></center></td>
                        </tr>

                        <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row">Antiguedad menor a 1 año</th>
                            <td><center><input type="checkbox" name="v1_ant_menor" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_ant_menor" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_ant_menor" value="1"></center></td>
                            <td><center><input type="checkbox" name="v4_ant_menor" value="1"></center></td>

                            <td><center><input type="checkbox" name="v6_ant_menor" value="1" onclick="insertar_input_mayor(this)" id="v6_ant_menor" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v7_ant_menor" value="1" onclick="insertar_input_menor(this)" id="v7_ant_menor" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v8_ant_menor" value="1" onclick="insertar_input_valor_valido(this)" id="v8_ant_menor" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v9_ant_menor" value="1" onclick="insertar_input_fecha_mayor(this)" id="v9_ant_menor" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v10_ant_menor" value="1" onclick="insertar_input_fecha_menor(this)" id="v10_ant_menor" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v11_ant_menor" value="1"></center></td>
                            <td><center><input type="checkbox" name="v12_ant_menor" value="1"></center></td>
                            <td><center><input type="checkbox" name="v13_ant_menor" value="1"></center></td>
                            <td><center><input type="checkbox" name="v16_ant_menor" value="1" onclick="insertar_select_padre_hijo(this)" id="v16_ant_menor" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v17_ant_menor" value="1"></center></td>
                            <td><center><input type="checkbox" name="v18_ant_menor" value="1"></center></td>
                            <td><center><input type="checkbox" name="v19_ant_menor" value="1"></center></td>
                        </tr>

                        <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row">Tipo de Cargo</th>
                            <td><center><input type="checkbox" name="v1_t_cargo" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_t_cargo" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_t_cargo" value="1"></center></td>
                            <td><center><input type="checkbox" name="v4_t_cargo" value="1"></center></td>

                            <td><center><input type="checkbox" name="v6_t_cargo" value="1" onclick="insertar_input_mayor(this)" id="v6_t_cargo" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v7_t_cargo" value="1" onclick="insertar_input_menor(this)" id="v7_t_cargo" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v8_t_cargo" value="1" onclick="insertar_input_valor_valido(this)" id="v8_t_cargo" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v9_t_cargo" value="1" onclick="insertar_input_fecha_mayor(this)" id="v9_t_cargo" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v10_t_cargo" value="1" onclick="insertar_input_fecha_menor(this)" id="v10_t_cargo" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v11_t_cargo" value="1"></center></td>
                            <td><center><input type="checkbox" name="v12_t_cargo" value="1"></center></td>
                            <td><center><input type="checkbox" name="v13_t_cargo" value="1"></center></td>
                            <td><center><input type="checkbox" name="v16_t_cargo" value="1" onclick="insertar_select_padre_hijo(this)" id="v16_t_cargo" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v17_t_cargo" value="1"></center></td>
                            <td><center><input type="checkbox" name="v18_t_cargo" value="1"></center></td>
                            <td><center><input type="checkbox" name="v19_t_cargo" value="1"></center></td>
                        </tr>

                         <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row"> Cargo</th>
                            <td><center><input type="checkbox" name="v1_cargo" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_cargo" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_cargo" value="1"></center></td>
                            <td><center><input type="checkbox" name="v4_cargo" value="1"></center></td>

                            <td><center><input type="checkbox" name="v6_cargo" value="1" onclick="insertar_input_mayor(this)" id="v6_cargo" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v7_cargo" value="1" onclick="insertar_input_menor(this)" id="v7_cargo" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v8_cargo" value="1" onclick="insertar_input_valor_valido(this)" id="v8_cargo" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v9_cargo" value="1" onclick="insertar_input_fecha_mayor(this)" id="v9_cargo" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v10_cargo" value="1" onclick="insertar_input_fecha_menor(this)" id="v10_cargo" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v11_cargo" value="1"></center></td>
                            <td><center><input type="checkbox" name="v12_cargo" value="1"></center></td>
                            <td><center><input type="checkbox" name="v13_cargo" value="1"></center></td>
                            <td><center><input type="checkbox" name="v16_cargo" value="1" onclick="insertar_select_padre_hijo(this)" id="v16_cargo" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v17_cargo" value="1"></center></td>
                            <td><center><input type="checkbox" name="v18_cargo" value="1"></center></td>
                            <td><center><input type="checkbox" name="v19_cargo" value="1"></center></td>
                        </tr>

                        <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row"> Cargo 2</th>
                            <td><center><input type="checkbox" name="v1_cargo2" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_cargo2" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_cargo2" value="1"></center></td>
                            <td><center><input type="checkbox" name="v4_cargo2" value="1"></center></td>

                            <td><center><input type="checkbox" name="v6_cargo2" value="1" onclick="insertar_input_mayor(this)" id="v6_cargo2" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v7_cargo2" value="1" onclick="insertar_input_menor(this)" id="v7_cargo2" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v8_cargo2" value="1" onclick="insertar_input_valor_valido(this)" id="v8_cargo2" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v9_cargo2" value="1" onclick="insertar_input_fecha_mayor(this)" id="v9_cargo2" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v10_cargo2" value="1" onclick="insertar_input_fecha_menor(this)" id="v10_cargo2" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v11_cargo2" value="1"></center></td>
                            <td><center><input type="checkbox" name="v12_cargo2" value="1"></center></td>
                            <td><center><input type="checkbox" name="v13_cargo2" value="1"></center></td>
                            <td><center><input type="checkbox" name="v16_cargo2" value="1" onclick="insertar_select_padre_hijo(this)" id="v16_cargo2" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v17_cargo2" value="1"></center></td>
                            <td><center><input type="checkbox" name="v18_cargo2" value="1"></center></td>
                            <td><center><input type="checkbox" name="v19_cargo2" value="1"></center></td>
                        </tr>

                        <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row"> Nivel Educacional</th>
                            <td><center><input type="checkbox" name="v1_niv_educ" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_niv_educ" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_niv_educ" value="1"></center></td>
                            <td><center><input type="checkbox" name="v4_niv_educ" value="1"></center></td>

                            <td><center><input type="checkbox" name="v6_niv_educ" value="1" onclick="insertar_input_mayor(this)" id="v6_niv_educ" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v7_niv_educ" value="1" onclick="insertar_input_menor(this)" id="v7_niv_educ" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v8_niv_educ" value="1" onclick="insertar_input_valor_valido(this)" id="v8_niv_educ" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v9_niv_educ" value="1" onclick="insertar_input_fecha_mayor(this)" id="v9_niv_educ" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v10_niv_educ" value="1" onclick="insertar_input_fecha_menor(this)" id="v10_niv_educ" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v11_niv_educ" value="1"></center></td>
                            <td><center><input type="checkbox" name="v12_niv_educ" value="1"></center></td>
                            <td><center><input type="checkbox" name="v13_niv_educ" value="1"></center></td>
                            <td><center><input type="checkbox" name="v16_niv_educ" value="1" onclick="insertar_select_padre_hijo(this)" id="v16_niv_educ" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v17_niv_educ" value="1"></center></td>
                            <td><center><input type="checkbox" name="v18_niv_educ" value="1"></center></td>
                            <td><center><input type="checkbox" name="v19_niv_educ" value="1"></center></td>
                        </tr>


                        <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row"> Región</th>
                            <td><center><input type="checkbox" name="v1_region" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_region" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_region" value="1"></center></td>
                            <td><center><input type="checkbox" name="v4_region" value="1"></center></td>

                            <td><center><input type="checkbox" name="v6_region" value="1" onclick="insertar_input_mayor(this)" id="v6_region" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v7_region" value="1" onclick="insertar_input_menor(this)" id="v7_region" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v8_region" value="1" onclick="insertar_input_valor_valido(this)" id="v8_region" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v9_region" value="1" onclick="insertar_input_fecha_mayor(this)" id="v9_region" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v10_region" value="1" onclick="insertar_input_fecha_menor(this)" id="v10_region" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v11_region" value="1"></center></td>
                            <td><center><input type="checkbox" name="v12_region" value="1"></center></td>
                            <td><center><input type="checkbox" name="v13_region" value="1"></center></td>
                            <td><center><input type="checkbox" name="v16_region" value="1" onclick="insertar_select_padre_hijo(this)" id="v16_region" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v17_region" value="1"></center></td>
                            <td><center><input type="checkbox" name="v18_region" value="1"></center></td>
                            <td><center><input type="checkbox" name="v19_region" value="1"></center></td>
                        </tr>

                           <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row"> Ciudad</th>
                            <td><center><input type="checkbox" name="v1_ciudad" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_ciudad" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_ciudad" value="1"></center></td>
                            <td><center><input type="checkbox" name="v4_ciudad" value="1"></center></td>

                            <td><center><input type="checkbox" name="v6_ciudad" value="1" onclick="insertar_input_mayor(this)" id="v6_ciudad" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v7_ciudad" value="1" onclick="insertar_input_menor(this)" id="v7_ciudad" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v8_ciudad" value="1" onclick="insertar_input_valor_valido(this)" id="v8_ciudad" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v9_ciudad" value="1" onclick="insertar_input_fecha_mayor(this)" id="v9_ciudad" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v10_ciudad" value="1" onclick="insertar_input_fecha_menor(this)" id="v10_ciudad" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v11_ciudad" value="1"></center></td>
                            <td><center><input type="checkbox" name="v12_ciudad" value="1"></center></td>
                            <td><center><input type="checkbox" name="v13_ciudad" value="1"></center></td>
                            <td><center><input type="checkbox" name="v16_ciudad" value="1" onclick="insertar_select_padre_hijo(this)" id="v16_ciudad" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v17_ciudad" value="1"></center></td>
                            <td><center><input type="checkbox" name="v18_ciudad" value="1"></center></td>
                            <td><center><input type="checkbox" name="v19_ciudad" value="1"></center></td>
                        </tr>

                          <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row"> Es Jefe</th>
                            <td><center><input type="checkbox" name="v1_es_jefe" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_es_jefe" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_es_jefe" value="1"></center></td>
                            <td><center><input type="checkbox" name="v4_es_jefe" value="1"></center></td>

                            <td><center><input type="checkbox" name="v6_es_jefe" value="1" onclick="insertar_input_mayor(this)" id="v6_es_jefe" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v7_es_jefe" value="1" onclick="insertar_input_menor(this)" id="v7_es_jefe" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v8_es_jefe" value="1" onclick="insertar_input_valor_valido(this)" id="v8_es_jefe" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v9_es_jefe" value="1" onclick="insertar_input_fecha_mayor(this)" id="v9_es_jefe" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v10_es_jefe" value="1" onclick="insertar_input_fecha_menor(this)" id="v10_es_jefe" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v11_es_jefe" value="1"></center></td>
                            <td><center><input type="checkbox" name="v12_es_jefe" value="1"></center></td>
                            <td><center><input type="checkbox" name="v13_es_jefe" value="1"></center></td>
                            <td><center><input type="checkbox" name="v16_es_jefe" value="1" onclick="insertar_select_padre_hijo(this)" id="v16_es_jefe" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v17_es_jefe" value="1"></center></td>
                            <td><center><input type="checkbox" name="v18_es_jefe" value="1"></center></td>
                            <td><center><input type="checkbox" name="v19_es_jefe" value="1"></center></td>
                        </tr>

                          <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row"> ID Jefe</th>
                            <td><center><input type="checkbox" name="v1_id_jefe" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_id_jefe" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_id_jefe" value="1"></center></td>
                            <td><center><input type="checkbox" name="v4_id_jefe" value="1"></center></td>

                            <td><center><input type="checkbox" name="v6_id_jefe" value="1" onclick="insertar_input_mayor(this)" id="v6_id_jefe" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v7_id_jefe" value="1" onclick="insertar_input_menor(this)" id="v7_id_jefe" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v8_id_jefe" value="1" onclick="insertar_input_valor_valido(this)" id="v8_es_jefe" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v9_id_jefe" value="1" onclick="insertar_input_fecha_mayor(this)" id="v9_id_jefe" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v10_id_jefe" value="1" onclick="insertar_input_fecha_menor(this)" id="v10_id_jefe" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v11_id_jefe" value="1"></center></td>
                            <td><center><input type="checkbox" name="v12_id_jefe" value="1"></center></td>
                            <td><center><input type="checkbox" name="v13_id_jefe" value="1"></center></td>
                            <td><center><input type="checkbox" name="v16_id_jefe" value="1" onclick="insertar_select_padre_hijo(this)" id="v16_id_jefe" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v17_id_jefe" value="1"></center></td>
                            <td><center><input type="checkbox" name="v18_id_jefe" value="1"></center></td>
                            <td><center><input type="checkbox" name="v19_id_jefe" value="1"></center></td>
                        </tr>

                          <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row"> Cat Jefe</th>
                            <td><center><input type="checkbox" name="v1_cat_jefe" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_cat_jefe" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_cat_jefe" value="1"></center></td>
                            <td><center><input type="checkbox" name="v4_cat_jefe" value="1"></center></td>

                            <td><center><input type="checkbox" name="v6_cat_jefe" value="1" onclick="insertar_input_mayor(this)" id="v6_cat_jefe" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v7_cat_jefe" value="1" onclick="insertar_input_menor(this)" id="v7_cat_jefe" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v8_id_jefe" value="1" onclick="insertar_input_valor_valido(this)" id="v8_cat_jefe" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v9_cat_jefe" value="1" onclick="insertar_input_fecha_mayor(this)" id="v9_cat_jefe" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v10_cat_jefe" value="1" onclick="insertar_input_fecha_menor(this)" id="v10_cat_jefe" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v11_cat_jefe" value="1"></center></td>
                            <td><center><input type="checkbox" name="v12_cat_jefe" value="1"></center></td>
                            <td><center><input type="checkbox" name="v13_cat_jefe" value="1"></center></td>
                            <td><center><input type="checkbox" name="v16_cat_jefe" value="1" onclick="insertar_select_padre_hijo(this)" id="v16_cat_jefe" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v17_cat_jefe" value="1"></center></td>
                            <td><center><input type="checkbox" name="v18_cat_jefe" value="1"></center></td>
                            <td><center><input type="checkbox" name="v19_cat_jefe" value="1"></center></td>
                        </tr>

                          <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row"> N1</th>
                            <td><center><input type="checkbox" name="v1_n1" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_n1" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_n1" value="1"></center></td>
                            <td><center><input type="checkbox" name="v4_n1" value="1"></center></td>

                            <td><center><input type="checkbox" name="v6_n1" value="1" onclick="insertar_input_mayor(this)" id="v6_n1" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v7_n1" value="1" onclick="insertar_input_menor(this)" id="v7_n1" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v8_n1" value="1" onclick="insertar_input_valor_valido(this)" id="v8_n1" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v9_n1" value="1" onclick="insertar_input_fecha_mayor(this)" id="v9_n1" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v10_n1" value="1" onclick="insertar_input_fecha_menor(this)" id="v10_n1" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v11_n1" value="1"></center></td>
                            <td><center><input type="checkbox" name="v12_n1" value="1"></center></td>
                            <td><center><input type="checkbox" name="v13_n1" value="1"></center></td>
                            <td><center><input type="checkbox" name="v16_n1" value="1" onclick="insertar_select_padre_hijo(this)" id="v16_n1" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v17_n1" value="1"></center></td>
                            <td><center><input type="checkbox" name="v18_n1" value="1"></center></td>
                            <td><center><input type="checkbox" name="v19_n1" value="1"></center></td>
                        </tr>

                        <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row"> N2</th>
                            <td><center><input type="checkbox" name="v1_n2" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_n2" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_n2" value="1"></center></td>
                            <td><center><input type="checkbox" name="v4_n2" value="1"></center></td>

                            <td><center><input type="checkbox" name="v6_n2" value="1" onclick="insertar_input_mayor(this)" id="v6_n2" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v7_n2" value="1" onclick="insertar_input_menor(this)" id="v7_n2" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v8_n2" value="1" onclick="insertar_input_valor_valido(this)" id="v8_n2" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v9_n2" value="1" onclick="insertar_input_fecha_mayor(this)" id="v9_n2" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v10_n2" value="1" onclick="insertar_input_fecha_menor(this)" id="v10_n2" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v11_n2" value="1"></center></td>
                            <td><center><input type="checkbox" name="v12_n2" value="1"></center></td>
                            <td><center><input type="checkbox" name="v13_n2" value="1"></center></td>
                            <td><center><input type="checkbox" name="v16_n2" value="1" onclick="insertar_select_padre_hijo(this)" id="v16_n2" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v17_n2" value="1"></center></td>
                            <td><center><input type="checkbox" name="v18_n2" value="1"></center></td>
                            <td><center><input type="checkbox" name="v19_n2" value="1"></center></td>
                        </tr>

                        <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row"> N3</th>
                            <td><center><input type="checkbox" name="v1_n3" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_n3" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_n3" value="1"></center></td>
                            <td><center><input type="checkbox" name="v4_n3" value="1"></center></td>

                            <td><center><input type="checkbox" name="v6_n3" value="1" onclick="insertar_input_mayor(this)" id="v6_n3" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v7_n3" value="1" onclick="insertar_input_menor(this)" id="v7_n3" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v8_n3" value="1" onclick="insertar_input_valor_valido(this)" id="v8_n3" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v9_n3" value="1" onclick="insertar_input_fecha_mayor(this)" id="v9_n3" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v10_n3" value="1" onclick="insertar_input_fecha_menor(this)" id="v10_n3" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v11_n3" value="1"></center></td>
                            <td><center><input type="checkbox" name="v12_n3" value="1"></center></td>
                            <td><center><input type="checkbox" name="v13_n3" value="1"></center></td>
                            <td><center><input type="checkbox" name="v16_n3" value="1" onclick="insertar_select_padre_hijo(this)" id="v16_n3" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v17_n3" value="1"></center></td>
                            <td><center><input type="checkbox" name="v18_n3" value="1"></center></td>
                            <td><center><input type="checkbox" name="v19_n3" value="1"></center></td>
                        </tr>

                        <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row"> N4</th>
                            <td><center><input type="checkbox" name="v1_n4" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_n4" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_n4" value="1"></center></td>
                            <td><center><input type="checkbox" name="v4_n4" value="1"></center></td>

                            <td><center><input type="checkbox" name="v6_n4" value="1" onclick="insertar_input_mayor(this)" id="v6_n4" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v7_n4" value="1" onclick="insertar_input_menor(this)" id="v7_n4" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v8_n4" value="1" onclick="insertar_input_valor_valido(this)" id="v8_n4" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v9_n4" value="1" onclick="insertar_input_fecha_mayor(this)" id="v9_n4" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v10_n4" value="1" onclick="insertar_input_fecha_menor(this)" id="v10_n4" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v11_n4" value="1"></center></td>
                            <td><center><input type="checkbox" name="v12_n4" value="1"></center></td>
                            <td><center><input type="checkbox" name="v13_n4" value="1"></center></td>
                            <td><center><input type="checkbox" name="v16_n4" value="1" onclick="insertar_select_padre_hijo(this)" id="v16_n4" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v17_n4" value="1"></center></td>
                            <td><center><input type="checkbox" name="v18_n4" value="1"></center></td>
                            <td><center><input type="checkbox" name="v19_n4" value="1"></center></td>
                        </tr>

                        <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row"> N5</th>
                            <td><center><input type="checkbox" name="v1_n5" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_n5" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_n5" value="1"></center></td>
                            <td><center><input type="checkbox" name="v4_n5" value="1"></center></td>

                            <td><center><input type="checkbox" name="v6_n5" value="1" onclick="insertar_input_mayor(this)" id="v6_n5" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v7_n5" value="1" onclick="insertar_input_menor(this)" id="v7_n5" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v8_n5" value="1" onclick="insertar_input_valor_valido(this)" id="v8_n5" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v9_n5" value="1" onclick="insertar_input_fecha_mayor(this)" id="v9_n5" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v10_n5" value="1" onclick="insertar_input_fecha_menor(this)" id="v10_n5" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v11_n5" value="1"></center></td>
                            <td><center><input type="checkbox" name="v12_n5" value="1"></center></td>
                            <td><center><input type="checkbox" name="v13_n5" value="1"></center></td>
                            <td><center><input type="checkbox" name="v16_n5" value="1" onclick="insertar_select_padre_hijo(this)" id="v16_n5" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v17_n5" value="1"></center></td>
                            <td><center><input type="checkbox" name="v18_n5" value="1"></center></td>
                            <td><center><input type="checkbox" name="v19_n5" value="1"></center></td>
                        </tr>

                        <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row"> Segmento add 1</th>
                            <td><center><input type="checkbox" name="v1_seg_add_1" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_seg_add_1" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_seg_add_1" value="1"></center></td>
                            <td><center><input type="checkbox" name="v4_seg_add_1" value="1"></center></td>

                            <td><center><input type="checkbox" name="v6_seg_add_1" value="1" onclick="insertar_input_mayor(this)" id="v6_seg_add_1" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v7_seg_add_1" value="1" onclick="insertar_input_menor(this)" id="v7_seg_add_1" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v8_seg_add_1" value="1" onclick="insertar_input_valor_valido(this)" id="v8_seg_add_1" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v9_seg_add_1" value="1" onclick="insertar_input_fecha_mayor(this)" id="v9_seg_add_1" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v10_seg_add_1" value="1" onclick="insertar_input_fecha_menor(this)" id="v10_seg_add_1" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v11_seg_add_1" value="1"></center></td>
                            <td><center><input type="checkbox" name="v12_seg_add_1" value="1"></center></td>
                            <td><center><input type="checkbox" name="v13_seg_add_1" value="1"></center></td>
                            <td><center><input type="checkbox" name="v16_seg_add_1" value="1" onclick="insertar_select_padre_hijo(this)" id="v16_seg_add_1" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v17_seg_add_1" value="1"></center></td>
                            <td><center><input type="checkbox" name="v18_seg_add_1" value="1"></center></td>
                            <td><center><input type="checkbox" name="v19_seg_add_1" value="1"></center></td>
                        </tr>

                       <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row"> Segmento add 2</th>
                            <td><center><input type="checkbox" name="v1_seg_add_2" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_seg_add_2" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_seg_add_2" value="1"></center></td>
                            <td><center><input type="checkbox" name="v4_seg_add_2" value="1"></center></td>

                            <td><center><input type="checkbox" name="v6_seg_add_2" value="1" onclick="insertar_input_mayor(this)" id="v6_seg_add_2" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v7_seg_add_2" value="1" onclick="insertar_input_menor(this)" id="v7_seg_add_2" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v8_seg_add_2" value="1" onclick="insertar_input_valor_valido(this)" id="v8_seg_add_2" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v9_seg_add_2" value="1" onclick="insertar_input_fecha_mayor(this)" id="v9_seg_add_2" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v10_seg_add_2" value="1" onclick="insertar_input_fecha_menor(this)" id="v10_seg_add_2" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v11_seg_add_2" value="1"></center></td>
                            <td><center><input type="checkbox" name="v12_seg_add_2" value="1"></center></td>
                            <td><center><input type="checkbox" name="v13_seg_add_2" value="1"></center></td>
                            <td><center><input type="checkbox" name="v16_seg_add_2" value="1" onclick="insertar_select_padre_hijo(this)" id="v16_seg_add_2" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v17_seg_add_2" value="1"></center></td>
                            <td><center><input type="checkbox" name="v18_seg_add_2" value="1"></center></td>
                            <td><center><input type="checkbox" name="v19_seg_add_2" value="1"></center></td>
                        </tr>

                       <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row"> Segmento add 3</th>
                            <td><center><input type="checkbox" name="v1_seg_add_3" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_seg_add_3" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_seg_add_3" value="1"></center></td>
                            <td><center><input type="checkbox" name="v4_seg_add_3" value="1"></center></td>

                            <td><center><input type="checkbox" name="v6_seg_add_3" value="1" onclick="insertar_input_mayor(this)" id="v6_seg_add_3" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v7_seg_add_3" value="1" onclick="insertar_input_menor(this)" id="v7_seg_add_3" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v8_seg_add_3" value="1" onclick="insertar_input_valor_valido(this)" id="v8_seg_add_3" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v9_seg_add_3" value="1" onclick="insertar_input_fecha_mayor(this)" id="v9_seg_add_3" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v10_seg_add_3" value="1" onclick="insertar_input_fecha_menor(this)" id="v10_seg_add_3" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v11_seg_add_3" value="1"></center></td>
                            <td><center><input type="checkbox" name="v12_seg_add_3" value="1"></center></td>
                            <td><center><input type="checkbox" name="v13_seg_add_3" value="1"></center></td>
                            <td><center><input type="checkbox" name="v16_seg_add_3" value="1" onclick="insertar_select_padre_hijo(this)" id="v16_seg_add_3" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v17_seg_add_3" value="1"></center></td>
                            <td><center><input type="checkbox" name="v18_seg_add_3" value="1"></center></td>
                            <td><center><input type="checkbox" name="v19_seg_add_3" value="1"></center></td>
                        </tr>

                        <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row"> Desempeño</th>
                            <td><center><input type="checkbox" name="v1_desempenio" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_desempenio" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_desempenio" value="1"></center></td>
                            <td><center><input type="checkbox" name="v4_desempenio" value="1"></center></td>

                            <td><center><input type="checkbox" name="v6_desempenio" value="1" onclick="insertar_input_mayor(this)" id="v6_desempenio" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v7_desempenio" value="1" onclick="insertar_input_menor(this)" id="v7_desempenio" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v8_desempenio" value="1" onclick="insertar_input_valor_valido(this)" id="v8_desempenio" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v9_desempenio" value="1" onclick="insertar_input_fecha_mayor(this)" id="v9_desempenio" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v10_desempenio" value="1" onclick="insertar_input_fecha_menor(this)" id="v10_desempenio" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v11_desempenio" value="1"></center></td>
                            <td><center><input type="checkbox" name="v12_desempenio" value="1"></center></td>
                            <td><center><input type="checkbox" name="v13_desempenio" value="1"></center></td>
                            <td><center><input type="checkbox" name="v16_desempenio" value="1" onclick="insertar_select_padre_hijo(this)" id="v16_desempenio" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v17_desempenio" value="1"></center></td>
                            <td><center><input type="checkbox" name="v18_desempenio" value="1"></center></td>
                            <td><center><input type="checkbox" name="v19_desempenio" value="1"></center></td>
                        </tr>

                        <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row"> Cumplimiento de metas</th>
                            <td><center><input type="checkbox" name="v1_cump_metas" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_cump_metas" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_cump_metas" value="1"></center></td>
                            <td><center><input type="checkbox" name="v4_cump_metas" value="1"></center></td>

                            <td><center><input type="checkbox" name="v6_cump_metas" value="1" onclick="insertar_input_mayor(this)" id="v6_cump_metas" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v7_cump_metas" value="1" onclick="insertar_input_menor(this)" id="v7_cump_metas" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v8_cump_metas" value="1" onclick="insertar_input_valor_valido(this)" id="v8_cump_metas" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v9_cump_metas" value="1" onclick="insertar_input_fecha_mayor(this)" id="v9_cump_metas" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v10_cump_metas" value="1" onclick="insertar_input_fecha_menor(this)" id="v10_cump_metas" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v11_cump_metas" value="1"></center></td>
                            <td><center><input type="checkbox" name="v12_cump_metas" value="1"></center></td>
                            <td><center><input type="checkbox" name="v13_cump_metas" value="1"></center></td>
                            <td><center><input type="checkbox" name="v16_cump_metas" value="1" onclick="insertar_select_padre_hijo(this)" id="v16_cump_metas" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v17_cump_metas" value="1"></center></td>
                            <td><center><input type="checkbox" name="v18_cump_metas" value="1"></center></td>
                            <td><center><input type="checkbox" name="v19_cump_metas" value="1"></center></td>
                        </tr>

                        <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row"> Horas extras</th>
                            <td><center><input type="checkbox" name="v1_hr_extra" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_hr_extra" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_hr_extra" value="1"></center></td>
                            <td><center><input type="checkbox" name="v4_hr_extra" value="1"></center></td>

                            <td><center><input type="checkbox" name="v6_hr_extra" value="1" onclick="insertar_input_mayor(this)" id="v6_hr_extra" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v7_hr_extra" value="1" onclick="insertar_input_menor(this)" id="v7_hr_extra" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v8_hr_extra" value="1" onclick="insertar_input_valor_valido(this)" id="v8_hr_extra" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v9_hr_extra" value="1" onclick="insertar_input_fecha_mayor(this)" id="v9_hr_extra" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v10_hr_extra" value="1" onclick="insertar_input_fecha_menor(this)" id="v10_hr_extra" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v11_hr_extra" value="1"></center></td>
                            <td><center><input type="checkbox" name="v12_hr_extra" value="1"></center></td>
                            <td><center><input type="checkbox" name="v13_hr_extra" value="1"></center></td>
                            <td><center><input type="checkbox" name="v16_hr_extra" value="1" onclick="insertar_select_padre_hijo(this)" id="v16_hr_extra" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v17_hr_extra" value="1"></center></td>
                            <td><center><input type="checkbox" name="v18_hr_extra" value="1"></center></td>
                            <td><center><input type="checkbox" name="v19_hr_extra" value="1"></center></td>
                        </tr>

                        <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row"> Horas Capacitación</th>
                            <td><center><input type="checkbox" name="v1_hr_capaci" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_hr_capaci" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_hr_capaci" value="1"></center></td>
                            <td><center><input type="checkbox" name="v4_hr_capaci" value="1"></center></td>

                            <td><center><input type="checkbox" name="v6_hr_capaci" value="1" onclick="insertar_input_mayor(this)" id="v6_hr_capaci" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v7_hr_capaci" value="1" onclick="insertar_input_menor(this)" id="v7_hr_capaci" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v8_hr_capaci" value="1" onclick="insertar_input_valor_valido(this)" id="v8_hr_capaci" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v9_hr_capaci" value="1" onclick="insertar_input_fecha_mayor(this)" id="v9_hr_capaci" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v10_hr_capaci" value="1" onclick="insertar_input_fecha_menor(this)" id="v10_hr_capaci" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v11_hr_capaci" value="1"></center></td>
                            <td><center><input type="checkbox" name="v12_hr_capaci" value="1"></center></td>
                            <td><center><input type="checkbox" name="v13_hr_capaci" value="1"></center></td>
                            <td><center><input type="checkbox" name="v16_hr_capaci" value="1" onclick="insertar_select_padre_hijo(this)" id="v16_hr_capaci" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v17_hr_capaci" value="1"></center></td>
                            <td><center><input type="checkbox" name="v18_hr_capaci" value="1"></center></td>
                            <td><center><input type="checkbox" name="v19_hr_capaci" value="1"></center></td>
                        </tr>

                        <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row"> Tipo Contrato</th>
                            <td><center><input type="checkbox" name="v1_t_contrato" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_t_contrato" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_t_contrato" value="1"></center></td>
                            <td><center><input type="checkbox" name="v4_t_contrato" value="1"></center></td>

                            <td><center><input type="checkbox" name="v6_t_contrato" value="1" onclick="insertar_input_mayor(this)" id="v6_t_contrato" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v7_t_contrato" value="1" onclick="insertar_input_menor(this)" id="v7_t_contrato" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v8_t_contrato" value="1" onclick="insertar_input_valor_valido(this)" id="v8_t_contrato" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v9_t_contrato" value="1" onclick="insertar_input_fecha_mayor(this)" id="v9_t_contrato" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v10_t_contrato" value="1" onclick="insertar_input_fecha_menor(this)" id="v10_t_contrato" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v11_t_contrato" value="1"></center></td>
                            <td><center><input type="checkbox" name="v12_t_contrato" value="1"></center></td>
                            <td><center><input type="checkbox" name="v13_t_contrato" value="1"></center></td>
                            <td><center><input type="checkbox" name="v16_t_contrato" value="1" onclick="insertar_select_padre_hijo(this)" id="v16_t_contrato" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v17_t_contrato" value="1"></center></td>
                            <td><center><input type="checkbox" name="v18_t_contrato" value="1"></center></td>
                            <td><center><input type="checkbox" name="v19_t_contrato" value="1"></center></td>
                        </tr>

                        <tr>
                            <td><input type="checkbox" checked/></td>
                            <th scope="row"> Tipo remuneración</th>
                            <td><center><input type="checkbox" name="v1_t_remun" value="1"></center></td>
                            <td><center><input type="checkbox" name="v2_t_remun" value="1"></center></td>
                            <td><center><input type="checkbox" name="v3_t_remun" value="1"></center></td>
                            <td><center><input type="checkbox" name="v4_t_remun" value="1"></center></td>

                            <td><center><input type="checkbox" name="v6_t_remun" value="1" onclick="insertar_input_mayor(this)" id="v6_t_remun" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v7_t_remun" value="1" onclick="insertar_input_menor(this)" id="v7_t_remun" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v8_t_remun" value="1" onclick="insertar_input_valor_valido(this)" id="v8_t_remun" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v9_t_remun" value="1" onclick="insertar_input_fecha_mayor(this)" id="v9_t_remun" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v10_t_remun" value="1" onclick="insertar_input_fecha_menor(this)" id="v10_t_remun" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v11_t_remun" value="1"></center></td>
                            <td><center><input type="checkbox" name="v12_t_remun" value="1"></center></td>
                            <td><center><input type="checkbox" name="v13_t_remun" value="1"></center></td>
                            <td><center><input type="checkbox" name="v16_t_remun" value="1" onclick="insertar_select_padre_hijo(this)" id="v16_t_remun" activo="no"></center></td>
                            <td><center><input type="checkbox" name="v17_t_remun" value="1"></center></td>
                            <td><center><input type="checkbox" name="v18_t_remun" value="1"></center></td>
                            <td><center><input type="checkbox" name="v19_t_remun" value="1"></center></td>
                        </tr>


                        </tbody>
                    </table>
                </div>


                <div class="col-lg-2">
                    {!! Form::submit('Guardar Validaciones', ['class' => 'btn btn-primary block full-width m-b']) !!}
                    {!! Form::close() !!}
                </div>
            </div>



        </div>
    </div>

    {!!Html::script('https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js')!!}


    <script>
        $('tr td:first-child input[type="checkbox"]').click( function() {
            //enable/disable all except checkboxes, based on the row is checked or not
            $(this).closest('tr').find(":input:not(:first)").attr('disabled', !this.checked);
        });
    </script>


@stop

@section('javascripts')
    {!!Html::script('js/dropdown.js')!!}
    {!!Html::script('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js')!!}
    <script>
        // In your Javascript (external .js resource or <script> tag)
        $(document).ready(function() {
            $('.basic-single').select2();
        });
    </script>
@endsection








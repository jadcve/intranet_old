@extends('layouts.principal')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Dashboard rubros</h2>
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Formulario</li>
                <li class="active"><strong>rubros</strong></li>
            </ol>
        </div>
    </div>

    @if(Session::has('message'))
    <div class="alert alert-success alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ Session::get('message') }}
    </div>
    @endif

    <div class="row">
    	<div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Crear rubro</h5>
                </div>
                <div class="ibox-content">
    				@include('alerts.request')
    				{!! Form::open(['route'=> 'rubros.store', 'method'=>'POST']) !!}
    					<div class="form-group">
    						{!! Form::label('Nombre del Rubro') !!}
    					    {!! Form::text('rubro', null, ['placeholder'=>'Rubro', 'class'=>'form-control']) !!}
    					</div>
    					<div class="form-group">
    						{!! Form::label('Estatus del Rubro') !!}
    					    {!! Form::select('status', ['1' => 'Activo', '0' => 'Inactivo'], '1',['class' => 'form-control']) !!}
    					</div>
    	            	{!! Form::submit('Registrar', ['class' => 'btn btn-primary block full-width m-b']) !!}				            
    				{!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-lg-7">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Listado de rubros</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-hover no-margins">
                        <thead>
                        <tr>
                            <th>Rubros</th>
                            <th>Estatus</th>
                            <th>Acci&oacute;n</th>
                        </tr>
                        </thead>
                        @foreach($rubros as $rubro)
                        <tbody>
                        <tr>
                            <td><small>{{ $rubro->rubro }}</small></td>
                            <td>@if ( $rubro->status == 1) <div class="alert-success">Activo</div> @else <div class="alert-warning">Inactivo</div> @endif</td>
                            <td>{!! link_to_route('rubros.edit', $title = 'Editar', $parameters = $rubro->id, $attributes = ['class'=>'btn btn-primary']) !!}</td>
                        </tr>
                        </tbody>
                        @endforeach
                    </table>
                    {!! $rubros->render() !!}
                </div>
            </div>
        </div>
    </div>
@stop
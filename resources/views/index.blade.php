@extends('layouts.login')

@section('content')
    @include('alerts.errors')
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <h1 class="logo-name"><img src="img/logo_kpi.png" width="40%"></h1>
            </div>
                <br>
            <p style="font-size: 15px"><b>Bienvenidos</b></p>
            <p>Inicia sesi&oacute;n para ingresar al sistema</p>
			{!! Form::open(['route'=> 'log.store', 'method'=>'POST']) !!}
    			<div class="form-group">
    			     {!! Form::email('email', null, ['placeholder'=>'Email', 'class' => 'form-control']); !!}
    			</div>
    			<div class="form-group">
    			     {{ Form::password('password',array('required' => "required", 'placeholder' => 'Password', 'class' => 'form-control')) }}
    			</div>
            {!! Form::submit('Enviar', ['class' => 'btn btn-primary block full-width m-b']) !!}
			{!! Form::close() !!}

                {!! link_to('auth/password', $title='¿Olvido su contrase&ntilde;a?', $attributes = null, $secure = null) !!}

            <p class="m-t"> <small>Derechos Reservados &copy; 2018</small> </p>
        </div>
    </div>
@stop 
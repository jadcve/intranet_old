
@extends('layouts.principal')
@section('content')
<!--Funcion para ocultar y mostrar input segun seleccion-->
    <script language="javascript" type="text/javascript">
    function d1(selectTag){
     if(selectTag.value == '1'){
        $('#bloque_carousel').show(); 
        $('#bloque_imagen').hide();
        $('#bloque_video').hide();
     document.getElementById('carousel').disabled = false;
     document.getElementById('imagen').disabled = true;
     document.getElementById('video').disabled = true;
     } else if(selectTag.value == '2'){
        $('#bloque_carousel').hide(); 
        $('#bloque_imagen').show();
        $('#bloque_video').hide();
     document.getElementById('carousel').disabled = true;
     document.getElementById('imagen').disabled = false;
     document.getElementById('video').disabled = true;
     } else if(selectTag.value == '3'){
        $('#bloque_carousel').hide(); 
        $('#bloque_imagen').hide();
        $('#bloque_video').show();
     document.getElementById('carousel').disabled = true;
     document.getElementById('imagen').disabled = true;
     document.getElementById('video').disabled = false;
     } else {
        $('#bloque_carousel').hide(); 
        $('#bloque_imagen').hide();
        $('#bloque_video').hide();
     }
    }
    </script>
<!--Fin Funcion para ocultar y mostrar input segun seleccion-->
<!--CCS para ocultar div antes de seleccionar-->
    <style type="text/css">
        .bloque_carousel{
            display:none;
        }
        .bloque_imagen{
            display:none;
        }
        .bloque_video{
            display:none;
        }
    </style>
<!--FIN CCS para ocultar div antes de seleccionar-->

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Dashboard Promociones</h2>
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Formulario</li>
                <li class="active"><strong>Promociones</strong></li>
            </ol>
        </div>
    </div>

    <div class="row">
    	<div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Editar Promoci&oacute;n</h5>
                </div>
                <div class="ibox-content">
    				@include('alerts.request')
    				{!! Form::model($promotion,['route'=> ['promotions.update',$promotion->id], 'method'=>'PUT', 'files' => true]) !!}
    					<div class="form-group">
    						<font color="red">* </font>{!! Form::label('Titulo de la Promoci&oacute;n') !!}
    					    {!! Form::text('titulo', null, ['placeholder'=>'Titulo', 'class'=>'form-control']) !!}
    					</div>
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Texto Corto') !!}
                            {!! Form::text('texto_corto', null, ['placeholder'=>'Texto corto', 'class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Usuario') !!}
                            {!! Form::select('usuario',  $clientes, null,['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Tipo de Promoci&oacute;n') !!}
                            {!! Form::select('tipo', ['0' => 'Seleccionar', '1' => 'Carrusel', '2' => 'Imagen', '3' => 'Video'], $promotion->tipo,['onchange'=>'d1(this)','class' => 'form-control']) !!}
                            <br>
                            <div id="bloque_carousel" class="bloque_carousel">
                                <font color="red">* </font>{!! Form::label('Carousel') !!}
                                  <select name="carousel" id="carousel" disabled="true" class="form-control">
                                    <option selected>Seleccionar</option>
                                      @foreach($carousels as $carousel)
                                          <option value="{{  $carousel }}">{{  $carousel  }}</option>
                                      @endforeach
                                </select>
                            </div>
                            <div id="bloque_imagen" class="bloque_imagen">                              
                                <font color="red">* </font>{!! Form::label('Imagen') !!}
                                <div title="Formato de Imagen: jpg, png"><input type='file' id="imagen" name='imagen' disabled="true" class="form-control"></div>
                            </div>
                            <div id="bloque_video" class="bloque_video">                                   
                                <font color="red">* </font>{!! Form::label('Video Youtube') !!}
                                <input type='text' id="video" name='video' disabled="true" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Mensaje Mail') !!}
                            {!! Form::textarea('mensaje',null, ['class'=>'form-control', 'rows' => 2, 'cols' => 40]) !!}
                        </div> 
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Titulo del Wizard') !!}
                            {!! Form::text('texto_wizard', null, ['placeholder'=>'Titulo', 'class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('URL del Wizard') !!}
                            {!! Form::text('url_wizard', null, ['placeholder'=>'Titulo', 'class'=>'form-control']) !!}
                        </div>                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Estatus del Estudio') !!}
                            {!! Form::select('status', ['1' => 'Activo', '0' => 'Inactivo'], '1',['class' => 'form-control']) !!}
                        </div>
    	            	{!! Form::submit('Actualizar', ['class' => 'btn btn-primary block full-width m-b']) !!}				            
    				{!! Form::close() !!}
                    @if ( Auth::user()->rol == 1) 
                    {!! Form::model($promotion,['route'=> ['promotions.destroy',$promotion->id], 'method'=>'DELETE']) !!}
                        {!! Form::submit('Eliminar', ['onClick'=>"javascript: return confirm('¿Esta seguro que desea eliminar la promoción?');", 'class' => 'btn btn-danger block full-width m-b']) !!}                            
                    {!! Form::close() !!}
                    @endif
                </div>
            </div>
        </div>        
    </div>
@stop
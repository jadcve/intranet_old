@extends('layouts.principal')

@section('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
@endsection

@section('content')
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2>Dashboard Promociones</h2>
	        <ol class="breadcrumb">
	            <li>Home</li>
	            <li>Formulario</li>
	            <li class="active"><strong>Ver Promociones</strong></li>
	        </ol>
	    </div>
	</div>

	@if(Session::has('message'))
	<div class="alert alert-success alert-dismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  {{ Session::get('message') }}
	</div>
	@endif

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Listado de Promociones</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <a href="{!! URL::to('promotions/create') !!}"><button class="btn btn-primary">Crear Promoci&oacute;n</button></a>
                        </a>
                    </div>
                </div>
	            <div class="ibox-content">
					<table id="table_id" class="display">
					    <thead>
							<tr>
								<th>Titulo</th>
								<th>Texto Corto</th>
								<th>Envio de Mail</th>
								<th>Mensaje Mail</th>
								<th>Usuario</th>
								<th>Tipo</th>
								<th>Ver</th>
								<th>Texto Wizard</th>
								<th>Url Wizard</th>
								<th>Estatus</th>
								<th>Acci&oacute;n</th>
							</tr>
						</thead>
						<tbody>
						@foreach($promotions as $promotion)
							<tr>
								<td>{{ $promotion->titulo }}</td>
								<td>{{ $promotion->texto_corto }}</td>
								<td>{{ $promotion->envio_mail }}</td>
								<td>{{ $promotion->mensaje }}</td>
								<td>{{ $promotion->nombre }} {{ $promotion->apellido }}</td>
								<td>@if ( $promotion->tipo == 1) Carousel @elseif ( $promotion->tipo == 2) Imagen @else( $promotion->tipo == 3) Video @endif</td>
								<td>@if ( $promotion->tipo == 1) {{ $promotion->carousel }} @elseif ( $promotion->tipo == 2) <a href="img/carousel/{{ $promotion->imagen }}" target="_blank"><button class="btn btn-primary">Ver Imagen</button></a> @else( $promotion->tipo == 3) <a href="https://www.youtube.com/embed/{{ $promotion->video }}" target="_blank"><button class="btn btn-primary">Ver Video</button></a> @endif</td>
								<td>{{ $promotion->texto_wizard }}</td>
								<td><a href="{{ $promotion->url_wizard }}" target="_blank"><button class="btn btn-primary">Ver</button></a></td>
								<td>@if ( $promotion->status == 1) <p class="text-success">Activo</p> @else <p class="text-danger">Inactivo</p> @endif</td>
								<td>{!! link_to_route('promotions.edit', $title = 'Editar', $parameters = $promotion->id, $attributes = ['class'=>'btn btn-primary']) !!}</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('javascripts')
	  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script>
    
    $(document).ready(function() {
    $('#table_id').DataTable( {
        "lengthMenu": [[100, 50, 20, -1], [100, 50, 20, "Todos"]]
    } );
} );
    </script>
@endsection
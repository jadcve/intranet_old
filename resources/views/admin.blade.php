@extends('layouts.principal')

@section('content')
    <div class="row">
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-success pull-right">KPI</span>
                    <h5>Usuarios</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">{!! $users = DB::table('users')->count(); !!}</h1>
                    <!--<div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div>-->
                    <small>Total Registrados</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-info pull-right">Clientes</span>
                    <h5>Usuarios</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">{!! $clientes = DB::table('clientes')->count(); !!}</h1>
                    <!--<div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div>-->
                    <small>Total Registrados</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-primary pull-right">Registradas</span>
                    <h5>Empresas</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">{!! $empresas = DB::table('empresas')->count(); !!}</h1>
                    <!--<div class="stat-percent font-bold text-navy">44% <i class="fa fa-level-up"></i></div>-->
                    <small>Empresas</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-warning pull-right">Servicios Activos</span>
                    <h5>KPI Analytics</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">{!! $trackin = DB::table('trackin')->count(); !!}</h1>
                    <!--<div class="stat-percent font-bold text-navy">44% <i class="fa fa-level-up"></i></div>-->
                    <small>Usuarios</small>
                </div>
            </div>
        </div>        
    </div>

    <div class="row">
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-success pull-right">Estudios</span>
                    <h5>Estudios</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">{!! $estudios = DB::table('estudios')->count(); !!}</h1>
                    <!--<div class="stat-percent font-bold text-danger"><i class="fa fa-level-down"></i></div>-->
                    <small>Archivos</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-info pull-right">Encuestas</span>
                    <h5>Encuestas</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">{!! $clientes = DB::table('encuestas')->count(); !!}</h1>
                    <!--<div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div>-->
                    <small>Total</small>
                </div>
            </div>
        </div>        
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-primary pull-right">Benchmarks</span>
                    <h5>Benchmarks</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">{!! $benchmarks = DB::table('benchmarks')->count(); !!}</h1>
                    <!--<div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div>-->
                    <small>Total</small>
                </div>
            </div>
        </div>
    </div>
@endsection
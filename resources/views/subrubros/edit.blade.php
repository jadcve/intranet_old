@extends('layouts.principal')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Dashboard Sub-rubros</h2>
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Formulario</li>
                <li class="active"><strong>Editar Sub-rubros</strong></li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">            
        <div class="col-lg-5">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Editar Sub-rubro</h5>
                </div>
                <div class="ibox-content">
                	@include('alerts.request')
    				{!! Form::model($subrubro,['route'=> ['subrubros.update',$subrubro->id], 'method'=>'PUT']) !!}
                        <div class="form-group">
                            {!! Form::label('Rubro') !!}
                            {!! Form::select('rubro', $rubros, null,['class'=>'form-control']) !!}
                        </div>    	    			
                        <div class="form-group">
                            {!! Form::label('Nombre del Sub-rubro') !!}
                            {!! Form::text('subrubro', null, ['placeholder'=>'Sub-rubro', 'class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Estatus del Sub-rubro') !!}
                            {!! Form::select('status', ['1' => 'Activo', '0' => 'Inactivo'], '1',['class' => 'form-control']) !!}
                        </div>
    	            	{!! Form::submit('Actualizar', ['class' => 'btn btn-primary block full-width m-b']) !!}				            
    				{!! Form::close() !!}
    				@if ( Auth::user()->rol == 1)
    				{!! Form::model($subrubro,['route'=> ['subrubros.destroy',$subrubro->id], 'method'=>'DELETE']) !!}
    	    			
    	            	{!! Form::submit('Eliminar', ['class' => 'btn btn-danger block full-width m-b']) !!}				            
    				{!! Form::close() !!}
                    @endif
                </div>
            </div>
        </div>
    </div>
    </div>
@stop
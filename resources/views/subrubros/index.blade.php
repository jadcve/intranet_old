@extends('layouts.principal')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Dashboard Sub-rubros</h2>
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Formulario</li>
                <li class="active"><strong>Sub-rubro</strong></li>
            </ol>
        </div>
    </div>

    @if(Session::has('message'))
    <div class="alert alert-success alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ Session::get('message') }}
    </div>
    @endif

    <div class="row">
    	<div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Crear &Aacute;rea</h5>
                </div>
                <div class="ibox-content">
    				@include('alerts.request')
    				{!! Form::open(['route'=> 'subrubros.store', 'method'=>'POST']) !!}
                        <div class="form-group">
                            {!! Form::label('Rubro') !!}
                            {!! Form::select('rubro', $rubros, null,['class'=>'form-control']) !!}
                        </div>                    
    					<div class="form-group">
    						{!! Form::label('Nombre del sub-rubro') !!}
    					    {!! Form::text('subrubro', null, ['placeholder'=>'Sub-rubro', 'class'=>'form-control']) !!}
    					</div>
    					<div class="form-group">
    						{!! Form::label('Estatus del Sub-rubro') !!}
    					    {!! Form::select('status', ['1' => 'Activo', '0' => 'Inactivo'], '1',['class' => 'form-control']) !!}
    					</div>
    	            	{!! Form::submit('Registrar', ['class' => 'btn btn-primary block full-width m-b']) !!}				            
    				{!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-lg-7">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Listado de &Aacute;reas</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-hover no-margins">
                        <thead>
                        <tr>
                            <th>Rubro</th>
                            <th>Sub-rubro</th>
                            <th>Estatus</th>
                            <th>Acci&oacute;n</th>
                        </tr>
                        </thead>
                        @foreach($subrubros as $subrubro)
                        <tbody>
                        <tr>
                            <td><small>{{ $subrubro->rubro }}</small></td>
                            <td><small>{{ $subrubro->subrubro }}</small></td>
                            <td>@if ( $subrubro->status == 1) <div class="alert-success">Activo</div> @else <div class="alert-warning">Inactivo</div> @endif</td>
                            <td>{!! link_to_route('subrubros.edit', $title = 'Editar', $parameters = $subrubro->id, $attributes = ['class'=>'btn btn-primary']) !!}</td>
                        </tr>
                        </tbody>
                        @endforeach
                    </table>
                   
                </div>
            </div>
        </div>
    </div>
@stop
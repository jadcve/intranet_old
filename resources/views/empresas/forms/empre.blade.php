<div class="form-group">
	{!! Form::label('Nombre de la empresa') !!}
    {!! Form::text('empresa', null, ['placeholder'=>'Empresa', 'class'=>'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('Rubro de la empresa') !!}
    {!! Form::select('rubro', $rubros, null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('Sub-rubro de la empresa') !!}
    {!! Form::select('subrubro', $subrubros, null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('Tel&eacute;fono de la empresa') !!}
    {!! Form::text('telefono', null,['placeholder'=>'Tel&eacute;fono', 'class'=>'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('Origen') !!}
    {!! Form::select('origen', ['1' => 'Nacional', '2' => 'Internacional'], '1',['class' => 'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('Pa&iacute;s de la empresa') !!}
    {!! Form::select('pais', $paises, null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('Web de la empresa') !!}
    {!! Form::text('web', null,['placeholder'=>'Web', 'class'=>'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('Logo de la empresa') !!}
    {!! Form::file('logo', ['class'=>'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('Estatus de la Empresa') !!}
    {!! Form::select('status', ['1' => 'Activo', '0' => 'Inactivo'], '1',['class' => 'form-control']) !!}
</div>
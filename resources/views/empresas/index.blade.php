@extends('layouts.principal')

@section('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
@endsection

@section('content')

	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2>Dashboard Empresas</h2>
	        <ol class="breadcrumb">
	            <li>Home</li>
	            <li>Formulario</li>
	            <li class="active"><strong>Ver Empresas</strong></li>
	        </ol>
	    </div>
	</div>

	@if(Session::has('message'))
	<div class="alert alert-success alert-dismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  {{ Session::get('message') }}
	</div>

	@endif
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Listado de Empresas</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
				<table id="table_id" class="display">
				    <thead>
				        <tr>
				            <th>Id</th>
				            <th>Logo</th>
							<th>Empresa</th>
							<th>Rubro</th>
							<th>Sub-rubro</th>
							<th>Telefono</th>
							<th>Origen</th>
							<th>Pais</th>
							<th>Web</th>
							<th>Estatus</th>
							<th>Accion</th>
				        </tr>
				    </thead>
				    <tbody>
				        @foreach($empresas as $empresa)	
						<tr>
							<td>{{ $empresa->id }}</td>
							<td><img src="{!! asset('img/logos') !!}/{{ $empresa->path }}" width="50px" height="50px" /></td>
							<td>{{ $empresa->empresa }}</td>
							<td>{{ $empresa->rubro }}</td>
							<td>{{ $empresa->subrubro }}</td>
							<td>{{ $empresa->telefono }}</td>
							<td>@if ( $empresa->origen == 1) Nacional @else Internacional @endif</td>
							<td>{{ $empresa->pais }}</td>
							<td>{{ $empresa->web }}</td>
							<td>@if ( $empresa->status == 1) <p class="text-success">Activo</p> @else <p class="text-danger">Inactivo</p> @endif</td>
							<td>{!! link_to_route('empresas.edit', $title = 'Editar', $parameters = $empresa->id, $attributes = ['class'=>'btn btn-primary']) !!}</td>
						</tr>
						@endforeach
				    </tbody>
				</table>             	
            </div>
		</div>
	</div>
@endsection

@section('javascripts')
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script>
    
    $(document).ready(function() {
    $('#table_id').DataTable( {
        "lengthMenu": [[100, 50, 20, -1], [100, 50, 20, "Todos"]]
    } );
} );
    </script>
@endsection
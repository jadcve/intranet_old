@extends('layouts.principal')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Dashboard Empresas</h2>
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Formulario</li>
                <li class="active"><strong>Editar Empresa</strong></li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">            
        <div class="col-lg-5">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Editar Empresa</h5>
                </div>
                <div class="ibox-content">
                	@include('alerts.request')
					{!! Form::model($empresa,['route'=> ['empresas.update',$empresa->id], 'method'=>'PUT']) !!}
                        <div class="form-group">
                            {!! Form::label('Nombre de la empresa') !!}
                            {!! Form::text('empresa', null, ['placeholder'=>'Empresa', 'class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Rubro de la empresa') !!}
                            {!! Form::select('rubro', $rubros, null,['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Sub-rubro de la empresa') !!}
                            {!! Form::select('subrubro', $subrubros, null,['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Tel&eacute;fono de la empresa') !!}
                            {!! Form::text('telefono', null,['placeholder'=>'Tel&eacute;fono', 'class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Origen') !!}
                            {!! Form::select('origen', ['1' => 'Nacional', '2' => 'Internacional'], '1',['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Pa&iacute;s de la empresa') !!}
                            {!! Form::select('pais', $paises, null,['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Web de la empresa') !!}
                            {!! Form::text('web', null,['placeholder'=>'Web', 'class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Estatus de la Empresa') !!}
                            {!! Form::select('status', ['1' => 'Activo', '0' => 'Inactivo'], '1',['class' => 'form-control']) !!}
                        </div>
		            	{!! Form::submit('Actualizar', ['class' => 'btn btn-primary block full-width m-b']) !!}				            
					{!! Form::close() !!}
					
					@if ( Auth::user()->rol == 1) 
                    {!! Form::model($empresa,['route'=> ['empresas.destroy',$empresa->id], 'method'=>'DELETE']) !!}
		    			
		            	{!! Form::submit('Eliminar', ['onClick'=>"javascript: return confirm('¿Esta seguro que desea eliminar el registro? todos los datos asociados a este ID de empresa se perderan');", 'class' => 'btn btn-danger block full-width m-b']) !!}				            
					{!! Form::close() !!}
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@stop
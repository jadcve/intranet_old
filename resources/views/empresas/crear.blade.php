@extends('layouts.principal')
@section('styles')
{!!Html::style('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css')!!}
@endsection
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Dashboard Empresas</h2>
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Formulario</li>
                <li class="active"><strong>Crear Empresa</strong></li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">            
        <div class="col-lg-5">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Crear Empresa</h5>
                </div>
                <div class="ibox-content">
    				@include('alerts.request')
    				{!! Form::open(['route'=> 'empresas.store', 'method'=>'POST', 'files' => true]) !!}
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Nombre de la empresa') !!}
                            {!! Form::text('empresa', null, ['placeholder'=>'Empresa', 'class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Rubro de la empresa') !!}
                            {!! Form::select('rubro', $rubros, null,['class'=>'basic-single form-control']) !!}
                        </div>
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Sub-rubro de la empresa') !!}
                            {!! Form::select('subrubro', $subrubros, null,['class'=>'basic-single form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Tel&eacute;fono de la empresa') !!}
                            {!! Form::text('telefono', null,['placeholder'=>'Tel&eacute;fono', 'class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Origen') !!}
                            {!! Form::select('origen', ['1' => 'Nacional', '2' => 'Internacional'], '1',['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Pa&iacute;s de la empresa') !!}
                            {!! Form::select('pais', $paises, null,['class'=>'basic-single form-control']) !!}
                        </div>
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Web de la empresa') !!}
                            {!! Form::text('web', null,['placeholder'=>'Web', 'class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Logo de la empresa') !!}
                            {!! Form::file('path', ['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Estatus de la Empresa') !!}
                            {!! Form::select('status', ['1' => 'Activo', '0' => 'Inactivo'], '1',['class' => 'form-control']) !!}
                        </div>
    	            	{!! Form::submit('Registrar', ['class' => 'btn btn-primary block full-width m-b']) !!}				            
    				{!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
@section('javascripts')
{!!Html::script('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js')!!}
<script>
    // In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    $('.basic-single').select2();
});
</script>
@endsection
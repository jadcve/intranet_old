@extends('layouts.principal')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Dashboard Carrusel</h2>
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Formulario</li>
                <li class="active"><strong>Carrusel</strong></li>
            </ol>
        </div>
    </div>

    <div class="row">
    	<div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cargar Imagenes a Carrusel</h5>
                </div>
                <div class="ibox-content">
                    {!! Form::open(['route'=> 'uploadimagenes.store', 'method'=>'POST', 'files' => true]) !!} 						
    					    {!! Form::hidden('carousel', $carousel->id, null, ['class'=>'form-control']) !!}
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Imagenes') !!}<br><br>
                            {{Form::file('imagen[]', ['multiple', 'id' => 'imagen'])}}
                        </div>
    	            	{!! Form::submit('Subir Imagenes', ['class' => 'btn btn-primary block full-width m-b']) !!}				            
    				{!! Form::close() !!}
                </div>
            </div>
        </div>        
    </div>
@stop
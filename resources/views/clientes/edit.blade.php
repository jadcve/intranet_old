@extends('layouts.principal')
@section('styles')
{!!Html::style('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css')!!}
@endsection
@section('content')           
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Dashboard Clientes</h2>
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Formulario</li>
                <li class="active"><strong>Editar Cliente</strong></li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">            
        <div class="col-lg-5">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Editar Cliente</h5>
                </div>
                <div class="ibox-content">
                	@include('alerts.request')
    				{!! Form::model($cliente, ['route'=> ['clientes.update',$cliente->id], 'method'=>'PUT', 'files' => true]) !!}
                        <div class="form-group">
                            {!! Form::label('Nombre del cliente') !!}
                            {!! Form::text('nombre', null, ['placeholder'=>'Nombre', 'class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Apellido del Cliente') !!}
                            {!! Form::text('apellido', null, ['placeholder'=>'Apellido', 'class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Empresa del cliente') !!}
                            {!! Form::select('empresa', $empresas, null,['class'=>'basic-single form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Cargo del cliente') !!}
                            {!! Form::select('cargo', $cargos, null,['class'=>'basic-single form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Tel&eacute;fono del cliente') !!}
                            {!! Form::text('telefono', null,['placeholder'=>'Tel&eacute;fono', 'class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                                {!! Form::label('Foto del cliente') !!}
                                {!! Form::file('path', ['class'=>'form-control']) !!}
                            </div>
                        <div class="form-group">
                            {!! Form::label('Email del cliente') !!}
                            {!! Form::email('email', null,['placeholder'=>'Email','class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Password del cliente') !!}
                             {{ Form::password('password',array('placeholder'=>'Password','class' => 'form-control')) }}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Estatus del cliente') !!}
                            {!! Form::select('status', ['1' => 'Activo', '0' => 'Inactivo'], $cliente->status,['class' => 'form-control']) !!}
                        </div>
    	            	{!! Form::submit('Actualizar', ['class' => 'btn btn-primary block full-width m-b']) !!}				            
    				{!! Form::close() !!}
    				
    				@if ( Auth::user()->rol == 1)
                    {!! Form::model($cliente,['route'=> ['clientes.destroy',$cliente->id], 'method'=>'DELETE']) !!}				    			
    	            	{!! Form::submit('Eliminar', ['onClick'=>"javascript: return confirm('¿Esta seguro que desea eliminar el registro? todos los datos asociados a este ID de cliente se perderan');",'class' => 'btn btn-danger block full-width m-b']) !!}				            
    				{!! Form::close() !!}
                    @endif
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
@section('javascripts')
{!!Html::script('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js')!!}
<script>
    // In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    $('.basic-single').select2();
});
</script>
@endsection
<div class="form-group">
	{!! Form::label('Nombre del cliente') !!}
    {!! Form::text('nombre', null, ['placeholder'=>'Nombre', 'class'=>'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('Apellido del Cliente') !!}
    {!! Form::text('apellido', null, ['placeholder'=>'Apellido', 'class'=>'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('Empresa del cliente') !!}
    {!! Form::select('empresa', $empresas, null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('Cargo del cliente') !!}
    {!! Form::select('cargo', $cargos, null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('Tel&eacute;fono del cliente') !!}
    {!! Form::text('telefono', null,['placeholder'=>'Tel&eacute;fono', 'class'=>'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('Foto del cliente') !!}
    {!! Form::file('path', ['class'=>'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('Email del cliente') !!}
    {!! Form::email('email', null,['placeholder'=>'Email','class'=>'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('Password del cliente') !!}
     {{ Form::password('password',array('placeholder'=>'Password','class' => 'form-control')) }}
</div>
<div class="form-group">
	{!! Form::label('Estatus del cliente') !!}
    {!! Form::select('status', ['1' => 'Activo', '0' => 'Inactivo'], '1',['class' => 'form-control']) !!}
</div>
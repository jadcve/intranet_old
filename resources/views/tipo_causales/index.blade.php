@extends('layouts.principal')

@section('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Dashboard Tipo de Causales</h2>
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Formulario</li>
                <li class="active"><strong>Tipo de Causales</strong></li>
            </ol>
        </div>
    </div>

    @if(Session::has('message'))
    <div class="alert alert-success alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ Session::get('message') }}
    </div>
    @endif

    <div class="row">
        <div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Crear Tipo de Causal</h5>
                </div>
                <div class="ibox-content">
                    @include('alerts.request')
                    {!! Form::open(['route'=> 'tipo_causales.store', 'method'=>'POST']) !!}
                        <div class="form-group">
                            {!! Form::label('Tipo de Causal') !!}
                            {!! Form::text('tipo', null,['class'=>'form-control']) !!}
                        </div>                    
                        <div class="form-group">
                            {!! Form::label('Estatus del Tipo de Causal') !!}
                            {!! Form::select('status', ['1' => 'Activo', '0' => 'Inactivo'], '1',['class' => 'form-control']) !!}
                        </div>
                        {!! Form::submit('Registrar', ['class' => 'btn btn-primary block full-width m-b']) !!}                          
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-lg-7">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Listado de Tipos de Causales</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <table id="table_id" class="display">
                        <thead>
                        <tr>
                            <th>Tipo de Causal</th>
                            <th>Estatus</th>
                            <th>Acci&oacute;n</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($tipo_causales as $tipo_causal)                        
                        <tr>
                            <td><small>{{ $tipo_causal->tipo }}</small></td>
                            <td>@if ( $tipo_causal->status == 1) <p class="text-success">Activo</p> @else <p class="text-danger">Inactivo</p> @endif</td>
                            <td>{!! link_to_route('tipo_causales.edit', $title = 'Editar', $parameters = $tipo_causal->id, $attributes = ['class'=>'btn btn-primary']) !!}</td>
                        </tr>                        
                        @endforeach
                        </tbody>
                    </table>
                   
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascripts')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready( function () {
            $('#table_id').DataTable();
        } );
    </script>
@endsection
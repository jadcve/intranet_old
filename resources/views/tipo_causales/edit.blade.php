@extends('layouts.principal')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Dashboard Tipo de Causales</h2>
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Formulario</li>
                <li class="active"><strong>Tipo de Causales</strong></li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">            
        <div class="col-lg-5">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Editar Tipo de Causales</h5>
                </div>
                <div class="ibox-content">
                	@include('alerts.request')
    				{!! Form::model($tipo_causal,['route'=> ['tipo_causales.update',$tipo_causal->id], 'method'=>'PUT']) !!}
                        <div class="form-group">
                            {!! Form::label('Nombre del tipo de Causal') !!}
                            {!! Form::text('tipo', null, ['placeholder'=>'Tipo de Causal', 'class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Estatus del Tipo de Causal') !!}
                            {!! Form::select('status', ['1' => 'Activo', '0' => 'Inactivo'], $tipo_causal->status,['class' => 'form-control']) !!}
                        </div>
    	            	{!! Form::submit('Actualizar', ['class' => 'btn btn-primary block full-width m-b']) !!}				            
    				{!! Form::close() !!}
    				@if ( Auth::user()->rol == 1)
    				{!! Form::model($tipo_causal,['route'=> ['tipo_causales.destroy',$tipo_causal->id], 'method'=>'DELETE']) !!}
    	    			
    	            	{!! Form::submit('Eliminar', ['class' => 'btn btn-danger block full-width m-b']) !!}				            
    				{!! Form::close() !!}
                    @endif
                </div>
            </div>
        </div>
    </div>
    </div>
@stop
@extends('layouts.principal')

@section('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Dashboard KPI Analytics</h2>
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Formulario</li>
                <li class="active"><strong>KPI Analytics</strong></li>
            </ol>
        </div>
    </div>

    @if(Session::has('message'))
    <div class="alert alert-success alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ Session::get('message') }}
    </div>
    @endif

    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Administrador de Carga</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <table id="table_carga" class="display">
                    <thead>
                        <tr>
                            <th>Empresa</th>
                            <th>Cliente</th>
                            <th>Mes Carga</th>
                            <th>A&ntilde;o Carga</th>
                            <th>Modulo Cargado</th>
                            <th>Acci&oacute;n</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        @foreach($reg_cargas as $reg_carga)
                        <tr>
                            <td>{{ $reg_carga->empresa }}</td>
                            <td>{{ $reg_carga->nombre }} {{ $reg_carga->apellido }}</td>
                            <td>{{ $reg_carga->mes }}</td>
                            <td>{{ $reg_carga->anio }}</td>
                            <td>{{ $reg_carga->tipo_carga }}</td>
                            <td>
                              {!! link_to_route('reg_cargas.edit', $title = 'Editar', $parameters = $reg_carga->id, $attributes = ['class'=>'btn btn-primary']) !!}
                            </td>                            
                        </tr>
                        @endforeach
                    </tbody>                        
                </table>                    
            </div>
        </div>
    </div>
@endsection

@section('javascripts')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready( function () {
            $('#table_carga').DataTable();
        } );
    </script>
@endsection
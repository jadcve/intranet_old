@extends('layouts.principal')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Administrador de Carga</h2>
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Formulario</li>
                <li class="active"><strong>Administrador de Carga</strong></li>
            </ol>
        </div>
    </div>

    @if(Session::has('message'))
    <div class="alert alert-success alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ Session::get('message') }}
    </div>
    @endif

    <div class="row">
        <div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Editar Carga</h5>
                </div>
                <div class="ibox-content">
                    @include('alerts.request')
                    {!! Form::model($reg_carga,['route'=> ['reg_cargas.destroy',$reg_carga->id], 'method'=>'DELETE']) !!}
                        <p align="justify">Se va ha proceder a eliminar de la Base de Datos el registro seleccionado correspondiente al <b>A&ntilde;o {{ $reg_carga->anio }}</b> del <b>Mes {{ $reg_carga->mes  }}</b> de la <b>Tabla de {{ $reg_carga->tipo_carga}}</b> </p>
                        <br>
                        {!! Form::submit('Eliminar', ['onClick'=>"javascript: return confirm('¿Esta seguro que desea eliminar la carga?');",'class' => 'btn btn-danger block full-width m-b']) !!}                          
                    {!! Form::close() !!}
                </div>
            </div>
        </div>        
    </div>
@stop
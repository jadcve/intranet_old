@extends('layouts.principal')

@section('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
@endsection

@section('content')
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2>Dashboard Bitacora</h2>
	        <ol class="breadcrumb">
	            <li>Home</li>
	            <li>Formulario</li>
	            <li class="active"><strong>Ver Bitacora</strong></li>
	        </ol>
	    </div>
	</div>

	@if(Session::has('message'))
	<div class="alert alert-success alert-dismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  {{ Session::get('message') }}
	</div>
	@endif
		<div class="col-lg-2" align="center"></div>
		<div class="col-lg-8" align="center">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Bitacora de Clientes</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
	            <div class="ibox-content">
					<table id="table_id" class="display">
					    <thead>
							<tr>
								<th>Usuario</th>
								<th>Emresa</th>
								<th>Pagina</th>
								<th>Fecha</th>
							</tr>
						</thead>
						<tbody>
						@foreach($bitacoras as $bitacora)
							<tr>					
								<td>{{ $bitacora->nombre }} {{ $bitacora->apellido }}</td>
								<td>{{ $bitacora->empresa }}</td>
								<td>{{ $bitacora->pagina }}</td>
								<td>{{ $bitacora->fecha }}</td>
							</tr>
						@endforeach
						</tbody>
						<tfoot>
						    <tr>
								<th>Usuario</th>
								<th>Emresa</th>
								<th>Pagina</th>
								<th>Fecha</th>
						    </tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
		<div class="col-lg-2" align="center"></div>
@endsection

@section('javascripts')
	  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script>
    
    $(document).ready(function() {
    $('#table_id').DataTable( {
        "lengthMenu": [[100, 50, 20, -1], [100, 50, 20, "Todos"]]
    } );
} );
    </script>
@endsection
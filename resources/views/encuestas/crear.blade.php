@extends('layouts.principal')
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Dashboard Encuestas</h2>
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Formulario</li>
                <li class="active"><strong>Encuestas</strong></li>
            </ol>
        </div>
    </div>

    <div class="row">
    	<div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Crear Encuesta</h5>
                </div>
                <div class="ibox-content">
    				@include('alerts.request')
    				{!! Form::open(['route'=> 'encuestas.store', 'method'=>'POST', 'files' => true]) !!}
                        <div class="form-group">
                            <input type="checkbox" name="enviar_mail" value="SI"> &nbsp;{!! Form::label('Enviar correo Creaci&oacute;n de Estudio') !!}
                        </div>                 
    					<div class="form-group">
    						<font color="red">* </font>{!! Form::label('Nombre de la Encuesta') !!}
    					    {!! Form::text('encuesta', null, ['placeholder'=>'Encuesta', 'class'=>'form-control']) !!}
    					</div>
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Descripci&oacute;n de la Encuesta') !!}
                            {!! Form::text('descripcion', null, ['placeholder'=>'Descripci&oacute;n', 'class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('URL de la Encuesta') !!}
                            {!! Form::text('document', null, ['placeholder'=>'URL', 'class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Usuarios') !!}
                            {{Form::select('usuario[]',$clientes,null,array('multiple'=>true,'class'=>'form-control'))}}
                        </div>
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Estatus de la Encuesta') !!}
                            {!! Form::select('status', ['1' => 'Activo', '0' => 'Inactivo'], '1',['class' => 'form-control']) !!}
                        </div>
    	            	{!! Form::submit('Crear', ['class' => 'btn btn-primary block full-width m-b']) !!}				            
    				{!! Form::close() !!}
                </div>
            </div>
        </div>        
    </div>
@stop
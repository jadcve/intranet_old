@extends('layouts.principal')
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Dashboard Encuestas</h2>
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Formulario</li>
                <li class="active"><strong>Encuestas</strong></li>
            </ol>
        </div>
    </div>

    @if(Session::has('message'))
    <div class="alert alert-success alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ Session::get('message') }}
    </div>
    @endif

    <div class="row">
        <div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Editar Encuesta</h5>
                </div>
                <div class="ibox-content">
                    @include('alerts.request')
                    {!! Form::model($encuesta,['route'=> ['encuestas.update',$encuesta->id], 'method'=>'PUT', 'files' => true]) !!}

                        <div class="form-group">
                            {!! Form::label('Nombre de la Encuesta') !!}
                            {!! Form::text('encuesta', null, ['placeholder'=>'Encuesta', 'class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Descripci&oacute;n de la Encuesta') !!}
                            {!! Form::text('descripcion', null, ['placeholder'=>'Descripci&oacute;n', 'class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('URL de la Encuesta') !!}
                            {!! Form::text('document', $encuesta->document, ['placeholder'=>'URL', 'class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Usuario que ver&aacute; la Encuesta') !!}
                            {!! Form::select('usuario',  $clientes, null,['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Estatus de la Encuesta') !!}
                            {!! Form::select('status', ['1' => 'Activo', '0' => 'Inactivo'], $encuesta->status,['class' => 'form-control']) !!}
                        </div>
                        {!! Form::submit('Actualizar', ['class' => 'btn btn-primary block full-width m-b']) !!}                          
                    {!! Form::close() !!}
                    @if ( Auth::user()->rol == 1)
                    {!! Form::model($encuesta,['route'=> ['encuestas.destroy',$encuesta->id], 'method'=>'DELETE']) !!}
                        
                        {!! Form::submit('Eliminar', ['onClick'=>"javascript: return confirm('¿Esta seguro que desea eliminar la encuesta?');",'class' => 'btn btn-danger block full-width m-b']) !!}                            
                    {!! Form::close() !!}
                    @endif
                </div>
            </div>
        </div>        
    </div>
@stop
@extends('layouts.principal')

@section('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
@endsection

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Dashboard Encuestas</h2>
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Formulario</li>
                <li class="active"><strong>Encuestas</strong></li>
            </ol>
        </div>
    </div>

    @if(Session::has('message'))
    <div class="alert alert-success alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ Session::get('message') }}
    </div>
    @endif

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Listado de Encuestas</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <a href="{!! URL::to('encuestas/create') !!}"><button class="btn btn-primary">Crear Encuesta</button></a>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <table id="table_id" class="display">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Descripci&oacute;n</th>
                                <th>Link encuesta</th>
                                <th>Usuario</th>
                                <th>Estatus</th>
                                <th>Acci&oacute;n</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($encuestas as $encuesta)
                            <tr>
                                <td>{{ $encuesta->encuesta }}</td>
                                <td>{{ $encuesta->descripcion }}</td>
                                <td><a href="{{ $encuesta->document }}" target="_blank"><button class="btn btn-primary">Ver Encuesta</button></a></td>
                                <td>{{ $encuesta->nombre }} {{ $encuesta->apellido }}</td>
                                <td>@if ( $encuesta->status == 1) <p class="text-success">Activo</p> @else <p class="text-danger">Inactivo</p> @endif</td>
                                <td>{!! link_to_route('encuestas.edit', $title = 'Editar', $parameters = $encuesta->id, $attributes = ['class'=>'btn btn-primary']) !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascripts')
      <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script>
    
    $(document).ready(function() {
    $('#table_id').DataTable( {
        "lengthMenu": [[100, 50, 20, -1], [100, 50, 20, "Todos"]]
    } );
} );
    </script>
@endsection
@extends('layouts.principal')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Dashboard estamentos</h2>
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Formulario</li>
                <li class="active"><strong>estamentos</strong></li>
            </ol>
        </div>
    </div>

    @if(Session::has('message'))
    <div class="alert alert-success alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ Session::get('message') }}
    </div>
    @endif

    <div class="row">
    	<div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Crear estamento</h5>
                </div>
                <div class="ibox-content">
    				@include('alerts.request')
    				{!! Form::open(['route'=> 'estamentos.store', 'method'=>'POST']) !!}
    					<div class="form-group">
    						{!! Form::label('Nombre del Estamento') !!}
    					    {!! Form::text('estamento', null, ['placeholder'=>'Estamento', 'class'=>'form-control']) !!}
    					</div>
    					<div class="form-group">
    						{!! Form::label('Estatus del Estamento') !!}
    					    {!! Form::select('status', ['1' => 'Activo', '0' => 'Inactivo'], '1',['class' => 'form-control']) !!}
    					</div>
    	            	{!! Form::submit('Registrar', ['class' => 'btn btn-primary block full-width m-b']) !!}				            
    				{!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-lg-7">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Listado de estamentos</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-hover no-margins">
                        <thead>
                        <tr>
                            <th>Estamentos</th>
                            <th>Estatus</th>
                            <th>Acci&oacute;n</th>
                        </tr>
                        </thead>
                        @foreach($estamentos as $estamento)
                        <tbody>
                        <tr>
                            <td><small>{{ $estamento->estamento }}</small></td>
                            <td>@if ( $estamento->status == 1) <p class="text-success">Activo</p> @else <p class="text-danger">Inactivo</p> @endif</td>
                            <td>{!! link_to_route('estamentos.edit', $title = 'Editar', $parameters = $estamento->id, $attributes = ['class'=>'btn btn-primary']) !!}</td>
                        </tr>
                        </tbody>
                        @endforeach
                    </table>
                    {!! $estamentos->render() !!}
                </div>
            </div>
        </div>
    </div>
@stop
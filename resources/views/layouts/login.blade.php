<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Estudios | Iniciar Sesi&oacute;n</title>
    <link rel=icon href=favicon.png sizes="16x16" type="image/png">
    {!!Html::style('css/bootstrap.min.css')!!}
    {!!Html::style('font-awesome/css/font-awesome.css')!!}
    {!!Html::style('css/animate.css')!!}
    {!!Html::style('css/style.css')!!}

</head>

<body class="gray-bg">

    @yield('content')

    <!-- Mainly scripts -->

 
    {!!Html::script('js/jquery-3.1.1.min.js')!!}
    {!!Html::script('js/jquery-3.1.1.min.js')!!}
    {!!Html::script('js/bootstrap.min.js')!!}

</body>

</html>
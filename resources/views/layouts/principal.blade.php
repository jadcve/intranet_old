<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="icon" href="{!! asset('favicon.png') !!}" sizes="16x16" type="image/png"/>
    <title>KPI Estudios</title>
    {!!Html::style('css/bootstrap.min.css')!!}
    {!!Html::style('font-awesome/css/font-awesome.css')!!}
    {!!Html::style('css/animate.css')!!}
    {!!Html::style('css/style.css')!!}
    {!!Html::style('css/custom.css')!!}
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.4.0/css/bootstrap4-toggle.min.css" rel="stylesheet">

    @yield('styles')  
</head>

<body>
<script>

    Base =  {!! json_encode(url('/api')) !!}
    BaseURL =  {!! json_encode(url('/')) !!}
    
</script>
<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element" align="center">
                        <img src="{!! asset('img/logo_kpi.png') !!}" width="60%">
                    </div>
                    <div class="logo-element">
                        KPI
                    </div>
                </li>
                <li class="active">
                    <a href="{!! URL::to('admin') !!}"><i class="fa fa-home"></i> <span class="nav-label">Inicio</span></a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-user"></i> <span class="nav-label">Usuarios</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        @if ( Auth::user()->rol == 1) <li><a href="{!! URL::to('/usuarios/create') !!}">Crear</a></li> @endif
                        <li><a href="{!! URL::to('usuarios') !!}">Ver</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-user-o"></i> <span class="nav-label">Clientes</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="{!! URL::to('/clientes/create') !!}">Crear</a></li>
                        <li><a href="{!! URL::to('clientes') !!}">Ver</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-building"></i> <span class="nav-label">Empresas</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="{!! URL::to('/empresas/create') !!}">Crear</a></li>
                        <li><a href="{!! URL::to('empresas') !!}">Ver</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-area-chart"></i> <span class="nav-label">KPI Analytics</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="{!! URL::to('/trackins/create') !!}">Activar Servicio</a></li>
                        <li><a href="{!! URL::to('trackins') !!}">Ver Clientes</a></li>

                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-address-book-o"></i> <span class="nav-label">BNMK Indicadores</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="{!! URL::to('/bench_indicadores/create') !!}">Activar Servicio</a></li>
                        <li><a href="{!! URL::to('bench_indicadores') !!}">Ver Clientes</a></li>
                    </ul>
                </li>

                <li>
                    <a href="#"><i class="fa fa-archive"></i> <span class="nav-label">Estudios</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="{!! URL::to('estudios') !!}">Ver Estudios</a></li>
                        <li><a href="{!! URL::to('estudios/create') !!}">Crear</a></li>
                        <li><a href="{!! URL::to('estudio_validate') !!}">Validaciones</a></li>
                    </ul>
                </li>


                <li>
                    <a href="#"><i class="fa fa-product-hunt"></i> <span class="nav-label">Productos</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="{!! URL::to('benchmarks') !!}">Benchmark</a></li>
                        <li><a href="{!! URL::to('encuestas') !!}">Encuestas</a></li>
                        <li><a href="{!! URL::to('promotions') !!}">Promociones</a></li>
                    </ul>
                </li>

                <li>
                    <a href="#"><i class="fa fa-file-text-o"></i> <span class="nav-label">Admin. de Cargas</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="{!! URL::to('reg_cargas') !!}">Administrar Carga</a></li>
                        <li><a href="{!! URL::to('bitacoras') !!}">Ver Bitacora</a></li>
                    </ul>
                </li>

                <li>
                    <a href="#"><i class="fa fa-cog"></i> <span class="nav-label">Configuraci&oacute;n</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="{!! URL::to('paises') !!}">Paises</a></li>
                        <li><a href="{!! URL::to('cargos') !!}">Cargos</a></li>
                        <li><a href="{!! URL::to('rubros') !!}">Rubros</a></li>
                        <li><a href="{!! URL::to('subrubros') !!}">Sub-rubros</a></li>
                        <li><a href="{!! URL::to('estamentos') !!}">Estamentos</a></li>
                        <li><a href="{!! URL::to('tipo_areas') !!}">Tipos de &Aacute;reas</a></li>
                        <li><a href="{!! URL::to('areas') !!}">&Aacute;reas</a></li>
                        <li><a href="{!! URL::to('tipo_causales') !!}">Tipos de Causales</a></li>
                        <li><a href="{!! URL::to('causales') !!}">Causales</a></li>
                        <li><a href="{!! URL::to('carousels') !!}">Carrusel</a></li>
                    </ul>
                </li>
                <li>
                    <a href="logout"><i class="fa fa-sign-out"></i> <span class="nav-label">Salir</span></a>
                </li>
            </ul>
        </div>
    </nav>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>          
                        <table>
                            <tbody>
                                <tr>
                                    <td>@if ( is_null(Auth::user()->path)) <img alt="image" class="img-circle" src="{!! asset('img/picture.png') !!}" width="50px" height="50px" />&nbsp;&nbsp; @else <img alt="image" class="img-circle" src="{!! asset('img/fotos') !!}/{!! Auth::user()->path !!}" width="50px" height="50px" />&nbsp;&nbsp; @endif</td>
                                    <td>
                                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                            <span class="clear"> 
                                                <span class="block m-t-xs">
                                                    <strong class="font-bold">{!! Auth::user()->nombre !!}&nbsp;{!! Auth::user()->apellido !!}</strong>
                                                </span> 
                                                <span class="text-muted text-xs block">
                                                    <small>@if ( Auth::user()->rol == 1 ) Administrador @else Usuario @endif</td></small>
                                                </span>
                                            </span> 
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">

                @yield('content')                 

                </div>
            </div>
        </div>
        <div class="footer">
            <div>
                <strong>Copyright</strong> Kpi Estudios &copy; 2019
            </div>
        </div>
    </div>
</div>
{!!Html::script('https://code.jquery.com/jquery-3.3.1.js')!!}
{!!Html::script('js/bootstrap.min.js')!!}
{!!Html::script('js/plugins/metisMenu/jquery.metisMenu.js')!!}
{!!Html::script('js/plugins/slimscroll/jquery.slimscroll.min.js')!!}
{!!Html::script('js/inspinia.js')!!}
{!!Html::script('js/plugins/pace/pace.min.js')!!}
{!!Html::script('js/custom.js')!!}
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.4.0/js/bootstrap4-toggle.min.js"></script>
@yield('javascripts')
</body>

</html>
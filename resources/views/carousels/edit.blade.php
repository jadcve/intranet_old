@extends('layouts.principal')
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Dashboard Carrusel</h2>
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Formulario</li>
                <li class="active"><strong>Carrusel</strong></li>
            </ol>
        </div>
    </div>

    @if(Session::has('message'))
    <div class="alert alert-success alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ Session::get('message') }}
    </div>
    @endif

    <div class="row">
    	<div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Crear Carrusel</h5>
                </div>
                <div class="ibox-content">
                    @include('alerts.request')
                    {!! Form::model($carousel,['route'=> ['carousels.update',$carousel->id], 'method'=>'PUT', 'files' => true]) !!}               
    					<div class="form-group">
    						<font color="red">* </font>{!! Form::label('Nombre del Carrusel') !!}
    					    {!! Form::text('titulo', null, ['placeholder'=>'Encuesta', 'class'=>'form-control']) !!}
    					</div>
                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Descripci&oacute;n del Carrusel') !!}
                            {!! Form::text('descripcion', null, ['placeholder'=>'Descripci&oacute;n', 'class'=>'form-control']) !!}
                        </div>

                        <div class="form-group">
                            <font color="red">* </font>{!! Form::label('Estatus del Carrusel') !!}
                            {!! Form::select('status', ['1' => 'Activo', '0' => 'Inactivo'], $carousel->status,['class' => 'form-control']) !!}
                        </div>
    	            	{!! Form::submit('Actualizar', ['class' => 'btn btn-primary block full-width m-b']) !!}				            
    				{!! Form::close() !!}
                </div>
            </div>
        </div>        
    </div>
@stop
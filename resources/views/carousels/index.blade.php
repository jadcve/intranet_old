@extends('layouts.principal')
@section('content')
	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-10">
	        <h2>Dashboard Carrusel</h2>
	        <ol class="breadcrumb">
	            <li>Home</li>
	            <li>Formulario</li>
	            <li class="active"><strong>Ver Carruseles</strong></li>
	        </ol>
	    </div>
	</div>

	@if(Session::has('message'))
	<div class="alert alert-success alert-dismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  {{ Session::get('message') }}
	</div>
	@endif

		<div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Lista de Carruseles</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <a href="{!! URL::to('carousels/create') !!}"><button class="btn btn-primary">Crear Carrusel</button></a>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
					<table class="table table-bordered">
						<thead>
							<th>Nombre</th>
							<th>Descripci&oacute;n</th>
							<th>Imagenes</th>
							<th>Estatus</th>
							<th colspan="2">Acci&oacute;n</th>
							<th colspan="2">Imagenes</th>
						</thead>
						-@foreach($carousels as $carousel)
						<tbody>
						<tbody>
							<td>{{ $carousel->titulo }}</td>
							<td>{{ $carousel->descripcion }}</td>
							<td>@if ( DB::table('imagenes_carousels')->where('imagenes_carousels.carousel','=',$carousel->id)->count()  == 0 ) <div class="alert alert-danger">Carrusel S/Imagenes</div> @else <div class="alert alert-success">Imagenes Cargadas</div> @endif  </td>
							<td>@if ( $carousel->status == 1) <div class="alert alert-success">Activo</div> @else <div class="alert alert-warning">Inactivo</div> @endif</td>
							@if ( Auth::user()->rol == 1) <td>{!! link_to_route('carousels.edit', $title = 'Editar Carrusel', $parameters = $carousel->id, $attributes = ['class'=>'btn btn-primary']) !!}</td>
							<td>
			                    {!! Form::model($carousel,['route'=> ['carousels.destroy',$carousel->id], 'method'=>'DELETE']) !!}			                        
			                        {!! Form::submit('Eliminar Carrusel', ['onClick'=>"javascript: return confirm('¿Esta seguro que desea eliminar el Carrusel? se eliminaran todas las promociones asociadas al carrusel');",'class' => 'btn btn-danger m-b']) !!}                            
			                    {!! Form::close() !!}
			                    @else <td colspan="2">{!! link_to_route('carousels.edit', $title = 'Editar Carrusel', $parameters = $carousel->id, $attributes = ['class'=>'btn btn-primary']) !!}</td>

			                @endif</td>
			                							

							<td><a class="btn btn-success" href="{{url('cargarimg', ['id' => $carousel->id])}}">Subir Imagenes</a></td>
							@if ( Auth::user()->rol == 1)<td>
								
			                    {!! Form::model($carousel,['route'=> ['uploadimagenes.destroy',$carousel->id], 'method'=>'DELETE']) !!}	
			                        {!! Form::submit('Eliminar Imagenes', ['onClick'=>"javascript: return confirm('¿Esta seguro que desea eliminar las Imagenes?');",'class' => 'btn btn-danger m-b']) !!}                            
			                    {!! Form::close() !!}

			                </td>@endif
						</tbody>
						</tbody>
						@endforeach
					</table>
				</div>
			</div>
		</div>
@stop
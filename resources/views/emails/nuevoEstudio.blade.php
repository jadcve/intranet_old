<table border='0' cellpadding='0' cellspacing='0' width='100%'>	
	<tr>
		<td style='padding: 10px 0 30px 0;'>
			<table align='center' border='0' cellpadding='0' cellspacing='0' width='600' style='border: 1px solid #cccccc; border-collapse: collapse;'>
				<tr>
					<td align='center' bgcolor='#f7f7f7' style='padding: 40px 0 30px 0; color: #153643; font-size: 20px; font-weight: bold; font-family: Arial, sans-serif;'>
						<img src='http://globalanalyticshr.com/intranet/public/img/logo_kpi.png' alt='KPI Estudios' width='128' height='180' style='display: block;' />
						<p style='font-size: 15px; font-weight: bold; font-family: 'Arial'>Nuevo Documento Agregado</p>
					</td>
				</tr>
				<tr>
					<td bgcolor='#ffffff' style='padding: 40px 30px 40px 30px;'>
						<table border='0' cellpadding='0' cellspacing='0' width='100%'>
							<tr>
								<td align='center' style='padding: 40px 0 30px 0; color: #153643; font-size: 20px; font-weight: bold; font-family: Arial, sans-serif;'>
								<p style='font-size: 15px; font-weight: bold; font-family: 'Arial'>{!! $estudio !!}</p>
								<p style='font-size: 10px; font-weight: bold; font-family: 'Arial'>{!! $descripcion !!}</p>
								</td>
							</tr>
							<tr>
								<td style='color: #153643; font-family: Arial, sans-serif; font-size: 16px;'>
									<p align="justify">Un nuevo documento se agrego al estudio <b>{!! $estudio !!}</b> que se encuentra disponible en nuestra plataforma, por favor inicia sesi&oacute;n con tu usuario para poder visualizarlo.</p>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td bgcolor='#1c1d87' style='padding: 30px 30px 30px 30px;'>
						<table border='0' cellpadding='0' cellspacing='0' width='100%'>
							<tr>
								<td colspan='2' style='color: #ffffff; font-family: Arial, sans-serif; font-size: 12px;'>
									&reg; KPI Estudios 2018 <br>Todos los derechos reservados	
								</td>
								<td colspan='2' style='color: #ffffff; font-family: Arial, sans-serif; font-size: 12px;'>
									<p align='right'><a href='https://www.kpiestudios.com/' target='_blank'><span style='color:#ffffff'>www.kpiestudios.com</span></a><br>Todo lo que se puede medir, se puede mejorar.</p>
								</td>
							</tr>
							<tr>
								<td colspan='4'><hr style='color: #ffffff;' /><br></td>
							</tr>

							<tr>
								<td width='25%'></td>
								<td align='center'>
								<a href='https://www.linkedin.com/company/kpi-estudios' target='_blank'><img src='https://cdn-images.mailchimp.com/icons/social-block-v2/outline-color-linkedin-48.png' style='display:block;' height='34' width='34'></a>
								</td>
								<td align='center'>
								<a href='https://twitter.com/KpiEstudios' target='_blank'><img src='https://cdn-images.mailchimp.com/icons/social-block-v2/outline-color-twitter-48.png' style='display:block;' height='34' width='34'></a>
								</td>
								<td width='25%'></td>
							</tr>							
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>